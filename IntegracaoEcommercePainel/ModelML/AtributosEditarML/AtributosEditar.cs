﻿using IntegracaoPainel.EnvioItemML;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoEcommercePainel.ModelML.AtributosEditarML
{
    public class AtributosEditar
    {
        public AtributosEditar()
        {
        }

        public AtributosEditar(List<AtributoItemML> atributos)
        {
            attributes = atributos;
        }

        public List<AtributoItemML> attributes { get; set; }
    }
}
