﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML
{
    public class UsuarioTeste
    {
        public UsuarioTeste()
        {
        }

        public UsuarioTeste(int Id, string Nickname, string Password, string Site_status, string Email)
        {
            id          = Id;
            nickname    = Nickname;
            password    = Password;
            site_status = Site_status;
            email       = Email;

        }

        public int id { get; set; }
        public string nickname { get; set; }
        public string password { get; set; }
        public string site_status { get; set; }
        public string email { get; set; }
    }
}
