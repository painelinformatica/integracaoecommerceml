﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML
{
    public class ProdutoML
    {
        public ProdutoML()
        {
        }

        public ProdutoML(string idProduto, string titulo, string categoria, decimal preco, string tipoAnuncio, string descricao, string garantia)
        {
            IdProduto = idProduto;
            Titulo = titulo;
            Categoria = categoria;
            Preco = preco;
            TipoAnuncio = tipoAnuncio;
            Descricao = descricao;
            Garantia = garantia;
        }

        public string IdProduto { get; set; }
        public string Titulo { get; set; }
        public string Categoria { get; set; }
        public decimal Preco { get; set; }
        public string TipoAnuncio { get; set; }
        public string Descricao { get; set; }
        public string Garantia { get; set; }
    }
}
