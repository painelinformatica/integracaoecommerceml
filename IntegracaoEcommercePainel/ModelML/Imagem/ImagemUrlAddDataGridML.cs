﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Imagem
{
    public class ImagemUrlAddDataGridML
    {
        public ImagemUrlAddDataGridML()
        {
        }

        public ImagemUrlAddDataGridML(string urlImagem)
        {
            URLImagem = urlImagem;
        }

        public string URLImagem { get; set; }
    }
}
