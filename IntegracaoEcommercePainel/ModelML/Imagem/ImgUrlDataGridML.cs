﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Imagem
{
    public class ImgUrlDataGridML
    {
        public ImgUrlDataGridML()
        {
        }

        public ImgUrlDataGridML(string tipoVariacao, string variacaoSGE, string urlImagem)
        {
            TipoVariacao = tipoVariacao;
            VariacaoSGE = variacaoSGE;
            URLImagem = urlImagem;
        }

        public string TipoVariacao { get; set; }
        public string VariacaoSGE { get; set; }
        public string URLImagem { get; set; }
    }
}
