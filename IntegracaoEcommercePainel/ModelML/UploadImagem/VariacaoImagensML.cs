﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML
{
    public class VariacaoImagensML
    {
        public VariacaoImagensML()
        {
        }

        public VariacaoImagensML(string tamanhoImagem, string urlHttp, string urlHttps)
        {
            size = tamanhoImagem;
            url = urlHttp;
            secure_url = urlHttps;
        }

        public string size { get; set; }
        public string url { get; set; }
        public string secure_url { get; set; }
    }
}
