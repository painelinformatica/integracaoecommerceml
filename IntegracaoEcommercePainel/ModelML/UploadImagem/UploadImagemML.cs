﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.UploadImagem
{
    public class UploadImagemML
    {
        public UploadImagemML()
        {
        }

        public UploadImagemML(string idImagem, List<VariacaoImagensML> variacaoImagen)
        {
            id = idImagem;
            variations = variacaoImagen;
        }

        public string id { get; set; }
        public List<VariacaoImagensML> variations { get; set; }
    }
}
