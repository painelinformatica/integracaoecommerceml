﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Credencial
{
    public class CredencialML
    {
        public CredencialML() { 
        }

        public CredencialML(int idDadosDeAcesso, string nickNameUsuario, string emailUsuario, DateTime dataAutenticacao, string valorVendaSGE,string status, bool validar, int validadeEmHoras, int idOperacao, int idPessoal){
            IdDadosDeAcesso = idDadosDeAcesso;
            NickNameUsuario = nickNameUsuario;
            EmailUsuario = emailUsuario;
            DataAutenticacao = dataAutenticacao;
            ValorSGE = valorVendaSGE;
            Status = status;
            Validar = validar;
            ValidadeHoras = validadeEmHoras;
            IdOperacao = idOperacao;
            IdPessoal = idPessoal;
        }

        public int IdDadosDeAcesso { get; set; }
        public string NickNameUsuario { get; set; }
        public string EmailUsuario { get; set; }
        public DateTime DataAutenticacao { get; set; }
        public string ValorSGE { get; set; }
        public string Status { get; set; }
        public bool Validar { get; set; }
        public int ValidadeHoras { get; set; }
        public int IdOperacao { get; set; }
        public int IdPessoal { get; set; }

    }
}
