﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IntegracaoPainel.ModelML.Credencial
{
    public class TokenML
    {
        public TokenML()
        {
        }

        public TokenML(string access_token, string token_type, int expires_in, string scope, int user_id, string refresh_token, int status)
        {
            this.access_token   = access_token;
            this.token_type     = token_type;
            this.expires_in     = expires_in;
            this.scope          = scope;
            this.user_id        = user_id;
            this.refresh_token  = refresh_token;
            this.status         = status;
        }

        [DataMember(Name = "access_token")]
        public string access_token { get; set; }

        [DataMember(Name = "token_type")]
        public string token_type { get; set; }

        [DataMember(Name = "expires_in")]
        public int expires_in { get; set; }

        [DataMember(Name = "scope")]
        public string scope { get; set; }

        [DataMember(Name = "user_id")]
        public int user_id { get; set; }

        [DataMember(Name = "refresh_token")]
        public string refresh_token { get; set; }

        [DataMember(Name = "status")]
        public int status { get; set; }

    }
}
