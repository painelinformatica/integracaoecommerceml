﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IntegracaoPainel.ModelML.Credencial
{
    public class RetornaDadosUsuarioML
    {
        public RetornaDadosUsuarioML(int id, string nickname, string email, string country_id, string site_id)
        {
            this.id = id;
            this.nickname = nickname;
            this.email = email;
            this.country_id = country_id;
            this.site_id = site_id;
        }

        [DataMember(Name = "id")]
        public int id { get; set; }

        [DataMember(Name = "nickname")]
        public string nickname { get; set; }

        [DataMember(Name = "email")]
        public string email { get; set; }

        [DataMember(Name = "country_id")]
        public string country_id { get; set; }

        [DataMember(Name = "site_id")]
        public string site_id { get; set; }
    }
}
