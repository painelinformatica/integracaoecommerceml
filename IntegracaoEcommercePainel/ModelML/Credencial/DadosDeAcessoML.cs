﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Credencial
{
    public class DadosDeAcessoML
    {
        public DadosDeAcessoML()
        {
        }

        public DadosDeAcessoML(string idAplica, string skAplica, string nicknameUsuario, string emailUsuario, string accessTokenML, string refreshTokenML, int usuarioId, DateTime dataAutenticacao)
        {
            IdAplicacao      = idAplica;
            SkAplicacao      = skAplica;
            NicknameUsuario  = nicknameUsuario;
            EmailUsuario     = emailUsuario;
            AccessTokenML    = accessTokenML;
            RefreshTokenML   = refreshTokenML;
            UsuarioId        = usuarioId;
            DataAutenticacao = dataAutenticacao;
        }

        public string IdAplicacao        { get; set; }
        public string SkAplicacao        { get; set; }
        public string NicknameUsuario    { get; set; }
        public string EmailUsuario       { get; set; }
        public string AccessTokenML      { get; set; }
        public string RefreshTokenML     { get; set; }
        public int UsuarioId             { get; set; }
        public DateTime DataAutenticacao   { get; set; }
    }
}
