﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Status
{
    public class StatusItemML
    {
        public StatusItemML()
        {
        }

        public StatusItemML(string tipoStatus)
        {
            status = tipoStatus;
        }

        public string status { get; set; }
    }
}
