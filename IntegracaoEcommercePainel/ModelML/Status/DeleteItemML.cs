﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Status
{
    public class DeleteItemML
    {
        public DeleteItemML()
        {
        }

        public DeleteItemML(string deletarItem)
        {
            deleted = deletarItem;
        }

        public string deleted { get; set; }
    }
}
