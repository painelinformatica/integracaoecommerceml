﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Produtos
{
    public class ProdutoImportEcommerce
    {
        public ProdutoImportEcommerce()
        {
        }

        public ProdutoImportEcommerce(string idProdutoEC, string tituloEcommerce, int quantidadeEC, decimal precoEC, string statusProduto, bool vincularSGE, string urlAnuncioEC, bool btnAnuncioAtivo, string corFonteAnuncio, string jsonEnvio)
        {
            IdProdutoEcommerce = idProdutoEC;
            TituloEcommerce = tituloEcommerce;
            Quantidade = quantidadeEC;
            Preco = precoEC;
            Status = statusProduto;
            VincularSGE = vincularSGE;
            UrlAnuncio = urlAnuncioEC;
            AnuncioHabilitado = btnAnuncioAtivo;
            CorFonteAnuncio = corFonteAnuncio;
            JsonEnvio = jsonEnvio;
        }

        public string IdProdutoEcommerce { get; set; }
        public string TituloEcommerce { get; set; }
        public int Quantidade { get; set; }
        public decimal Preco { get; set; }
        public string Status { get; set; }
        public bool VincularSGE { get; set; }
        public string UrlAnuncio { get; set; }
        public bool AnuncioHabilitado { get; set; }
        public string CorFonteAnuncio { get; set; }
        public string JsonEnvio { get; set; }
     
    }
}
