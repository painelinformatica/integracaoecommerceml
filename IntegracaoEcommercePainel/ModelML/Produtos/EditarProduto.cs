﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Produtos
{
    public class EditarProduto
    {
        public EditarProduto()
        {
        }

        public EditarProduto(string idProduto, string idProdutoEc, string tituloEc, string descricaoEc, string jsonEcEnvio, string idGrupo, string grupo, string contVariacao, 
            bool contVarCor, bool contVarTamanho, bool contVarVoltagem, bool contVarSabor, string idProdutoV, string sku, 
            string idCor, string cor, string idTamanho, string tamanho, string idVoltagem, string voltagem, string idSabor, string sabor, string qdePendEc)
        {
            IdProduto = idProduto;
            IdProdutoEc = idProdutoEc;
            TituloEc = tituloEc;
            DescricaoEc = descricaoEc;
            JsonEcEnvio = jsonEcEnvio;
            IdGrupo = idGrupo;
            Grupo = grupo;
            ContVariacao = contVariacao;
            ContVarCor = contVarCor;
            ContVarTamanho = contVarTamanho;
            ContVarVoltagem = contVarVoltagem;
            ContVarSabor = contVarSabor;
            IdProdutoV = idProdutoV;
            Sku = sku;
            IdCor = idCor;
            Cor = cor;
            IdTamanho = idTamanho;
            Tamanho = tamanho;
            IdVoltagem = idVoltagem;
            Voltagem = voltagem;
            IdSabor = idSabor;
            Sabor = sabor;
            QdePendEc = qdePendEc;
        }

        public string IdProduto { get; set; }
        public string IdProdutoEc { get; set; }
        public string TituloEc { get; set; }
        public string DescricaoEc { get; set; }
        public string JsonEcEnvio { get; set; }
        public string IdGrupo { get; set; }
        public string Grupo { get; set; }
        public string ContVariacao { get; set; }
        public bool ContVarCor { get; set; }
        public bool ContVarTamanho { get; set; }
        public bool ContVarVoltagem { get; set; }
        public bool ContVarSabor { get; set; }
        public string IdProdutoV { get; set; }
        public string Sku { get; set; }
        public string IdCor { get; set; }
        public string Cor { get; set; }
        public string IdTamanho { get; set; }
        public string Tamanho { get; set; }
        public string IdVoltagem { get; set; }
        public string Voltagem { get; set; }
        public string IdSabor { get; set; }
        public string Sabor { get; set; }
        public string QdePendEc { get; set; }
    }
}
