﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Produtos
{
    public class ListaProduto
    {
        public ListaProduto()
        {
        }

        public ListaProduto(string idProdutoSGE, string idProdutoEC, string descricaoSGE, string descricaoEC, decimal precoEC, int quantidadeEC, string statusProduto, bool btnCadastrarAtivo, bool btnEditarAtivo, string urlAnuncioEC, bool btnAnuncioAtivo, bool btnReenviarAtivo, string corFonteAnuncio, bool btnExcluirAtivo)
        {
            IdProduto = idProdutoSGE;
            IdProdutoEcommerce = idProdutoEC;
            DescricaoSGE = descricaoSGE;
            DescricaoEcommerce = descricaoEC;
            Preco = precoEC;
            Quantidade = quantidadeEC;
            Status = statusProduto;
            CadastrarHabilitado = btnCadastrarAtivo;
            EditarHabilitador = btnEditarAtivo;
            UrlAnuncio = urlAnuncioEC;
            AnuncioHabilitado = btnAnuncioAtivo;
            ReenviarHabilitado = btnReenviarAtivo;
            CorFonteAnuncio = corFonteAnuncio;
            ExcluirHabilitado = btnExcluirAtivo;
        }

        public string IdProduto { get; set; }
        public string IdProdutoEcommerce { get; set; }
        public string DescricaoSGE { get; set; }
        public string DescricaoEcommerce { get; set; }
        public decimal Preco { get; set; }
        public int Quantidade { get; set; }
        public string Status { get; set; }
        public bool CadastrarHabilitado { get; set; }
        public bool EditarHabilitador { get; set; }
        public string UrlAnuncio { get; set; }
        public bool AnuncioHabilitado { get; set; }
        public bool ReenviarHabilitado { get; set; }
        public string CorFonteAnuncio { get; set; }
        public bool ExcluirHabilitado { get; set; }
    }
}
