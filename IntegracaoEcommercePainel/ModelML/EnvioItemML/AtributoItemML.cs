﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;


namespace IntegracaoPainel.EnvioItemML
{
    public class AtributoItemML
    {
        public AtributoItemML()
        {
        }

        public AtributoItemML(string idAtributo, string valorAtributo)
        {
            id = idAtributo;
            value_name = valorAtributo;
        }

        public string id { get; set; }
        public string value_name { get; set; }

    }
}
