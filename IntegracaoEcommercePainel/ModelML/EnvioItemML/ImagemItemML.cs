﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace IntegracaoPainel.EnvioItemML
{
    public class ImagemItemML
    {
        public ImagemItemML()
        {
        }

        public ImagemItemML(string urlImagem, string idProdutoV)
        {
            source = urlImagem;
            IdProdutoV = idProdutoV;
        }

        public string source { get; set; }
        [JsonIgnore]
        public string IdProdutoV { get; set; }
    }
}
