﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.EnvioItemML
{
    public class TermosDeVendaML
    {
        public TermosDeVendaML()
        {
        }

        public TermosDeVendaML(string idTermos, string valorTermos)
        {
            id = idTermos;
            value_name = valorTermos;
        }

        public string id { get; set; }
        public string value_name { get; set; }
    }
}
