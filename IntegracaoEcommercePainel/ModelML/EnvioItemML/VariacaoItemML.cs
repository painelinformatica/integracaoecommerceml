﻿/*using Newtonsoft.Json;*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IntegracaoPainel.EnvioItemML
{
    public class VariacaoItemML
    {
        public VariacaoItemML()
        {
        }

        public VariacaoItemML(List<CombinacaoAtributosML> combinarAtributos, decimal preco, int quantidadeDisponivel, List<AtributoItemML> atributos, int quantidadeVendida, string[] idImagem, long idVariacao, string idCatalogoProduto)
        {
            attribute_combinations = combinarAtributos;
            price = preco;
            available_quantity = quantidadeDisponivel;
            attributes = atributos;
            sold_quantity = quantidadeVendida;
            picture_ids = idImagem;
            id = idVariacao;
            catalog_product_id = idCatalogoProduto;
        }
        public List<CombinacaoAtributosML> attribute_combinations { get; set; }
        public decimal price { get; set; }
        public int available_quantity { get; set; }
        public List<AtributoItemML> attributes { get; set; }
        public int sold_quantity { get; set; }
        public string[] picture_ids { get; set; }

        [JsonIgnore]
        public long id { get; set; }
        public string catalog_product_id { get; set; }

    }
}
