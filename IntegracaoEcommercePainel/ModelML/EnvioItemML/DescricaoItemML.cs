﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.EnvioItemML
{
    public class DescricaoItemML
    {
        public DescricaoItemML()
        {
        }
        
        public DescricaoItemML(string descricaoItem)
        {
            plain_text = descricaoItem;
        }
        public string plain_text { get; set; }

    }
}
