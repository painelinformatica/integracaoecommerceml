﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
/*using System.Text.Json.Serialization;*/

namespace IntegracaoPainel.EnvioItemML
{
    public class CombinacaoAtributosML
    {
        public CombinacaoAtributosML()
        {
        }

        public CombinacaoAtributosML(string idAc, string nameAc ,string valueIdAc, string valueNameAc)
        {
            id = idAc;
            value_name = valueNameAc;
            name = nameAc;
            value_id = valueIdAc;
        }

        public string id { get; set; }
        public string value_name { get; set; }

        [JsonIgnore]
        public string value_id { get; set; }
        public string name { get; set; }


    }
}
