﻿using IntegracaoPainel.ModelML.EnvioItemML;
/*using Org.OpenAPITools.Model;*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace IntegracaoPainel.EnvioItemML
{
    public class ItemML : ItemMLPai
    {
        public ItemML()
        {
        }

        public ItemML(string titulo, string categoriaId, decimal preco, string moedaId, int quantidadeDisponivel, string modoDeVenda, string condicao, string tipoDeListaId, DescricaoItemML descricao,
            string linkVideoId, List<TermosDeVendaML> termosDeVenda, List<ImagemItemML> imagens, List<AtributoItemML> atributos, List<VariacaoItemML> variacoes) 
        {
            title = titulo;
            category_id = categoriaId;
            price = preco;
            currency_id = moedaId;
            available_quantity = quantidadeDisponivel;
            buying_mode = modoDeVenda;
            condition = condicao;
            listing_type_id = tipoDeListaId;
            description = descricao;
            video_id = linkVideoId;
            sale_terms = termosDeVenda;
            pictures = imagens;
            attributes = atributos;
            variations = variacoes;
        }

        public List<VariacaoItemML> variations { get; set; }
    }
}
