﻿using IntegracaoPainel.EnvioItemML;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.EnvioItemML
{
    public class ItemMLPai
    {
        public ItemMLPai()
        {
        }

        public ItemMLPai(string titulo, string categoriaId, decimal preco, string moedaId, int quantidadeDisponivel, string modoDeVenda, string condicao, string tipoDeListaId, DescricaoItemML descricao,
            string linkVideoId, List<TermosDeVendaML> termosDeVenda, List<ImagemItemML> imagens, List<AtributoItemML> atributos)
        {
            title = titulo;
            category_id = categoriaId;
            price = preco;
            currency_id = moedaId;
            available_quantity = quantidadeDisponivel;
            buying_mode = modoDeVenda;
            condition = condicao;
            listing_type_id = tipoDeListaId;
            description = descricao;
            video_id = linkVideoId;
            sale_terms = termosDeVenda;
            pictures = imagens;
            attributes = atributos;
        }

        public string title { get; set; }
        public string category_id { get; set; }
        public decimal price { get; set; }
        public string currency_id { get; set; }
        public int available_quantity { get; set; }
        public string buying_mode { get; set; }
        public string condition { get; set; }
        public string listing_type_id { get; set; }
        public DescricaoItemML description { get; set; }
        public string video_id { get; set; }
        public List<TermosDeVendaML> sale_terms { get; set; }
        public List<ImagemItemML> pictures { get; set; }
        public List<AtributoItemML> attributes { get; set; }
    }
}
