﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace IntegracaoPainel.EnvioItemML
{
    public class ImagemItemIdML
    {
        public ImagemItemIdML()
        {
        }

        public ImagemItemIdML(string idImage, string idProdutoV)
        {
            id = idImage;
            IdProdutoV = idProdutoV;
        }

        public string id { get; set; }
        [JsonIgnore]
        public string IdProdutoV { get; set; }
    }
}
