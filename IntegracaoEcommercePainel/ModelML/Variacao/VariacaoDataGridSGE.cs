﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Variacao
{
    public class VariacaoDataGridSGE
    {
        public VariacaoDataGridSGE()
        {
        }

        public VariacaoDataGridSGE(string tipoVariacao, string variacao, decimal qtdEstoque,string idProdutoV ,string sku, bool produtoSelecionado, bool variacaoDesativada)
        {
            TipoVariacao = tipoVariacao;
            Variacao = variacao;
            QuantidadeVariacao = qtdEstoque;
            IdProdutoV = idProdutoV;
            SKU = sku;
            ItemSelecionadoEcommerce = produtoSelecionado;
            VariacaoAtivada = variacaoDesativada;
        }

        public string TipoVariacao { get; set; }
        public string Variacao { get; set; }
        public decimal QuantidadeVariacao { get; set; }
        public string IdProdutoV { get; set; }
        public string SKU { get; set; }
        public bool ItemSelecionadoEcommerce { get; set; }
        public bool VariacaoAtivada { get; set; }
    }
}
