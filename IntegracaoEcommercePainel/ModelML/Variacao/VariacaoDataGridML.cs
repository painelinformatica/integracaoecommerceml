﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Variacao
{
    public class VariacaoDataGridML
    {
        public VariacaoDataGridML()
        {
        }

        public VariacaoDataGridML(string tipoAtributo, string atributo)
        {
            TipoAtributo = tipoAtributo;
            Atributo = atributo;
        }

        public string TipoAtributo { get; set; }
        public string Atributo { get; set; }
    }
}
