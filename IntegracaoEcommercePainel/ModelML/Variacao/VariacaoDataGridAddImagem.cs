﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Variacao
{
    public class VariacaoDataGridAddImagem
    {
        public VariacaoDataGridAddImagem()
        {
        }

        public VariacaoDataGridAddImagem(string variacao)
        {
            Variacao = variacao;
        }

        public string Variacao { get; set; }
    }
}
