﻿using IntegracaoPainel.EnvioItemML;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Variacao
{
    public class VariacaoRetornoAtributo
    {
        public VariacaoRetornoAtributo()
        {
        }

        public VariacaoRetornoAtributo(List<VariacaoItemML> variacao)
        {
            variations = variacao;
        }

        public List<VariacaoItemML> variations { get; set; }
    }
}
