﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Payments
{
    public class Payments
    {
        public Payments()
        {
        }

        public Payments(long idPay, string idSite, string dataCriacaoPay, string dataAprovacaoPay, string dataLiberacaoDinheiro, string nomeProdutoVendido, string moeda, decimal motanteTransacao,
                        decimal montanteTotalPago, decimal custoEnvio, decimal montanteCupom, string statusPay, string detalheStatus, decimal valorParcela, string tipoPagamento, string bandeiraCartao, 
                        string referenciaExterna, string idOrdem)
        {
            id = idPay;
            site_id = idSite;
            date_created = dataCriacaoPay;
            date_approved = dataAprovacaoPay;
            money_release_date = dataLiberacaoDinheiro;
            reason = nomeProdutoVendido;
            currency_id = moeda;
            transaction_amount = motanteTransacao;
            total_paid_amount = montanteTotalPago;
            shipping_cost = custoEnvio;
            coupon_amount = montanteCupom;
            status = statusPay;
            status_detail = detalheStatus;
            installment_amount = valorParcela;
            payment_type = tipoPagamento;
            payment_method_id = bandeiraCartao;
            external_reference = referenciaExterna;
            order_id = idOrdem;
        }

        public long id { get; set; }
        public string site_id { get; set; }
        public string date_created { get; set; }
        public string date_approved { get; set; }
        public string money_release_date { get; set; }
        public string reason { get; set; }
        public string currency_id { get; set; }
        public decimal transaction_amount { get; set; }
        public decimal total_paid_amount { get; set; }
        public decimal shipping_cost { get; set; }
        public decimal coupon_amount { get; set; }
        public string status { get; set; }
        public string status_detail { get; set; }
        public decimal installment_amount { get; set; }
        public string payment_type { get; set; }
        public string payment_method_id { get; set; }
        public string external_reference { get; set; }
        public string order_id { get; set; }

    }
}
