﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class RequestOrder
    {
        public RequestOrder()
        {
        }

        public RequestOrder(string retorno, string mudanca)
        {
            Retorno = retorno;
            change = mudanca;
        }

        public string Retorno { get; set; }
        public string change { get; set; }
    }
}
