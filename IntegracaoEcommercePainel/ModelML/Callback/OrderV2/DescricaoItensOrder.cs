﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class DescricaoItensOrder
    {
        public DescricaoItensOrder()
        {
        }

        public DescricaoItensOrder(string idItem, string tituloItem, string categoriaItem, string idVariacaoItem, string campoPersonalizadoVendedor, string garantiaItem, string condicaoItem,string skuItem, string precoGlobalItem)
        {
            id = idItem;
            title = tituloItem;
            category_id = categoriaItem;
            variation_id = idVariacaoItem;
            seller_custom_field = campoPersonalizadoVendedor;
            warranty = garantiaItem;
            condition = condicaoItem;
            seller_sku = skuItem;
            global_price = precoGlobalItem;
        }

        public string id { get; set; }
        public string title { get; set; }
        public string category_id { get; set; }
        public string variation_id { get; set; }
        public string seller_custom_field { get; set; }
        public string warranty { get; set; }
        public string condition { get; set; }
        public string seller_sku { get; set;  }
        public string global_price { get; set; }

    }
}
