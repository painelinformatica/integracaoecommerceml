﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class OrdersV2
    {
        public OrdersV2()
        {
        }

        public OrdersV2(long idOrdem, string dataCriacao, string dataFinalizada, string ultimaAtualizacao, FeedbackOrder feedbackOrdem, List<MediacaoOrder> mediacao, string comentario, string idPacote, 
                        string idPegar, RequestOrder requisaoOrdem, string itemFull, decimal valorProduto, decimal totalPago, CuponOrder cupomDesconto, string dataExpiracao, List<ItemsOrder> itensOrder, string moeda, 
                        List<PagamentoOrder> pagamento, string statusOrdem, string detalheStatusOrdem, String[]tagsOrdem, CompradorOrder comprador, VendedorOrder vendedor, TaxasOrder taxas)
        {
            id = idOrdem;
            date_created = dataCriacao;
            date_closed = dataFinalizada;
            last_updated = ultimaAtualizacao;
            feedback = feedbackOrdem;
            mediations = mediacao;
            comments = comentario;
            pack_id = idPacote;
            pickup_id = idPegar;
            order_request = requisaoOrdem;
            fulfilled = itemFull;
            total_amount = valorProduto;
            paid_amount = totalPago;
            coupon = cupomDesconto;
            expiration_date = dataExpiracao;
            order_items = itensOrder;
            currency_id = moeda;
            payments = pagamento;
            status = statusOrdem;
            status_detail = detalheStatusOrdem;
            tags = tagsOrdem;
            buyer = comprador;
            seller = vendedor;
            taxes = taxas;
        }

        public long id { get; set; }
        public string date_created { get; set; }
        public string date_closed { get; set; }
        public string last_updated { get; set; }
        public FeedbackOrder feedback { get; set; }
        public List<MediacaoOrder> mediations { get; set; }
        public string comments { get; set; }
        public string pack_id { get; set; }
        public string pickup_id { get; set; }
        public RequestOrder order_request { get; set; }
        public string fulfilled { get; set; }
        public decimal total_amount { get; set; }
        public decimal paid_amount { get; set; }
        public CuponOrder coupon { get; set; }
        public string expiration_date { get; set; }
        public List<ItemsOrder> order_items { get; set; }
        public string currency_id { get; set; }
        public List<PagamentoOrder> payments { get; set; }
        public string status { get; set; }
        public string status_detail { get; set; }
        public String[] tags { get; set; }
        public CompradorOrder buyer { get; set; }
        public VendedorOrder seller { get; set; }
        public TaxasOrder taxes { get; set; }
    }
}
