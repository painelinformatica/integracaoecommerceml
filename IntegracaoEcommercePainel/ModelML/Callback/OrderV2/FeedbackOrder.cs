﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class FeedbackOrder
    {
        public FeedbackOrder()
        {
        }

        public FeedbackOrder(SaleFeedbackOrder venda, string compra)
        {
            sale = venda;
            purchase = compra;
        }

        public SaleFeedbackOrder sale { get; set; }
        public string purchase { get; set; }
    }
}
