﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class CuponOrder
    {
        public CuponOrder()
        {
        }

        public CuponOrder(string idCupon, decimal valorCupom)
        {
            id = idCupon;
            amount = valorCupom;
        }

        public string id { get; set; }
        public decimal amount { get; set; }
    }
}
