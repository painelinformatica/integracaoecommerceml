﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class PessoaOrdem
    {
        public PessoaOrdem()
        {
        }

        public PessoaOrdem(long idPessoa, string nicknamePessoa, string emailPessoa, string primeiroNome, string ultimoNome)
        {
            id = idPessoa;
            nickname = nicknamePessoa;
            email = emailPessoa;
            first_name = primeiroNome;
            last_name = ultimoNome;
        }

        public long id { get; set; }
        public string nickname { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
    }
}
