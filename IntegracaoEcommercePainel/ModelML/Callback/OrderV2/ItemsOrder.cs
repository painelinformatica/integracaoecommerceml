﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class ItemsOrder
    {
        public ItemsOrder()
        {
        }

        public ItemsOrder(DescricaoItensOrder itemDescricao, int quantidade, decimal precoUnitario, decimal precoCompletoUnitario, string moeda, decimal comissaoVenda, string tipoDeListaMercadoLivre)
        {
            item = itemDescricao;
            quantity = quantidade;
            unit_price = precoUnitario;
            full_unit_price = precoCompletoUnitario;
            currency_id = moeda;
            sale_fee = comissaoVenda;
            listing_type_id = tipoDeListaMercadoLivre;
        }

        public DescricaoItensOrder item { get; set; }
        public int quantity { get; set; }
        public decimal unit_price { get; set; }
        public decimal full_unit_price { get; set; }
        public string currency_id { get; set; }
        public decimal sale_fee { get; set; }
        public string listing_type_id { get; set; }
    }
}
