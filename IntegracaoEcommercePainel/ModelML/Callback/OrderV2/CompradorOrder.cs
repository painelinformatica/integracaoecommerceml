﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class CompradorOrder : PessoaOrdem
    {
        public CompradorOrder()
        {
        }

        public CompradorOrder(long idComprador, string nicknameComprador, string emailComprador, string nomeComprador, string ultimoNomeComprador, InformacaoFaturamentoOrder informacaoFaturamento, TelefoneOrder telefone)
        {
            id = idComprador;
            nickname = nicknameComprador;
            email = emailComprador;
            first_name = nomeComprador;
            last_name = ultimoNomeComprador;
            billing_info = informacaoFaturamento;
            phone = telefone;
        }

        public InformacaoFaturamentoOrder billing_info { get; set; }
        public TelefoneOrder phone { get; set; }
    }
}
