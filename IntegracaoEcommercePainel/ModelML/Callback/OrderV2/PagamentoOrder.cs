﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class PagamentoOrder
    {
        public PagamentoOrder()
        {
        }

        public PagamentoOrder(long idPagamento, long idOrdem, long idPagadora, long idCartao, string idSite, string razao, string idMetodoPagamento, string moeda, int parcelas, string idEmissora, string idCupom, string tipoOperacao,
                              string tipoDePagamento, string statusPagamento, string codigoStatusPagamento, string detalheStatusPagamento, decimal montanteTransacao, decimal montanteTaxas, decimal custoDeVenda, decimal montanteCupom,
                              decimal montantePagoAmais, decimal valorTotalPago, decimal valorDaParcela, string dataAprovacaoPagamento, string codigoAutorizacao, string idOrdemTransacao, string dataCriacaoPagamento, string ultimaModicacaoPagamento)
        {
            id = idPagamento;
            order_id = idOrdem;
            payer_id = idPagadora;
            card_id = idCartao;
            site_id = idSite;
            reason = razao;
            payment_method_id = idMetodoPagamento;
            currency_id = moeda;
            installments = parcelas;
            issuer_id = idEmissora;
            coupon_id = idCupom;
            operation_type = tipoOperacao;
            payment_type = tipoDePagamento;
            status = statusPagamento;
            status_code = codigoStatusPagamento;
            status_detail = detalheStatusPagamento;
            transaction_amount = montanteTransacao;
            taxes_amount = montanteTaxas;
            shipping_cost = custoDeVenda;
            coupon_amount = montanteCupom;
            overpaid_amount = montantePagoAmais;
            total_paid_amount = valorTotalPago;
            installment_amount = valorDaParcela;
            date_approved = dataAprovacaoPagamento;
            authorization_code = codigoAutorizacao;
            transaction_order_id = idOrdemTransacao;
            date_created = dataCriacaoPagamento;
            date_last_modified = ultimaModicacaoPagamento;
        }

        public long id { get; set; }
        public long order_id { get; set; }
        public long payer_id { get; set; }
        public long card_id { get; set; }
        public string site_id { get; set; }
        public string reason { get; set; }
        public string payment_method_id { get; set; }
        public string currency_id { get; set; }
        public int installments { get; set; }
        public string issuer_id { get; set; }
        public string coupon_id { get; set; }
        public string operation_type { get; set; }
        public string payment_type { get; set; }
        public string status { get; set; }
        public string status_code { get; set; }
        public string status_detail { get; set; }
        public decimal transaction_amount { get; set; }
        public decimal taxes_amount { get; set; }
        public decimal shipping_cost { get; set; }
        public decimal coupon_amount { get; set; }
        public decimal overpaid_amount { get; set; }
        public decimal total_paid_amount { get; set; }
        public decimal installment_amount { get; set; }
        public string date_approved { get; set; }
        public string authorization_code { get; set; }
        public string transaction_order_id { get; set; }
        public string date_created { get; set; }
        public string date_last_modified { get; set; }

    }
}
