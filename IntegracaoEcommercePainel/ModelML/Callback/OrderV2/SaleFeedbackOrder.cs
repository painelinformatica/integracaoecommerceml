﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class SaleFeedbackOrder
    {
        public SaleFeedbackOrder()
        {
        }

        public SaleFeedbackOrder(long idSale, string dataCriacao, bool mercadoFull, string avaliacao, string statusSales, string reacao)
        {
            id = idSale;
            date_created = dataCriacao;
            fulfilled = mercadoFull;
            rating = avaliacao;
            status = statusSales;
            reason = reacao;
        }

        public long id { get; set; }
        public string date_created { get; set; }
        public bool fulfilled { get; set; }
        public string rating { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
    }
}
