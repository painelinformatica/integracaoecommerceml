﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class InformacaoFaturamentoOrder
    {
        public InformacaoFaturamentoOrder()
        {
        }

        public InformacaoFaturamentoOrder(string tipoDocumento, string numeroDocumento)
        {
            doc_type = tipoDocumento;
            doc_number = numeroDocumento;
        }

        public string doc_type { get; set; }
        public string doc_number { get; set; }
    }
}
