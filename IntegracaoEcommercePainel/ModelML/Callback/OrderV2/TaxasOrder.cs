﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class TaxasOrder
    {
        public TaxasOrder()
        {
        }

        public TaxasOrder(string valorTaxa, string moeda)
        {
            amount = valorTaxa;
            currency_id = moeda;
        }

        public string amount { get; set; }
        public string currency_id { get; set; }
    }
}
