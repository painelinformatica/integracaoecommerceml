﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class VendedorOrder : PessoaOrdem
    {
        public VendedorOrder()
        {
        }

        public VendedorOrder(long idVendedor, string nicknameVendedor, string emailVendedor, string nomeVendedor, string ultimoNomeVendedor, TelefoneOrder telefonePrincipal, TelefoneOrder telefoneAlternativo)
        {
            id = idVendedor;
            nickname = nicknameVendedor;
            email = emailVendedor;
            first_name = nomeVendedor;
            last_name = ultimoNomeVendedor;
            phone = telefonePrincipal;
            alternative_phone = telefoneAlternativo;
        }

        public TelefoneOrder phone { get; set; }
        public TelefoneOrder alternative_phone { get; set; }
    }
}
