﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class MediacaoOrder
    {
        public MediacaoOrder()
        {
        }

        public MediacaoOrder(long idMediacao, string statusMediacao, string dataCriacao)
        {
            id = idMediacao;
            status = statusMediacao;
            date_created = dataCriacao;
        }

        public long id { get; set; }
        public string status { get; set; }
        public string date_created { get; set; }
    }
}
