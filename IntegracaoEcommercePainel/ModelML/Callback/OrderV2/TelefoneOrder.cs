﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.OrderV2
{
    public class TelefoneOrder
    {
        public TelefoneOrder()
        {
        }

        public TelefoneOrder(string extensao, string codigoArea, string numero)
        {
            extension = extensao;
            area_code = codigoArea;
            number = numero;
        }

        public string extension { get; set; }
        public string area_code { get; set; }
        public string number { get; set; }
    }
}
