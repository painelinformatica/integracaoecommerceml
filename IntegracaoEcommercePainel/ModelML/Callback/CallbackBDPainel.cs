﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback
{
    public class CallbackBDPainel
    {
        public CallbackBDPainel()
        {
        }

        public CallbackBDPainel(string topico, string recurso, string idUsuario, string idAplicacao, string horaRecebida)
        {
            topic = topico;
            resource = recurso;
            user_id = idUsuario;
            application_id = idAplicacao;
            received = horaRecebida;
        }

        public string topic { get; set; }
        public string resource { get; set; }
        public string user_id { get; set; }
        public string application_id { get; set; }
        public string received { get; set; }
    }
}
