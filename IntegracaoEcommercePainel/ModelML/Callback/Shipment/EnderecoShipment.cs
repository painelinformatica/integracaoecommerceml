﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class EnderecoShipment
    {
        public EnderecoShipment()
        {
        }

        public EnderecoShipment(string idEndereco, string logradouroEndereco)
        {
            id = idEndereco;
            name = logradouroEndereco;
        }

        public string id { get; set; }
        public string name { get; set; }
    }
}
