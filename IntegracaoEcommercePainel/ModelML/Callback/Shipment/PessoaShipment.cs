﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class PessoaShipment
    {
        public PessoaShipment()
        {
        }

        public PessoaShipment(long idPessoaShip, string enderecoEmUmaLinha,  string nomeRua, string numero, string CEP, EnderecoShipment cidade, EnderecoShipment estado, EnderecoShipment pais, EnderecoShipment bairro,
                              EnderecoShipment municipio, string agencia, String[] tipos, string tipoEndereco, string nomePessoa, string telefoneContato)
        {
            id = idPessoaShip;
            address_line = enderecoEmUmaLinha;
            street_name = nomeRua;
            street_number = numero;
            zip_code = CEP;
            city = cidade;
            state = estado;
            country = pais;
            neighborhood = bairro;
            municipality = municipio;
            agency = agencia;
            types = tipos;
            delivery_preference = tipoEndereco;
            receiver_name = nomePessoa;
            receiver_phone = telefoneContato;
        }

        public long id { get; set; }
        public string address_line { get; set; }
        public string street_name { get; set; }
        public string street_number { get; set; }
        public string zip_code { get; set; }
        public EnderecoShipment city { get; set; }
        public EnderecoShipment state { get; set; }
        public EnderecoShipment country { get; set; }
        public EnderecoShipment neighborhood { get; set; }
        public EnderecoShipment municipality { get; set; }
        public string agency { get; set; }
        public String[] types { get; set; }
        public string delivery_preference { get; set; }
        public string receiver_name { get; set; }
        public string receiver_phone { get; set; }
    }
}
