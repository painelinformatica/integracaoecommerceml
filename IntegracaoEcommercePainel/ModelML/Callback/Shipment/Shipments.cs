﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class Shipments
    {
        public Shipments()
        {
        }

        public Shipments(long idShip, string modoEnvio, string criadoPor, long idOrdem, decimal custoOrdem, decimal custoEnvio, string idSite, string statusShip, string subStatusShip, HistoricoStatusShipment historicoStatusShip,
                         string dataCriacaoShip, string ultimaModificaoShip, string numeroRastreioShip, string metodoEnvioShip, string idServicoEnvio, long idVendedor, PessoaShipment enderecoVendedor, long idComprador,
                         PessoaShipment enderecoComprador, List<ItemShipment> itensVendidos, OpcaoEnvioShipment opcaoEnvio)
        {
            id = idShip;
            mode = modoEnvio;
            created_by = criadoPor;
            order_id = idOrdem;
            order_cost = custoOrdem;
            base_cost = custoEnvio;
            site_id = idSite;
            status = statusShip;
            substatus = subStatusShip;
            status_history = historicoStatusShip;
            date_created = dataCriacaoShip;
            last_updated = ultimaModificaoShip;
            tracking_method = numeroRastreioShip;
            tracking_method = metodoEnvioShip;
            service_id = idServicoEnvio;
            sender_id = idVendedor;
            sender_address = enderecoVendedor;
            receiver_id = idComprador;
            receiver_address = enderecoComprador;
            shipping_items = itensVendidos;
            shipping_option = opcaoEnvio;
        }

        public long id { get; set; }
        public string mode { get; set; }
        public string created_by { get; set; }
        public long order_id { get; set; }
        public decimal order_cost { get; set; }
        public decimal base_cost { get; set; }
        public string site_id { get; set; }
        public string status { get; set; }
        public string substatus { get; set; }
        public HistoricoStatusShipment status_history { get; set; }
        public string date_created { get; set; }
        public string last_updated { get; set; }
        public string tracking_number { get; set; }
        public string tracking_method { get; set; }
        public string service_id { get; set; }
        public long sender_id { get; set; }
        public PessoaShipment sender_address { get; set; }
        public long receiver_id { get; set; }
        public PessoaShipment receiver_address { get; set; }
        public List<ItemShipment> shipping_items { get; set; }
        public OpcaoEnvioShipment shipping_option { get; set; }
    }
}
