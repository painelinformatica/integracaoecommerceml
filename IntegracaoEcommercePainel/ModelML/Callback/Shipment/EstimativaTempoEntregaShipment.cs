﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class EstimativaTempoEntregaShipment
    {
        public EstimativaTempoEntregaShipment()
        {
        }

        public EstimativaTempoEntregaShipment(string tipo, string data, string unidade, DeslocamentoOpcaoEnvioShipment deslocamento, string pagamentoAntes, int remessa, int tratamento)
        {
            type = tipo;
            date = data;
            unit = unidade;
            offset = deslocamento;
            pay_before = pagamentoAntes;
            shipping = remessa;
            handling = tratamento;
        }
        public string type { get; set; }
        public string date { get; set; }
        public string unit { get; set; }
        public DeslocamentoOpcaoEnvioShipment offset { get; set; }
        public string pay_before { get; set; }
        public int shipping { get; set; }
        public int handling { get; set; }

    }
}
