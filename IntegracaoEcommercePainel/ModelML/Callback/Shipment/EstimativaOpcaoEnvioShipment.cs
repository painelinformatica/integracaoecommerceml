﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class EstimativaOpcaoEnvioShipment
    {
        public EstimativaOpcaoEnvioShipment()
        {
        }

        public EstimativaOpcaoEnvioShipment(string data, int deslocamento)
        {
            date = data;
            offset = deslocamento;
        }

        public string date { get; set; }
        public int offset { get; set; }
    }
}
