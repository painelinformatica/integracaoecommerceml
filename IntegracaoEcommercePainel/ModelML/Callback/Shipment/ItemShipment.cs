﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class ItemShipment
    {
        public ItemShipment()
        {
        }

        public ItemShipment(string idItemShip, string descricaoItem, int quantidade)
        {
            id = idItemShip;
            description = descricaoItem;
            quantity = quantidade;
        }

        public string id { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
    }
}
