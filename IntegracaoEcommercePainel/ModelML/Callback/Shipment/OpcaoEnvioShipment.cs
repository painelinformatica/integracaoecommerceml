﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class OpcaoEnvioShipment
    {
        public OpcaoEnvioShipment()
        {
        }

        public OpcaoEnvioShipment(long idOpcaoEnvioShip, long idMetodoEnvio, string nomeEnvio, string moeda, decimal custoLista, decimal custo, string tipoEntrega, EstimativaTempoEntregaShipment estimativaTempoEnvio,
                                  EstimativaOpcaoEnvioShipment tempoEntregaLimite, EstimativaOpcaoEnvioShipment tempoEntregaFinal, EstimativaOpcaoEnvioShipment tempoEntregaExtendido)
        {
            id = idOpcaoEnvioShip;
            shipping_method_id = idMetodoEnvio;
            name = nomeEnvio;
            currency_id = moeda;
            list_cost = custoLista;
            cost = custo;
            delivery_type = tipoEntrega;
            estimated_delivery_time = estimativaTempoEnvio;
            estimated_delivery_limit = tempoEntregaLimite;
            estimated_delivery_final = tempoEntregaFinal;
            estimated_delivery_extended = tempoEntregaExtendido;
        }

        public long id { get; set; }
        public long shipping_method_id { get; set; }
        public string name { get; set; }
        public string currency_id { get; set; }
        public decimal list_cost { get; set; }
        public decimal cost { get; set; }
        public string delivery_type { get; set; }
        public EstimativaTempoEntregaShipment estimated_delivery_time { get; set; }
        public EstimativaOpcaoEnvioShipment estimated_delivery_limit { get; set; }
        public EstimativaOpcaoEnvioShipment estimated_delivery_final { get; set; }
        public EstimativaOpcaoEnvioShipment estimated_delivery_extended { get; set; }
    }
}
