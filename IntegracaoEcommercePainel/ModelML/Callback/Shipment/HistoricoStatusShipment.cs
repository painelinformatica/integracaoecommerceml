﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Callback.Shipment
{
    public class HistoricoStatusShipment
    {
        public HistoricoStatusShipment()
        {
        }

        public HistoricoStatusShipment(string dataCancelamento, string dataEntrega, string dataPrimeiraVisita, string manipulacaoData, string dataNaoEntregue, string dataProntoParaEnvio, string dataEnvio, string dataRetorno)
        {
            date_cancelled = dataCancelamento;
            date_delivered = dataEntrega;
            date_first_visit = dataPrimeiraVisita;
            date_handling = manipulacaoData;
            date_not_delivered = dataNaoEntregue;
            date_ready_to_ship = dataProntoParaEnvio;
            date_shipped = dataEnvio;
            date_returned = dataRetorno;
        }
        public string date_cancelled { get; set; }
        public string date_delivered { get; set; }
        public string date_first_visit { get; set; }
        public string date_handling { get; set; }
        public string date_not_delivered { get; set; }
        public string date_ready_to_ship { get; set; }
        public string date_shipped { get; set; }
        public string date_returned { get; set; }
    }
}

