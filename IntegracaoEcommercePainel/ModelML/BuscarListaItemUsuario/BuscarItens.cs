﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.BuscarListaItemUsuario
{
    public class BuscarItens
    {
        public BuscarItens()
        {
        }

        public BuscarItens(long idUsuario, String[] listaItens)
        {
            seller_id = idUsuario;
            results = listaItens;
        }

        public long seller_id { get; set; }
        public String[] results { get; set; }
    }
}
