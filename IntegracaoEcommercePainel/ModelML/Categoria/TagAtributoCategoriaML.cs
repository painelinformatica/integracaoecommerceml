﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class TagAtributoCategoriaML
    {
        public TagAtributoCategoriaML()
        {
        }

        public TagAtributoCategoriaML(bool identificarItem, bool requirido)
        {
            catalog_required = identificarItem;
            required = requirido;
        }

        public bool catalog_required { get; set; }
        public bool required { get; set; }
    }
}
