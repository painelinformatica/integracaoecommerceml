﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class AtributosPreditorCategoriaML
    {
        public AtributosPreditorCategoriaML()
        {
        }

        public AtributosPreditorCategoriaML(string idAPC, string nomeAPC, string valorIdAPC, string valorNomeAPC)
        {
            id = idAPC;
            name = nomeAPC;
            value_id = valorIdAPC;
            value_name = valorNomeAPC;
        }

        public string id { get; set; }
        public string name { get; set; }
        public string value_id { get; set; }
        public string value_name { get; set; }
    }
}
