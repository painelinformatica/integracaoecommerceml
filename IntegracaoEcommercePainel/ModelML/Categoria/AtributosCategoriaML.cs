﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class AtributosCategoriaML
    {
        public AtributosCategoriaML()
        {
        }

        public AtributosCategoriaML(string idAC, string nomeAC, TagAtributoCategoriaML tagAC, string hierarquiaAC, int relevanciaAC, string tipoDeValorAC, int tamanhoMaximoValorAC, List<ValorAtributoCategoriaML> valoresAC, string tipoAC, List<ValorAtributoCategoriaML> unidadePermitidas)
        {
            id = idAC;
            name = nomeAC;
            tags = tagAC;
            hierarchy = hierarquiaAC;
            relevance = relevanciaAC;
            value_type = tipoDeValorAC;
            value_max_length = tamanhoMaximoValorAC;
            values = valoresAC;
            type = tipoAC;
            allowed_units = unidadePermitidas;
        }
        public string id { get; set; }
        public string name { get; set; }
        public TagAtributoCategoriaML tags { get; set; }
        public string hierarchy { get; set; }
        public int relevance { get; set; }
        public string value_type { get; set; }
        public int value_max_length { get; set; }
        public List<ValorAtributoCategoriaML> values { get; set; }
        public string type { get; set; }
        public List<ValorAtributoCategoriaML> allowed_units { get; set; }
    }
}
