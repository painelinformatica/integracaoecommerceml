﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class ValorAtributoCategoriaML
    {
        public ValorAtributoCategoriaML()
        {
        }

        public ValorAtributoCategoriaML(string idVAC, string nomeVAC)
        {
            id = idVAC;
            name = nomeVAC;
        }
        public string id { get; set; }
        public string name { get; set; }
    }
}
