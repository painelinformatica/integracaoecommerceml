﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class TipoGarantiaCategoriaML
    {
        public TipoGarantiaCategoriaML()
        {
        }

        public TipoGarantiaCategoriaML(string idTGC, string nomeTGC, TagAtributoCategoriaML tagsTGC, string hierarquiaTGC, int relevanciaTGC, string tipoDeValorTGC, List<ValorAtributoCategoriaML> valorTGC,int valorTamanhoMaxTGC, List<ValorAtributoCategoriaML> unidadePermitida, string unidadePadrao)
        {
            id = idTGC;
            name = nomeTGC;
            tags = tagsTGC;
            hierarchy = hierarquiaTGC;
            relevance = relevanciaTGC;
            value_type = tipoDeValorTGC;
            values = valorTGC;
            value_max_length = valorTamanhoMaxTGC;
            allowed_units = unidadePermitida;
            default_unit = unidadePadrao;
        }

        public string id { get; set; }
        public string name { get; set; }
        public TagAtributoCategoriaML tags { get; set; }
        public string hierarchy { get; set; }
        public int relevance { get; set; }
        public string value_type { get; set; }
        public List<ValorAtributoCategoriaML> values { get; set; }
        public int value_max_length { get; set; }
        public List<ValorAtributoCategoriaML> allowed_units { get; set; }
        public string default_unit { get; set; }
    }
}
