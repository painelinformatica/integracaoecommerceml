﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class CategoriaML
    {
        public CategoriaML()
        {
        }

        public CategoriaML(string idML, string nameML, List<CategoriaML> paiML,List<CategoriaML> filhosML, CfgCategoriasML cfgCategoriaML)
        {
            id = idML;
            name = nameML;
            path_from_root = paiML;
            children_categories = filhosML;
            settings = cfgCategoriaML;
        }

        public string id { get; set; }
        public string name { get; set; }
        public List<CategoriaML> path_from_root { get; set; }
        public List<CategoriaML> children_categories { get; set; }
        public CfgCategoriasML settings { get; set; }
    }
}
