﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class CfgCategoriasML
    {
        public CfgCategoriasML()
        {
        }

        public CfgCategoriasML(string statusML)
        {
            status = statusML;
        }

        public string status { get; set; }
    }
}
