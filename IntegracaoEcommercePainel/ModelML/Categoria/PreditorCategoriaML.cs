﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.Categoria
{
    public class PreditorCategoriaML
    {
        public PreditorCategoriaML()
        {
        }

        public PreditorCategoriaML(string domaindIdPC, string domainNamePC, string categoriaIdPC, string categoriaNamePC, List<AtributosPreditorCategoriaML> atributosPC)
        {
            domain_id = domaindIdPC;
            domain_name = domainNamePC;
            category_id = categoriaIdPC;
            category_name = categoriaNamePC;
            attributes = atributosPC;
        }

        public string domain_id { get; set; }
        public string domain_name { get; set; }
        public string category_id { get; set; }
        public string category_name { get; set; }
        public List<AtributosPreditorCategoriaML> attributes { get; set; }
    }
}
