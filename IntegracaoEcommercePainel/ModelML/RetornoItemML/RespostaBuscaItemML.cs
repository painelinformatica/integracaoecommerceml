﻿using IntegracaoPainel.ModelML.RetornoItemML;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.RetornoItemML
{
    public class RespostaBuscaItemML
    {
        public RespostaBuscaItemML()
        {
        }

        public RespostaBuscaItemML(int codigoResposta, ItemMLRetornoIntegrar itemML)
        {
            code = codigoResposta;
            body = itemML;
        }

        public int code { get; set; }
        public ItemMLRetornoIntegrar body { get; set; }
    }
}
