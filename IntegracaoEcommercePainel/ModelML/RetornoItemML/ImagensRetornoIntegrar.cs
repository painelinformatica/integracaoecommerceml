﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoEcommercePainel.ModelML.RetornoItemML
{
    public class ImagensRetornoIntegrar
    {
        public ImagensRetornoIntegrar()
        {
        }

        public ImagensRetornoIntegrar(string idImagem, string urlSegura)
        {
            id = idImagem;
            secure_url = urlSegura;
        }

        public string id { get; set; }
        public string secure_url { get; set; }
    }
}
