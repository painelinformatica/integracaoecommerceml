﻿using IntegracaoPainel.EnvioItemML;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.RetornoItemML
{
    public class ItemMLRetorno
    {
        public ItemMLRetorno()
        {
        }

        public ItemMLRetorno(string idItem, string titulo, string idVendedor, string idCategoria, string urlProdutoEcommerce, List<AvisoRetorno> atencao, string tipoStatus, decimal precoML, List<VariacaoItemML>variacao, int quantidade, int quantidadeInicial, int quantidadeVendida)
        {
            id = idItem;
            title = titulo;
            seller_id = idVendedor;
            category_id = idCategoria;
            permalink = urlProdutoEcommerce;
            warnings = atencao;
            status = tipoStatus;
            price = precoML;
            variations = variacao;
            available_quantity = quantidade;
            initial_quantity = quantidadeInicial;
            sold_quantity = quantidadeVendida;
        }

        public string id { get; set; }
        public string title { get; set; }
        public string seller_id { get; set; }
        public string category_id { get; set; }
        public string permalink { get; set; }
        public List<AvisoRetorno> warnings { get; set; }
        public string status { get; set; }
        public decimal price { get; set; }
        public List<VariacaoItemML> variations { get; set; }
        public int available_quantity { get; set; }
        public int initial_quantity { get; set; }
        public int sold_quantity { get; set; }

    }
}
