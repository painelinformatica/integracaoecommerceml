﻿using IntegracaoEcommercePainel.ModelML.RetornoItemML;
using IntegracaoPainel.EnvioItemML;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.RetornoItemML
{
    public class ItemMLRetornoIntegrar : ItemML
    {
        public ItemMLRetornoIntegrar()
        {
        }

        public ItemMLRetornoIntegrar( string idItemML, string titulo, string categoriaId, decimal preco, string moedaId,  string modoDeVenda, string condicao, string tipoDeListaId, DescricaoItemML descricao,
            string linkVideoId, List<TermosDeVendaML> termosDeVenda, List<ImagensRetornoIntegrar> imagens, List<AtributoItemML> atributos, List<VariacaoItemML> variacoes, string linkAnuncio, string statusProduto)
        {
            id = idItemML;
            title = titulo;
            category_id = categoriaId;
            price = preco;
            currency_id = moedaId;
            available_quantity = initial_quantity - sold_quantity;
            buying_mode = modoDeVenda;
            condition = condicao;
            listing_type_id = tipoDeListaId;
            description = descricao;
            video_id = linkVideoId;
            sale_terms = termosDeVenda;
            pictures = imagens;
            attributes = atributos;
            variations = variacoes;
            permalink = linkAnuncio;
            status = statusProduto;
         /*   initial_quantity = qtdInicial;
            sold_quantity = qtdVendida;*/
        }

        public string id { get; set; }
        public int initial_quantity { get; set; }
        public int sold_quantity { get; set; }
        public new List<ImagensRetornoIntegrar> pictures { get; set; }
        public string permalink { get; set; }
        public string status { get; set; }

    }
}
