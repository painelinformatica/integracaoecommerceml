﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.RetornoItemML
{
    public class AvisoRetorno
    {
        public AvisoRetorno()
        {
        }

        public AvisoRetorno(string departamento, string idCausa, string codigo, string mensagem, string[] referencias)
        {
            department = departamento;
            cause_id = idCausa;
            code = codigo;
            message = mensagem;
            references = referencias;
        }

        public string department { get; set; }
        public string cause_id { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string[] references { get; set; }
    }
}
