﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.RetornoItemML
{
    public class RetornaDescricaoItem
    {
        public RetornaDescricaoItem()
        {
        }

        public RetornaDescricaoItem(string descricao)
        {
            /*id = idDescricao;*/
            plain_text = descricao;
        }

        /*public string id { get; set; }*/
        public string  plain_text { get; set; }
    }
}
