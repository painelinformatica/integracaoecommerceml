﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IntegracaoPainel.ModelML.EditarPrecoQuantidade
{
    public class Preco
    {
        public Preco()
        {
        }

        public Preco(long idVariacao, decimal preco)
        {
            id = idVariacao;
            price = preco;
        }

        public long id { get; set; }
        public decimal price { get; set; }
    }
}
