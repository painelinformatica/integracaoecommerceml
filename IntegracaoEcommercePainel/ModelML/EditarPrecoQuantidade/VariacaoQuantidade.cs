﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.EditarPrecoQuantidade
{
    public class VariacaoQuantidade
    {
        public VariacaoQuantidade()
        {
        }

        public VariacaoQuantidade(List<Quantidade> quantidade)
        {
            variations = quantidade;
        }

        public List<Quantidade> variations { get; set; }
    }
}
