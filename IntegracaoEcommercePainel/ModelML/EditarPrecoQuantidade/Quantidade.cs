﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IntegracaoPainel.ModelML.EditarPrecoQuantidade
{
    public class Quantidade
    {
        public Quantidade()
        {
        }

        public Quantidade(long idVariacao, int quantidadeSGE)
        {
            id = idVariacao;
            available_quantity = quantidadeSGE;
        }

        public long id { get; set; }
        public int available_quantity { get; set; }
       
    }
}
