﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.ModelML.EditarPrecoQuantidade
{
    public class VariacaoPreco
    {
        public VariacaoPreco()
        {
        }

        public VariacaoPreco(List<Preco> preco)
        {
            variations = preco;
        }

        public List<Preco> variations { get; set; }
    }
}
