﻿using CefSharp;
using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoPainel;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.Credencial;
using IntegracaoPainel.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using IntegracaoEcommercePainel.Util;
using System.Windows.Media;
using IntegracaoEcommercePainel.FuncoesSGE;
using System.Windows.Input;
using IntegracaoPainel.FuncoesML;

namespace IntegracaoEcommercePainel
{
    public partial class MainWindow : Window
    {
        public string diretorioCredencialJson = @"C:\painelinformatica\ecommerce\credenciais";
        public string dadosAcessoML = "dadosAcessoML.json";
        public string conexaoBD = "conexaoBD.json";

        //private String Code;
        /*Configuracoes configuracoes;*/
        List<Loja> Lojas = new List<Loja>();

        ObservableCollection<string> bancos = new ObservableCollection<string>();
        ObservableCollection<string> LojasCombo = new ObservableCollection<string>();

        public MainWindow()
        {
            InitializeComponent();

            /*VerificaSGE.MonitoraEcommerceSGE();
            this.Close();*/

            /*var data = Environment.GetCommandLineArgs();
            if(data.Length == 2)
            {
                if (data[1].ToString() == "Service")
                {
                    VerificaSGE.MonitoraEcommerceSGE();
                    this.Close();
                }
            }*/

            
            AutenticarEcommerce.Visibility = Visibility.Collapsed;
            VerificaConexaoBancoDedados();
            comboBanco.ItemsSource = bancos;
            comboLoja.ItemsSource = LojasCombo;
            AutenticarEcommerce.IsEnabled = false;
            Cef.GetGlobalCookieManager().DeleteCookies();

        }

        public void ListaEcommerce()
        {
            comboBoxEcommerce.Items.Add("Mercado Livre");
            
        }

        private void MoverBox(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnFechar(object sender, RoutedEventArgs e)
        {
            this.Close();
            Console.WriteLine("click fechar");
        }

        private void ComboBancoChange(object sender, SelectionChangedEventArgs e)
        {
            string IpServidor = ipServidor.Text;
            string banco = comboBanco.SelectedItem.ToString();

            comboBoxEcommerce.Items.Clear();
            ConfiguraML.Visibility = Visibility.Collapsed;
            logoML.Visibility = Visibility.Collapsed;
            dataGridCredencialML.ItemsSource = null;
            acessarProdutos.IsEnabled = false;
            SalvarCfgBd.IsEnabled = false;
            SalvarCfgBd.Content = "Salvar";
            SalvarCfgBd.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
            SalvarCfgBd.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");

            btnConectaSGE.Content = "Conectar";
            usuarioSGE.IsEnabled = true;
            usuarioSGE.Clear();
            senhaSGE.IsEnabled = true;
            senhaSGE.Clear();
            AutenticarEcommerce.IsEnabled = false;
            StatusSGETexto.Content = "Não Autenticado";
            btnConectaSGE.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
            btnConectaSGE.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");

            ListaEcommerce();

            CarregarLoja(IpServidor, banco);
        }

        private void BtnCheckBanco(object sender, RoutedEventArgs e)
        {
            //load.Visibility = Visibility.Visible;
            //StatusTexto.Content = "Conectando . . .";

            string IpServidor = ipServidor.Text;
            CarregarBanco(IpServidor);
        }
        
        public static string SemFormatacao(string Codigo)
        {
            return Codigo.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
        }

        private void comboBoxEcommerceChange(object sender, SelectionChangedEventArgs e)
        {
            var selecionaEcommerce = comboBoxEcommerce.SelectedValue;
            ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());
            string sql = "Select Count(*) From Tbcredencialecommerce Where Ecommerce = '"+ selecionaEcommerce + "' AND Nomebanco = '"+ comboBanco.SelectedItem.ToString() + "'" ;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            string qtdRegistro = reader[0].ToString();
            reader.Close();

            if(qtdRegistro == "1")
            {
                List<CredencialML> resultado = Util.Util.CarregarDadosDataGrid(ipServidor.Text, comboBanco.SelectedItem.ToString());
                dataGridCredencialML.ItemsSource = resultado;
                if (resultado.FirstOrDefault().Status == "Válido")
                {
                    acessarProdutos.IsEnabled = true;
                }

                logoML.Margin = new Thickness(0, 20, 0, 0);
                logoML.Visibility = Visibility.Visible;
                SalvarCfgBd.Content = "Cfg Salva";
                SalvarCfgBd.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                SalvarCfgBd.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                SalvarCfgBd.IsEnabled = false;
            }
            else
            {
                string sqlDelete = "Delete From Tbcredencialecommerce Where Ecommerce = '"+ selecionaEcommerce + "' AND Nomebanco = '"+ comboBanco.SelectedItem.ToString() + "'";
                SqlCommand cmdDelete = new SqlCommand(sqlDelete, ConexaoBancoDados.Conexao);
                cmdDelete.ExecuteNonQuery();

                logoML.Margin = new Thickness(0, 0, 130, 0);
                logoML.Visibility = Visibility.Visible;
                ConfiguraML.Visibility = Visibility.Visible;
                SalvarCfgBd.IsEnabled = false;
            }

            /*if (selecionaEcommerce == "Mercado Livre")
            {
                ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());
                string sqlquery = "SELECT COUNT(*) FROM Tbdadosdeacessoml";
                SqlCommand cmd2 = new SqlCommand(sqlquery, ConexaoBancoDados.Conexao);
                cmd2.CommandType = CommandType.Text;
                SqlDataReader reader2;
                reader2 = cmd2.ExecuteReader();
                reader2.Read();
                string daml = reader2[0].ToString();
                reader2.Close();
                if (daml == "1")
                {
                    List<CredencialML> resultado = Util.Util.CarregarDadosDataGrid(ipServidor.Text, comboBanco.SelectedItem.ToString());
                    dataGridCredencialML.ItemsSource = resultado;
                    if (resultado.FirstOrDefault().Status == "Válido")
                    {
                        acessarProdutos.Visibility = Visibility.Visible;
                    }

                    logoML.Margin = new Thickness(0, 20, 0, 0);
                    logoML.Visibility = Visibility.Visible;
                    SalvarCfgBd.IsEnabled = true;
                }
                else
                {
                    string sql = "TRUNCATE TABLE Tbdadosdeacessoml";
                    SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                    cmd.ExecuteNonQuery();

                    logoML.Margin = new Thickness(0, 0, 130, 0);
                    logoML.Visibility = Visibility.Visible;
                    ConfiguraML.Visibility = Visibility.Visible;
                }
            }*/
        }

        private void ConfiguraML_Click(object sender, RoutedEventArgs e)
        {

            ConfigurarMercadoLivre configurarML = new ConfigurarMercadoLivre(ipServidor.Text, comboBanco.SelectedItem.ToString());
            var credenciasML = configurarML.ShowDialog();

            if (credenciasML.Item1 != null)
            {
                logoML.Margin = new Thickness(0, 20, 0, 0);
                logoML.Visibility = Visibility.Visible;
                ConfiguraML.Visibility = Visibility.Collapsed;

                DateTime dataCriacaoArquivo = DateTime.Now;
                DateTime dataValidadeToken = dataCriacaoArquivo.AddSeconds(credenciasML.Item8);

                ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());
                String query = "set dateformat dmy; INSERT INTO Tbdadosdeacessoml (Idaplicacaoml, Skaplicacao, Nicknameusuario, Emailusuario, Accesstokenml, Refreshtokenml, Dataautenticacao, Vlr_pegar_ecommerce, Idoper_pegar_ecommerce, Idpessoal_pegar_ecommerce) VALUES ('" + credenciasML.Item1 + "','" + credenciasML.Item2 + "' , '" + credenciasML.Item3 + "','" + credenciasML.Item4 + "', '" + credenciasML.Item5 + "','" + credenciasML.Item6 + "','" + dataValidadeToken + "','"+ credenciasML.Item9+"', '"+credenciasML.Item11+"', '"+credenciasML.Item10+"')";
                SqlCommand command = new SqlCommand(query, ConexaoBancoDados.Conexao);
                command.ExecuteNonQuery();
                List<CredencialML> resultado = Util.Util.CarregarDadosDataGrid(ipServidor.Text, comboBanco.SelectedItem.ToString());

                dataGridCredencialML.ItemsSource = resultado;
                SalvarCfgBd.IsEnabled = true;
            }
        }

        private void ValidarTokenAcesso(object sender, RoutedEventArgs e)
        {
            string sqlquery = "SELECT * FROM Tbdadosdeacessoml";
            SqlCommand cmd = new SqlCommand(sqlquery, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            string idApp = reader["Idaplicacaoml"].ToString();
            string skApp = reader["Skaplicacao"].ToString();
            string rtApp = reader["RefreshTokenml"].ToString();
            reader.Close();
            FuncoesRespostaML.refreshToken(idApp, skApp, rtApp, ipServidor.Text, comboBanco.SelectedItem.ToString());      
            List<CredencialML> resultado = Util.Util.CarregarDadosDataGrid(ipServidor.Text, comboBanco.SelectedItem.ToString());

            dataGridCredencialML.ItemsSource = resultado;
            if (resultado.FirstOrDefault().Status == "Válido")
            {
                acessarProdutos.IsEnabled = true;
            }
        }

        private void VerificaConexaoBancoDedados()
        {
            var verificaConexao = Util.Util.VerificarSeArquivoExiste(diretorioCredencialJson, conexaoBD);

            if(verificaConexao == "ArquivoExiste")
            {
                /*StatusCfg.Content = "Cfg Salva";*/

                string json = File.ReadAllText(Path.Combine(diretorioCredencialJson, conexaoBD));
                ConexaoBancodeDados conexao = JsonConvert.DeserializeObject<ConexaoBancodeDados>(json);

                AutenticarEcommerce.Visibility = Visibility.Visible;
                ipServidor.Text = conexao.IpServidor;
                CarregarBanco(conexao.IpServidor);
                comboBanco.SelectedItem = conexao.Banco;
                CarregarLoja(conexao.IpServidor, conexao.Banco);
            }
        }
        
        private void CarregarLoja(string ipServidor, string banco)
        {
            try
            {
                ConexaoBancoDados.GetConexao(ipServidor, banco);
                string sql = "SELECT IDLOJA, LOJA, CNPJ FROM TLOJA";
                SqlCommand cmd = new SqlCommand(sql,ConexaoBancoDados.Conexao);
                cmd.CommandType = CommandType.Text;
                SqlDataReader reader;
                    
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    LojasCombo.Clear();
                    Lojas.Clear();
                    int i = 0;
                    while (reader.Read())
                    {
                        LojasCombo.Add(reader[0].ToString() + " - " + reader[1].ToString() + " CNPJ: " + reader[2].ToString());
                        Lojas.Add(new Loja(i, ipServidor, banco, reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), SemFormatacao(reader[2].ToString())));
                        i++;
                    }
                }
                else
                {
                    MessageBox.Show("Nenhuma Loja encontrada!");
                    LojasCombo.Clear();
                }
                reader.Close();
                if (LojasCombo.Count > 0)
                {
                    comboLoja.ItemsSource = LojasCombo;
                    comboLoja.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                /*ConexaoBancoDados.GetConexao(ipServidor, banco).Close();*/
            }

        }

        private void CarregarBanco(string ipServidor)
        {
            string sql = "SELECT name FROM sys.databases";

            try
            { 
                ConexaoBancoDados.GetBanco(ipServidor);
                SqlCommand cmd = new SqlCommand(sql,ConexaoBancoDados.ConexaoBanco);
                cmd.CommandType = CommandType.Text;
                SqlDataReader reader;
               
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    bancos.Clear();
                    while (reader.Read())
                    {
                        if ((reader[0].ToString().StartsWith("DBSGE", StringComparison.InvariantCultureIgnoreCase)) && (reader[0].ToString() != "DBSGELOG"))
                        {
                            bancos.Add(reader[0].ToString());
                        }   
                    }
                }
                else
                {
                    StatusTexto.Content = "Banco Não Encontrado";
                }
                reader.Close();
                if (bancos.Count > 0)
                {
                    comboBanco.ItemsSource = bancos;
                    for(int i =0; i < comboBanco.Items.Count; i++)
                    {
                        if(comboBanco.Text == comboBanco.Items[i].ToString())
                        {
                            comboBanco.SelectedIndex = i;
                        }
                    }

                    ComboBancoChange(comboBanco, null);

                    btnConectaBanco.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675"); 
                    btnConectaBanco.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                    btnConectaBanco.Content = "Conectado";
                    StatusTexto.Content = "Banco Conectado";
                    AutenticarEcommerce.Visibility = Visibility.Visible;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro: Banco de Dados não Encontrado! ");
                StatusTexto.Content = "Banco Não Encontrado";
                LojasCombo.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
                StatusTexto.Content = "Banco Não Encontrado";
                LojasCombo.Clear();
            }
            finally
            {
                /*ConexaoBancoDados.GetBanco(ipServidor).Close();*/
            }
        }

        /*private void SalvarConfigBd(object sender, RoutedEventArgs e)
        {
            string IpServidor = ipServidor.Text;
            string banco = comboBanco.Text;
            string idLoja = Lojas[comboLoja.SelectedIndex].idloja;

            ConexaoBancodeDados conexao = new ConexaoBancodeDados(IpServidor, banco, idLoja);
            string json = JsonConvert.SerializeObject(conexao, Formatting.Indented);

            Util.Util.CriarArquivoCredencialJson(diretorioCredencialJson, conexaoBD, json);

            StatusCfg.Content = "Cfg Salva";
            MessageBox.Show("Configuração Salva!");
        }*/

        private void AcessarProdutos_Click(object sender, RoutedEventArgs e)
        {
            List<CredencialML> cred = (List<CredencialML>)dataGridCredencialML.ItemsSource;

            ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());

            String sql1 = "Select Idusuario from Tbdadosusuarioecommerceml Where Nickname = '"+cred[0].NickNameUsuario+"'";
            SqlCommand cmd = new SqlCommand(sql1, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            SessaoConexao.IdUsuario = reader[0].ToString();
            reader.Close();

            String sql2 = "Select Accesstokenml from Tbdadosdeacessoml Where Nicknameusuario = '" + cred[0].NickNameUsuario + "'";
            SqlCommand cmd1 = new SqlCommand(sql2, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader2;
            reader2 = cmd1.ExecuteReader();
            reader2.Read();
            SessaoConexao.AccessToken = reader2[0].ToString();
            reader2.Close();

            SessaoConexao.NickNameUsuario = cred[0].NickNameUsuario;
            SessaoConexao.ValorSGE = cred[0].ValorSGE;
            SessaoConexao.IpServidor = ipServidor.Text;
            SessaoConexao.BancoDeDados = comboBanco.SelectedItem.ToString();
            SessaoConexao.Ecommerce = comboBoxEcommerce.SelectedItem.ToString();
            String[] idLoja = comboLoja.SelectedItem.ToString().Split(" - ");
            SessaoConexao.IdLoja = Convert.ToInt32(idLoja[0]);
            SessaoConexao.UsuarioLogadoSGE = usuarioSGE.Text.ToString().ToUpper();

            /*Dashboard dashboard = new Dashboard();
            var dash = dashboard.ShowDialog();*/

            Produtos produtos = new Produtos();
            var prd = produtos.ShowDialog();
        }

        private void ExcluirIntegracao(object sender, RoutedEventArgs e)
        {
            string sql = "TRUNCATE TABLE Tbtoken; TRUNCATE TABLE Tbdadosdeacessoml; TRUNCATE TABLE Tbdadosusuarioecommerceml;";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();

            string sqlDelete = "Delete From Tbcredencialecommerce Where Ecommerce = '" + comboBoxEcommerce.SelectedItem.ToString() + "' AND Nomebanco = '" + comboBanco.SelectedItem.ToString() + "'";
            SqlCommand cmdDelete = new SqlCommand(sqlDelete, ConexaoBancoDados.Conexao);
            cmdDelete.ExecuteNonQuery();

            logoML.Margin = new Thickness(0, 20, 130, 0);
            logoML.Visibility = Visibility.Visible;
            ConfiguraML.Visibility = Visibility.Visible;
            dataGridCredencialML.ItemsSource = null;
            acessarProdutos.IsEnabled = false;
            SalvarCfgBd.IsEnabled = false;
            SalvarCfgBd.Content = "Salvar";
            SalvarCfgBd.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
            SalvarCfgBd.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
        }

        private void SalvarCfgBd_Click(object sender, RoutedEventArgs e)
        {
            if ((ipServidor.Text == "") || (ipServidor.Text == null))
            {
                MessageBox.Show("Digite o IP ou Nome do Servidor");
            }
            else if ((comboBanco.SelectedItem.ToString() == "") || (comboBanco.SelectedItem.ToString() == null))
            {
                MessageBox.Show("Escolha um Banco de Dados");
            }
            else if ((comboLoja.SelectedItem.ToString() == "") || (comboLoja.SelectedItem.ToString() == null))
            {
                MessageBox.Show("Escolha uma Loja");
            }
            else if (StatusTexto.Content != "Banco Conectado")
            {
                MessageBox.Show("Conecte no banco");
            }
            else
            {
                List<CredencialML> credencials = (List<CredencialML>)dataGridCredencialML.ItemsSource;
                if (MessageBox.Show( "Confirme os dados antes de Salvar: "+ "\n\n"+
                                "Servidor: " + ipServidor.Text + "\n" +
                                "Banco: " + comboBanco.SelectedItem.ToString() + "\n" +
                                "Loja: " + comboLoja.SelectedItem.ToString() + "\n" +
                                "Ecommerce Selecionado: " + comboBoxEcommerce.SelectedItem.ToString() + "\n" +
                                "Nome de Usuário: "+ credencials[0].NickNameUsuario + "\n" +
                                "Email: " + credencials[0].EmailUsuario + "\n" +
                                "Valor do SGE: "+ credencials[0].ValorSGE+"\n\n" +
                                "Deseja realmente salvar esses dados?", "Dados para salvar", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
                {
                }
                else
                {
                    ConexaoBancodeDados bd = new ConexaoBancodeDados();
                    bd.IpServidor = ipServidor.Text;
                    bd.Banco = comboBanco.SelectedItem.ToString();
                    bd.IdLoja = comboLoja.SelectedItem.ToString();
                    var json = JsonConvert.SerializeObject(bd);

                    Util.Util.CriarPastaArquivo(diretorioCredencialJson, conexaoBD, json);
                    /*File.WriteAllText(Path.Combine(diretorioCredencialJson,conexaoBD),json);*/

                    ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());
                    String[] idLoja = comboLoja.SelectedItem.ToString().Split(" - ");
                    String query = "INSERT INTO Tbcredencialecommerce(Ipservidor, Nomebanco, Idloja, Ecommerce, Iddadosdeacesso) " +
                                   "VALUES ('" + ipServidor.Text + "','" + comboBanco.SelectedItem.ToString() + "' , '" + Convert.ToInt32(idLoja[0]) + "','" + comboBoxEcommerce.SelectedItem.ToString() + "', '" + credencials[0].IdDadosDeAcesso + "')";
                    SqlCommand command = new SqlCommand(query, ConexaoBancoDados.Conexao);
                    command.ExecuteNonQuery();

                    SalvarCfgBd.Content = "Cfg Salva";
                    SalvarCfgBd.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                    SalvarCfgBd.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                    SalvarCfgBd.IsEnabled = false;
                    acessarProdutos.IsEnabled = true;

                    MessageBox.Show("Configuração Salva!","Salvar Configuração");
                }
            }
        }

        private void btnConectaSGE_Click(object sender, RoutedEventArgs e)
        {
            if (btnConectaSGE.Content == "Deslogar")
            {
                btnConectaSGE.Content = "Conectar";
                usuarioSGE.IsEnabled = true;
                usuarioSGE.Clear();
                senhaSGE.IsEnabled = true;
                senhaSGE.Clear();
                AutenticarEcommerce.IsEnabled = false;
                StatusSGETexto.Content = "Não Autenticado";
                btnConectaSGE.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
                btnConectaSGE.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#58A5F0");
            }
            else
            {
                ConectaSGE();
            }
        }

        private void senhaSGE_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ConectaSGE();
            }
        }

        private void usuarioSGE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                senhaSGE.Focus();
            }
        }

        private void ConectaSGE()
        {
            ConexaoBancoDados.GetConexao(ipServidor.Text, comboBanco.SelectedItem.ToString());
            string sql = "SELECT Usuario, Senha from Tl_usu where Usuario = '" + usuarioSGE.Text.ToString().ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            if (reader.Read() == true)
            {
                if (reader[1].ToString() == senhaSGE.Password.ToString().ToUpper())
                {
                    btnConectaSGE.Content = "Deslogar";
                    btnConectaSGE.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#FF4040");
                    btnConectaSGE.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#FF4040");
                    StatusSGETexto.Content = "Login Efetuado com Sucesso";
                    usuarioSGE.IsEnabled = false;
                    senhaSGE.IsEnabled = false;
                    AutenticarEcommerce.IsEnabled = true;
                }
                else
                {
                    StatusSGETexto.Content = "Senha Incorreta";
                    senhaSGE.Clear();
                }
            }
            else
            {
                StatusSGETexto.Content = "Usuário Incorreto";
                usuarioSGE.Clear();
                senhaSGE.Clear();
            }
            reader.Close();
        }

        private void EditarIntegracao(object sender, RoutedEventArgs e)
        {
            List<CredencialML> credencial = (List<CredencialML>)dataGridCredencialML.ItemsSource;
            EditarConfiguracao editar = new EditarConfiguracao(ipServidor.Text, comboBanco.SelectedItem.ToString(), credencial[0].NickNameUsuario, credencial[0].IdOperacao, credencial[0].IdPessoal, credencial[0].ValorSGE);
            var ed = editar.ShowDialog();

            List<CredencialML> resultado = Util.Util.CarregarDadosDataGrid(ipServidor.Text, comboBanco.SelectedItem.ToString());

            dataGridCredencialML.ItemsSource = resultado;
        }
    }
}
