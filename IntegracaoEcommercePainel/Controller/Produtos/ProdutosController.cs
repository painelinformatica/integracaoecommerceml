﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.BuscarListaItemUsuario;
using IntegracaoPainel.ModelML.Produtos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Linq;

namespace IntegracaoEcommercePainel.Controller.Produtos
{
    public class ProdutosController
    {
        public static List<String> RetornaProdutosEcommerceNaoVinculadosSGE(BuscarItens itens)
        {
            List<String> produtoEcommerce = new List<String>();
            List<String> listaIdProdutoEcSGE = new List<String>();

            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);

            string sql = "SELECT Idproduto_ec FROM TPRODUTOEC";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listaIdProdutoEcSGE.Add(new String(reader["Idproduto_ec"].ToString()));
                }
                reader.Close();
            }

            for (int i = 0; i < itens.results.Length; i++)
            {
                var resultado = listaIdProdutoEcSGE.Where(x => x == itens.results[i]).Count();
                if(resultado == 0)
                {
                    produtoEcommerce.Add(new string(itens.results[i]));
                }
            }

            return produtoEcommerce;
        }
    }
}
