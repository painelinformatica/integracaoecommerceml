﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using IntegracaoEcommercePainel.Model.BuscarItemSGE;
using System.Windows;

namespace IntegracaoEcommercePainel.Controller.VincularItemEcommerce
{
    public class VincularItemEcommerceController
    {
        public static List<ProdutoVincularEcommerce> CarregarItemSGE(string ipServidor, string banco, string idProduto)
        {
            List<ProdutoVincularEcommerce> prdVincular = new List<ProdutoVincularEcommerce>();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select tpv.Idproduto, tpv.Idprodutov, tpi.Descricao, tp."+SessaoConexao.ValorSGE+" as ValorVendaProduto, tpv.Sku, tpv.Variacao, tpv.Idcor, tc.Cor, tpv.Idtamanho, tt.Tamanho, tpv.Idvoltagem, tv.Voltagem, tpv.Idsabor, ts.Sabor, tpv.Qde - isnull(vpe.QDE,0) - tpv.Qde_pend_ec as QtdDisponivelEC, " +
                         "tpi.Idgrupo, tg.Grupo, tg.Cont_variacao, tg.Cont_var_cor, tg.Cont_var_tamanho, tg.Cont_var_voltagem, tg.Cont_var_sabor " +
                         "from Tprodutov as tpv " +
                         "left outer join Tcor as tc on tc.Idcor = tpv.Idcor " +
                         "left outer join Ttamanho as tt on tt.Idtamanho = tpv.Idtamanho " +
                         "left outer join Tsabor as ts on ts.Idsabor = tpv.Idsabor " +
                         "left outer join Tvoltagem as tv on tv.Idvoltagem = tpv.Idvoltagem " +
                         "inner join Tprodutoi as tpi on tpi.Idproduto = tpv.Idproduto " +
                         "inner join Tgrupo as tg on tg.Idgrupo = tpi.Idgrupo " +
                         "inner join Tproduto as tp on tp.Idproduto = tpv.Idproduto "+
                         "left outer join VP_PEND_ESTOQUE as vpe on vpe.IDPRODUTO = tpv.Idproduto and vpe.IDLOJA = tpv.Idloja and vpe.IDPRODUTOV = tpv.Idprodutov " +
                         "where tpv.Idproduto = '" + idProduto + "' and Tpv.Idloja = " + SessaoConexao.IdLoja;

            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if(reader["Cont_variacao"].ToString() == "0")
                {
                    prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                 reader["Descricao"].ToString(),
                                                                 reader["Sku"].ToString(),
                                                                 Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                 Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                 "",
                                                                 Visibility.Collapsed,
                                                                 "",
                                                                 Visibility.Collapsed,
                                                                 "",
                                                                 Visibility.Collapsed,
                                                                 "",
                                                                 Visibility.Collapsed,
                                                                 Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                 ""
                                                                 ));
                }
                else if (reader["Cont_variacao"].ToString() == "3")
                {
                    string cor = "";
                    string tamanho = "";
                    string sabor = "";
                    string voltagem = "";
                    int variacaoInconsistente = 0;
                    if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == true) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        if (reader["Cor"].ToString() == "")
                        {
                            cor = "Variação Inconsistente";
                            variacaoInconsistente++;
                        } 
                        else if (reader["Tamanho"].ToString() == "")
                        {
                            tamanho = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            cor = reader["Cor"].ToString();
                            tamanho = reader["Tamanho"].ToString();
                        }

                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                     reader["Descricao"].ToString(),
                                                                     reader["Sku"].ToString(),
                                                                     Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                     Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                     cor,
                                                                     Visibility.Visible,
                                                                     tamanho,
                                                                     Visibility.Visible,
                                                                     "",
                                                                     Visibility.Collapsed,
                                                                     "",
                                                                     Visibility.Collapsed,
                                                                     Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                     ""
                                                                     ));
                    }
                    else if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == true))
                    {
                        if (reader["Cor"].ToString() == "")
                        {
                            cor = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else if (reader["Voltagem"].ToString() == "")
                        {
                            voltagem = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            cor = reader["Cor"].ToString();
                            voltagem = reader["Voltagem"].ToString();
                        }
                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                     reader["Descricao"].ToString(),
                                                                     reader["Sku"].ToString(),
                                                                     Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                     Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                     cor,
                                                                     Visibility.Visible,
                                                                     "",
                                                                     Visibility.Collapsed,
                                                                     voltagem,
                                                                     Visibility.Visible,
                                                                     "",
                                                                     Visibility.Collapsed,
                                                                     Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                     ""
                                                                     ));
                    }
                    else if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        if (reader["Cor"].ToString() == "")
                        {
                            cor = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            cor = reader["Cor"].ToString();
                        }
                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                    reader["Descricao"].ToString(),
                                                                    reader["Sku"].ToString(),
                                                                    Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                    Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                    cor,
                                                                    Visibility.Visible,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    Convert.ToInt32(reader["Idprodutov"].ToString()), 
                                                                    ""
                                                                    ));
                    }
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == true) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        if (reader["Tamanho"].ToString() == "")
                        {
                            tamanho = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            tamanho = reader["Tamanho"].ToString();
                        }
                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                    reader["Descricao"].ToString(),
                                                                    reader["Sku"].ToString(),
                                                                    Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                    Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    tamanho,
                                                                    Visibility.Visible,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                    ""
                                                                    ));
                    }
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == true))
                    {
                        if (reader["Voltagem"].ToString() == "")
                        {
                            voltagem = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            voltagem = reader["Voltagem"].ToString();
                        }

                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                    reader["Descricao"].ToString(),
                                                                    reader["Sku"].ToString(),
                                                                    Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                    Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    voltagem,
                                                                    Visibility.Visible,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                    ""
                                                                    ));
                    } 
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == true) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        if (reader["Sabor"].ToString() == "")
                        {
                            sabor = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            sabor = reader["Sabor"].ToString();
                        }

                        prdVincular.Add(new ProdutoVincularEcommerce(idProduto,
                                                                    reader["Descricao"].ToString(),
                                                                    reader["Sku"].ToString(),
                                                                    Convert.ToDecimal(reader["QtdDisponivelEC"].ToString()),
                                                                    Convert.ToDecimal(reader["ValorVendaProduto"].ToString()),
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    "",
                                                                    Visibility.Collapsed,
                                                                    sabor,
                                                                    Visibility.Visible,
                                                                    Convert.ToInt32(reader["Idprodutov"].ToString()),
                                                                    ""
                                                                    ));
                    }
                }
            }
            reader.Close();

            return prdVincular;
        }
        
        public static void InserirDadosEC(string ipServidor, string banco, string idProduto, int idLoja, string ecommerce, string tituloEc, string descricaoEc, string idProdutoEc, string jsonEnvio)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tprodutoec (Idproduto, Idloja, Ecommerce, Titulo_ec, Descricao_ec, Idproduto_ec, Json_ec_envio) " +
                         "VALUES ('"+idProduto+"', "+idLoja+", '"+ecommerce+"', '"+tituloEc+"', '"+descricaoEc+"', '"+idProdutoEc+"', '"+jsonEnvio+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void InserirDadosMonitoraEC(string ipServidor, string banco, List<ProdutoListaSGEEcommerce> produtoVinculaECSGE, int idLoja, string ecommerce, string idProdutoEc)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            for (int i = 0; i < produtoVinculaECSGE.Count; i++)
            {
                // fazer insert tproduto monitara ec

                string sql = "INSERT INTO Tproduto_monitora_ec (Idproduto, Idloja, Idprodutov, Sku, Ecommerce, Idproduto_ec, Idprodutov_ec, Vlr_venda_atual_ec, Qde_atual_ec, Status_produto_ec) " +
                             "VALUES ('"+produtoVinculaECSGE[i].IdProduto+"', " +
                                      ""+idLoja+", " +
                                      "'"+produtoVinculaECSGE[i].IdProdutoV+"'," +
                                      "'"+produtoVinculaECSGE[i].ProdutoSKU+"', " +
                                      "'"+ecommerce+"', " +
                                      "'"+idProdutoEc+"', " +
                                      ""+produtoVinculaECSGE[i].IdVariacaoEC+", " +
                                      ""+Util.Util.ConverteValorSQL(produtoVinculaECSGE[i].PrecoProdutoSGE.ToString())+", " +
                                      ""+Util.Util.ConverteValorSQL(produtoVinculaECSGE[i].QuantidadeProdutoSGE.ToString())+", " +
                                      "0)";
                SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                cmd.ExecuteNonQuery();
            }
        }

        public static void HabilitarProdutoEcommerce(string ipServidor, string banco, string idProduto)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "UPDATE Tprodutoi SET E_commerce = 1 WHERE Idproduto = '" + idProduto + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

    }
}
