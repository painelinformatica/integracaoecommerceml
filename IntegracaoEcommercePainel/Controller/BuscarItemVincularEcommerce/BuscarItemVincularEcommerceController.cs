﻿using IntegracaoEcommercePainel.Model.BuscarItemSGE;
using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Windows;

namespace IntegracaoEcommercePainel.Controller.BuscarItemVincularEcommerce
{
    public class BuscarItemVincularEcommerceController
    {
        public static List<ProdutoVincularEcommerce> BuscarProdutosProdutosSGE(string ipServidor, string banco, string itemBuscar, int offset)
        {
            List<ProdutoVincularEcommerce> produtos = new List<ProdutoVincularEcommerce>();

            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT TP.Idproduto, TPI.Descricao FROM TPRODUTO AS TP INNER JOIN TPRODUTOI AS TPI ON TPI.IDPRODUTO = TP.IDPRODUTO " +
                         "WHERE TPI.Descricao Like '"+itemBuscar+"%' "+
                         "ORDER BY TPI.Descricao " +
                         "OFFSET "+offset+" ROWS " +
                         "FETCH NEXT 50 ROWS ONLY";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    produtos.Add(new ProdutoVincularEcommerce(reader["Idproduto"].ToString(), reader["Descricao"].ToString(), "", 0, 0, "", Visibility.Collapsed, "", Visibility.Collapsed, "", Visibility.Collapsed, "", Visibility.Collapsed, 0, ""));
                }
                
            }
            reader.Close();
            return produtos;
        }

        public static List<ProdutoVincularEcommerce> BuscarTodosProdutosSGE(string ipServidor, string banco, int offset)
        {
            List<ProdutoVincularEcommerce> produtos = new List<ProdutoVincularEcommerce>();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT TP.Idproduto, TPI.Descricao FROM TPRODUTO AS TP INNER JOIN TPRODUTOI AS TPI ON TPI.IDPRODUTO = TP.IDPRODUTO " +
                         "ORDER BY TPI.Descricao " +
                         "OFFSET " + offset + " ROWS " +
                         "FETCH NEXT 50 ROWS ONLY";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    produtos.Add(new ProdutoVincularEcommerce(reader["Idproduto"].ToString(), reader["Descricao"].ToString(), "", 0, 0,"",Visibility.Collapsed,"",Visibility.Collapsed,"",Visibility.Collapsed,"",Visibility.Collapsed,0, ""));
                }
                reader.Close();
            }

            return produtos;
        }

        public static int RetornaQuantidadeProdutosSGE(string ipServidor, string banco, string itemBuscar)
        {
            int quantidadeItens = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(TP.Idproduto) as QuantidadeProdutos FROM TPRODUTO AS TP INNER JOIN TPRODUTOI AS TPI ON TPI.IDPRODUTO = TP.IDPRODUTO " +
                         "WHERE TPI.Descricao Like '" + itemBuscar + "%' ";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            quantidadeItens = Convert.ToInt32(reader["QuantidadeProdutos"].ToString());
            reader.Close();

            return quantidadeItens;
        }
    }
}
