﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.FuncoesSGE.FuncoesSGEBD;
using IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD;
using IntegracaoPainel.ModelML.Callback.OrderV2;
using IntegracaoPainel.ModelML.Callback.Payments;
using IntegracaoPainel.ModelML.Callback.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class AnalisaRetornoCallbackOSPML
    {
        // comparar lista de ordem com lista de shipment com a de pagamento

        private static string caminhoPasta = @"C:\painelinformatica\ecommerce\logs";
       
        public static void AnalisarRetornoCallback(List<OrdersV2> orderv2, List<Shipments> shipment, List<Payments> payment, string ipServidor, string banco, int idOperacao, int idPessoal, int idLoja, int valorVenda, string origem)
        {
            for (int i = 0; i < orderv2.Count; i++)
            {
                List<Shipments> searchShip = shipment.Where(x => x.order_id == orderv2[i].id).ToList();
                List<Payments> searchPay = payment.Where(x => x.order_id == orderv2[i].id.ToString()).ToList();

                if (SelectSGE.VerificaPedidoDeVenda(ipServidor, banco, orderv2[i].id) > 0)
                {
                    if (SelectSGE.RetornaPedidoVenda(ipServidor, banco, orderv2[i].id).Status != orderv2[i].status)
                    {
                        // atualizar o status da ordem no banco
                        int idStatus = 0;
                        StatusPedidoVenda status = new StatusPedidoVenda();

                        if (SelectSGE.VerificaStatusPedidoVendaEc(ipServidor, banco, origem, orderv2[i].status) != 0)
                        {
                            status = SelectSGE.RetornaStatusPedidoVendaEc(ipServidor, banco, origem, orderv2[i].status);
                            idStatus = status.IdStatusEc;
                        }
                        else
                        {
                            idStatus = 999;
                            string logStatusDesconhecido = "Status Desconhecido às " + DateTime.Now + "\n\n" + "Dados status desconhecido: " + "\n\n" + orderv2[i].status + "\n\n" + "----------------------------\n" + "Erro aconteceu às " + DateTime.Now;
                            Util.CriarPastaArquivo(caminhoPasta, "StatusDesconhecido-" + Util.NormalizaHora(DateTime.Now) + ".txt", logStatusDesconhecido);
                        }
                        
                        UpdateSGE.AtualizarStatusPedidoDeVenda(ipServidor, banco,idStatus, orderv2[i].id.ToString());
                    }
                }
                else
                {
                    DadosCliente cliente = new DadosCliente();
                    TipoPessoa tpPessoa = new TipoPessoa();
                    int atividade = 0;
                    string ultimoIDCadastrado = "";
                    FichaExtraCliente fichaExtra = new FichaExtraCliente();
                    Endereco endereco = new Endereco();

                    if(orderv2[i].buyer.billing_info.doc_type == "CPF")
                    {
                        if(SelectSGE.VerificaCadastroClientePF(ipServidor, banco, orderv2[i].buyer.billing_info.doc_number) > 0)
                        {
                            cliente = SelectSGE.RetornaClienteCadastrado(ipServidor, banco, "F", 0, orderv2[i].buyer.billing_info.doc_number);
                            tpPessoa.TpPessoa = "F";
                            tpPessoa.Cpf = orderv2[i].buyer.billing_info.doc_number;
                            tpPessoa.Cnpj = "";
                        }
                    } 
                    else if (orderv2[i].buyer.billing_info.doc_type == "CNPJ")
                    {
                        if (SelectSGE.VerificaCadastroClientePJ(ipServidor, banco, orderv2[i].buyer.billing_info.doc_number) > 0)
                        {
                            cliente = SelectSGE.RetornaClienteCadastrado(ipServidor, banco, "J", SelectSGE.VerificaCadastroClientePJ(ipServidor, banco, orderv2[i].buyer.billing_info.doc_number), orderv2[i].buyer.billing_info.doc_number);
                            tpPessoa.TpPessoa = "J";
                            tpPessoa.Cpf = "";
                            tpPessoa.Cnpj = orderv2[i].buyer.billing_info.doc_number; ;
                        }
                    }

                    if (cliente.IdCadastro == 0)
                    {
                        //Cadastro Cliente
                        string nomeCliente = orderv2[i].buyer.first_name + " " + orderv2[i].buyer.last_name;

                        if (orderv2[i].buyer.billing_info.doc_type == "CPF")
                        {
                            tpPessoa.TpPessoa = "F";
                            tpPessoa.Cpf = orderv2[i].buyer.billing_info.doc_number;
                            tpPessoa.Cnpj = "";
                        }
                        else if (orderv2[i].buyer.billing_info.doc_type == "CNPJ")
                        {
                            tpPessoa.TpPessoa = "J";
                            tpPessoa.Cpf = "";
                            tpPessoa.Cnpj = orderv2[i].buyer.billing_info.doc_number; ;
                        }

                        if (SelectSGE.VerificarAtividadesCadastrada(ipServidor, banco, "Ecommerce") == 0)
                        {
                            InsertSGE.CadastrarAtividade(ipServidor, banco, "Ecommerce", 999);
                        }
                        atividade = SelectSGE.RetornaIdAtividadeCadastrada(ipServidor, banco, "Ecommerce");

                        InsertSGE.CadastrarPessoa(ipServidor, banco, nomeCliente, tpPessoa.TpPessoa, "C", "O", DateTime.Now, "", Util.FormatarCPF(tpPessoa.Cpf), "100000.00", "N", "S", atividade);
                        ultimoIDCadastrado = SelectSGE.RetornaUltimoIdCadastrado(ipServidor, banco);
                        cliente = SelectSGE.RetornaClienteCadastrado(ipServidor, banco, tpPessoa.TpPessoa, Convert.ToInt32(ultimoIDCadastrado), orderv2[i].buyer.billing_info.doc_number);
                        UpdateSGE.AdicionarCadastroPrincipalPessoa(ipServidor, banco, cliente.IdCadastro);
                    }
                 
                    fichaExtra = SelectSGE.RetornaFichaExtraCliente(ipServidor, banco, Convert.ToInt32(searchShip[0].receiver_address.zip_code), cliente.IdCadastro);

                    ///// verificar ficha extra ou cadastrar uma novaa
                    if(fichaExtra.IdCadastroe == 0)
                    {
                        String[] uf = searchShip[0].receiver_address.state.id.Split("-");
                        endereco = VerificaCadastro.VerificaCadastraEndereco(ipServidor, banco, uf[1], searchShip[0].receiver_address.state.name, searchShip[0].receiver_address.city.name,
                                                                    searchShip[0].receiver_address.neighborhood.name, searchShip[0].receiver_address.zip_code, searchShip[0].receiver_address.address_line);


                        // buscar quantas fichas o cliente tem e somar mais uma
                        InsertSGE.CadastrarFichaExtra(ipServidor, banco, cliente.IdCadastro, 1, searchShip[0].receiver_address.receiver_phone, Util.FormatarCNPJ(tpPessoa.Cnpj), "S", DateTime.Now, "", 9, idPessoal, idLoja,
                                                        endereco.Cep.NumeroCep, endereco.Cep.NumeroCep, endereco.Cep.NumeroCep, endereco.Cep.Endereco, endereco.Cep.Endereco, endereco.Cep.Endereco,
                                                        searchShip[0].receiver_address.street_number, searchShip[0].receiver_address.street_number, searchShip[0].receiver_address.street_number,
                                                        endereco.Bairro.IdBairro, endereco.Bairro.IdBairro, endereco.Bairro.IdBairro, endereco.Cidade.IdCidade, endereco.Cidade.IdCidade, endereco.Cidade.IdCidade);

                        fichaExtra = SelectSGE.RetornaFichaExtraCliente(ipServidor, banco, Convert.ToInt32(searchShip[0].receiver_address.zip_code), cliente.IdCadastro);
                    }
                    fichaExtra.Endereco = searchShip[0].receiver_address.address_line;
             

                    //Buscar status

                    int idStatus = 0;
                    StatusPedidoVenda status = new StatusPedidoVenda();

                    if (SelectSGE.VerificaStatusPedidoVendaEc(ipServidor, banco, origem, orderv2[i].status) != 0)
                    {
                        status = SelectSGE.RetornaStatusPedidoVendaEc(ipServidor, banco, origem, orderv2[i].status);
                        idStatus = status.IdStatusEc;
                    }
                    else
                    {
                        idStatus = 999;
                        string logStatusDesconhecido = "Status Desconhecido às " + DateTime.Now + "\n\n" + "Dados status desconhecido: " + "\n\n" + orderv2[i].status + "\n\n" + "----------------------------\n" + "Erro aconteceu às " + DateTime.Now;
                        Util.CriarPastaArquivo(caminhoPasta, "StatusDesconhecido-" + Util.NormalizaHora(DateTime.Now) + ".txt", logStatusDesconhecido);
                    }

                    InsertSGE.CadastrarPedidoDeVenda(ipServidor, banco, idLoja, DateTime.Now, cliente.IdCadastro, fichaExtra.IdCadastroe, idPessoal, idOperacao, 10, "Ecommerce", orderv2[i].total_amount,
                                                         orderv2[i].total_amount, orderv2[i].order_items.Count, valorVenda-1, orderv2[i].id, origem, searchShip[0].id, idStatus);
                    ultimoIDCadastrado = SelectSGE.RetornaUltimoIdCadastrado(ipServidor, banco);

                    for (int a = 0; a < orderv2[i].order_items.Count; a++)
                    {
                        ItemPedidoDeVenda item = new ItemPedidoDeVenda();
                        item = SelectSGE.RetornaProduto(ipServidor, banco, orderv2[i].order_items[a].item.seller_sku);

                        InsertSGE.CadastrarItemPedidoDeVenda(ipServidor, banco, Convert.ToInt32(ultimoIDCadastrado), item.IdProduto, item.IdProdutoV, item.Descricao, item.IdUnidade, orderv2[i].order_items[a].quantity,
                                                             orderv2[i].order_items[a].unit_price, 0, orderv2[i].order_items[a].quantity * orderv2[i].order_items[a].unit_price,
                                                             orderv2[i].order_items[a].quantity * orderv2[i].order_items[a].unit_price, orderv2[i].order_items[a].unit_price, orderv2[i].id);

                        /*if ((orderv2[i].status != "cancelled") || (orderv2[i].status != "invalid "))
                        {
                            UpdateSGE.AtualizarQuantidadeItemProdutoV(ipServidor, banco, orderv2[i].order_items[a].item.seller_sku, orderv2[i].order_items[a].quantity);
                            UpdateSGE.AtualizarQuantidadeItemProdutoMonitora(ipServidor, banco, orderv2[i].order_items[a].item.seller_sku, orderv2[i].order_items[a].quantity);
                        }*/
                    }

                    UpdateSGE.AtualizaPedidoDeVenda(ipServidor, banco, Convert.ToInt32(ultimoIDCadastrado));
                }
            }
        }
    }
}
