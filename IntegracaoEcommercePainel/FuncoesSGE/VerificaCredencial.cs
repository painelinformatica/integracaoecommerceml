﻿using IntegracaoEcommercePainel.Model.VerificaSGE;
using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class VerificaCredencial
    {
        private static List<CredencialEcommerce> ecommerce = new List<CredencialEcommerce>();

        public static List<CredencialEcommerce> VerificaCredencialEcommerce(string ipServidor, string banco)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Idcredencialecommerce, Ipservidor, Nomebanco, Idloja, Ecommerce, Iddadosdeacesso FROM Tbcredencialecommerce";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ecommerce.Add(new CredencialEcommerce(Convert.ToInt32(reader["Idcredencialecommerce"]), reader["Ipservidor"].ToString(),
                                                          reader["Nomebanco"].ToString(), Convert.ToInt32(reader["Idloja"]), reader["Ecommerce"].ToString(),
                                                          Convert.ToInt32(reader["Iddadosdeacesso"])));
                }
                reader.Close();
            }

            List<CredencialEcommerce> retornoCredencial = ecommerce;

            return retornoCredencial;
        }
    }
}
