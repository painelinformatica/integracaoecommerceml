﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model.VerificaSGE;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;

namespace IntegracaoPainel.FuncoesSGE
{
    public class BuscarCallback
    {
        public static List<BuscarDataHoraCallbackSGE> BuscarCallbackSGE(string ipServidor, string banco)
        {
            List<BuscarDataHoraCallbackSGE> callbackSGE = new List<BuscarDataHoraCallbackSGE>();

            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(Idecommerce_callback) as nCallRegistrados FROM Tecommerce_callback";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            int numeroDeCallbacks = Convert.ToInt32(reader["nCallRegistrados"].ToString());
            reader.Close();

            if (numeroDeCallbacks == 0)
            {
                callbackSGE = null;
            }
            else
            {
                string sql2 = "SELECT TOP 1 * FROM Tecommerce_callback ORDER BY Data_hora_consulta ASC";
                SqlCommand cmd2 = new SqlCommand(sql2, ConexaoBancoDados.Conexao);
                cmd2.CommandType = CommandType.Text;
                SqlDataReader reader2;
                reader2 = cmd2.ExecuteReader();
                while (reader2.Read())
                {
                    callbackSGE.Add(new BuscarDataHoraCallbackSGE(reader2["Idecommerce_callback"].ToString(), reader2["Tipo_callback"].ToString(), DateTime.Parse(reader2["Data_hora_consulta"].ToString()), reader2["Status_callback"].ToString()));
                }
                reader2.Close(); 

            }
            return callbackSGE;
        }

        public static void RegistrarCallback(string ipServidor, string banco, string idOrder, DateTime horaConsulta, string ecommerce, string status)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tecommerce_callback (Idecommerce_callback, Tipo_callback, Data_hora_consulta, Status_callback) " +
                         "VALUES('"+idOrder+"', '"+ecommerce+"', '"+horaConsulta+"', '"+status+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }
    }
}
