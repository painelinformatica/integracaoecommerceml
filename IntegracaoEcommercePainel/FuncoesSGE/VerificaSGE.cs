﻿using IntegracaoEcommercePainel.Util;
using IntegracaoEcommercePainel.Model.VerificaSGE;
using IntegracaoEcommercePainel.FuncoesML;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System;
using System.Windows;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.EditarPrecoQuantidade;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.RetornoItemML;
using System.Globalization;
using IntegracaoPainel.ModelML.Variacao;
using System.Linq;
using IntegracaoPainel.FuncoesSGE;
using IntegracaoPainel.Model.VerificaSGE;
using IntegracaoPainel.ModelML.Credencial;
using IntegracaoPainel.ModelML.Callback;
using IntegracaoPainel.ModelML.Callback.OrderV2;

namespace IntegracaoEcommercePainel.FuncoesSGE
{
    public static class VerificaSGE
    {
        private static List<CredencialEcommerce> ecommerce = new List<CredencialEcommerce>();
        private static List<BuscarDataHoraCallbackSGE> callbackSGE = new List<BuscarDataHoraCallbackSGE>();
        //private static List<ConexaoBancodeDados> lojas = new List<ConexaoBancodeDados>();

        private static string ipServidor;
        
        public static void MonitoraEcommerceSGE()
        {
            VerificaConexaoBanco();
            var bancos = VerificaBD.VerificaBanco(ipServidor);

            for(int a=0; a < bancos.Count; a++)
            {
                ecommerce = VerificaCredencial.VerificaCredencialEcommerce(ipServidor,bancos[a]);
            }

            for(int a=0; a < ecommerce.Count; a++)
            {
                switch (ecommerce[a].NomeEcommerce)
                {
                    case "Mercado Livre":
                        VerificaToken token = ValidaToken.VerificaTokenML(ecommerce[a].IpServidor, ecommerce[a].NomeBanco,ecommerce[a].IdDadosDeAcesso);
                        AtualizaPrecoQuantidade.AlterarPrecoEC(ecommerce[a].IpServidor, ecommerce[a].NomeBanco, token.AccessToken, ecommerce[a].NomeEcommerce, token.ValorSGE);
                        AtualizaPrecoQuantidade.AlteraQuantidadeEC(ecommerce[a].IpServidor, ecommerce[a].NomeBanco, token.AccessToken, ecommerce[a].NomeEcommerce);
                        AtualizaTabelas.AtualizaPrecoQuantidade(ecommerce[a].IpServidor, ecommerce[a].NomeBanco, token.ValorSGE);
                        callbackSGE = BuscarCallback.BuscarCallbackSGE(ecommerce[a].IpServidor, ecommerce[a].NomeBanco);
                        List<CallbackBDPainel> listaConsultaCallback = ConsultarCallbackEcommerce.ConsultaCallbackML(callbackSGE, token.IdAplicacaoEC);

                        //datetimenow para salvar na tabela  de callback sge

                        DateTime horaConsulta = DateTime.Now;
                        String[] valorPegar = token.ValorVendaEcommerce.Split("a");
                        var listaOrder = listaConsultaCallback.Where(x => x.topic.Contains("orders_v2")).ToList();
                        var listaShipement = listaConsultaCallback.Where(x => x.topic.Contains("shipments")).ToList();
                        var listaPayment = listaConsultaCallback.Where(x => x.topic.Contains("payments")).ToList();
                        //RetornaCallbackOSPML.VerificaCallbackOrder(listaOrder, token.AccessToken);
                        AnalisaRetornoCallbackOSPML.AnalisarRetornoCallback(RetornaCallbackOSPML.VerificaCallbackOrder(listaOrder, token.AccessToken),
                                                                            RetornaCallbackOSPML.VerificaCallbackShipment(listaShipement, token.AccessToken),
                                                                            RetornaCallbackOSPML.VerificaCallbackPayment(listaPayment, token.AccessToken),
                                                                            ecommerce[a].IpServidor, ecommerce[a].NomeBanco, token.IdOperacaoEcommerce, token.IdPessoalEcommerce, ecommerce[a].IdLoja, Convert.ToInt32(valorPegar[1]), "Mercado Livre");
                        BuscarCallback.RegistrarCallback(ecommerce[a].IpServidor, ecommerce[a].NomeBanco, "Teste", horaConsulta, "Mercado Livre", "OK");

                        break;
                }
            }
        }

        private static void VerificaConexaoBanco()
        {
            string diretorioCredencialJson = @"C:\painelinformatica\ecommerce\credenciais";
            string conexaoBD = "conexaoBD.json";
            var verificaConexao = Util.Util.VerificarSeArquivoExiste(diretorioCredencialJson, conexaoBD);

            if (verificaConexao == "ArquivoExiste")
            {
                string json = File.ReadAllText(Path.Combine(diretorioCredencialJson, conexaoBD));
                ConexaoBancodeDados conexao = JsonConvert.DeserializeObject<ConexaoBancodeDados>(json);
                ipServidor = conexao.IpServidor;
            }
            else
            {
                MessageBox.Show("Sem Conexão com o Servidor");
            }
        }
    }
}
