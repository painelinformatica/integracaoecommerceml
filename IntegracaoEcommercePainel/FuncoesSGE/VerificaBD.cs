﻿using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class VerificaBD
    {
        private static List<string> bancos = new List<string>();

        public static List<string> VerificaBanco(string ipServidor)
        {
            string sql = "SELECT name FROM sys.databases";
            ConexaoBancoDados.GetBanco(ipServidor);
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.ConexaoBanco);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                bancos.Clear();
                while (reader.Read())
                {
                    /*if ((reader[0].ToString().StartsWith("DBSGE", StringComparison.InvariantCultureIgnoreCase)) && (reader[0].ToString() != "DBSGELOG"))
                    {
                        bancos.Add(new string(reader[0].ToString()));
                    }*/

                    if (reader[0].ToString() == "DBSGE")
                    {
                        bancos.Add(new string(reader[0].ToString()));
                    }
                }
            }
            reader.Close();

            List<string> retornoBancos = bancos;

            return retornoBancos;
        }
    }
}
