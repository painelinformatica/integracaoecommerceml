﻿using IntegracaoPainel.FuncoesML;
using IntegracaoPainel.ModelML.Callback;
using IntegracaoPainel.ModelML.Callback.OrderV2;
using IntegracaoPainel.ModelML.Callback.Payments;
using IntegracaoPainel.ModelML.Callback.Shipment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class RetornaCallbackOSPML
    {

        public static List<OrdersV2> VerificaCallbackOrder(List<CallbackBDPainel> listaOrder, string accessToken)
        {
            List<OrdersV2> ordersV2 = new List<OrdersV2>();
            for (int i = 0; i < listaOrder.Count; i++)
            {
                var retornoConsultaOrdem = FuncoesCallbackML.ConsultarCallbackEcommerce(listaOrder[i].resource, accessToken);
                var jsonOrder = JsonConvert.DeserializeObject<OrdersV2>(retornoConsultaOrdem);
                ordersV2.Add(jsonOrder);
            }

            return ordersV2;
        }

        public static List<Shipments> VerificaCallbackShipment(List<CallbackBDPainel> listaShipment, string accessToken)
        {
            List<Shipments> shipments = new List<Shipments>();
            for (int i = 0; i < listaShipment.Count; i++)
            {
                var retornoConsultaShip = FuncoesCallbackML.ConsultarCallbackEcommerce(listaShipment[i].resource, accessToken);
                var jsonShip = JsonConvert.DeserializeObject<Shipments>(retornoConsultaShip);
                shipments.Add(jsonShip);
            }

            return shipments;
        }

        public static List<Payments> VerificaCallbackPayment(List<CallbackBDPainel> listaPayment, string accessToken)
        {
            List<Payments> payments = new List<Payments>();
            for (int i = 0; i < listaPayment.Count; i++)
            {
                var retornoConsultaShip = FuncoesCallbackML.ConsultarCallbackEcommerce(listaPayment[i].resource, accessToken);
                var jsonPay = JsonConvert.DeserializeObject<Payments>(retornoConsultaShip);
                payments.Add(jsonPay);
            }

            return payments;
        }
    }
}
