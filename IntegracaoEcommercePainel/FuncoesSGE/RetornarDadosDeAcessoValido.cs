﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.ModelML.Credencial;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace IntegracaoPainel.FuncoesSGE
{
    public class RetornarDadosDeAcessoValido
    {
        public static DadosDeAcessoML DadosAcessoUsuarioML(string ipServidor, string banco,int dadosAcesso)
        {
            DadosDeAcessoML usuario = new DadosDeAcessoML();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select * from Tbdadosdeacessoml where Iddadosdeacessoml = " + dadosAcesso;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            usuario.IdAplicacao = reader["Idaplicacaoml"].ToString();
            usuario.SkAplicacao = reader["Skaplicacao"].ToString();
            usuario.NicknameUsuario = reader["Nicknameusuario"].ToString();
            usuario.EmailUsuario = reader["Emailusuario"].ToString();
            usuario.AccessTokenML = reader["Accesstokenml"].ToString();
            usuario.RefreshTokenML = reader["Refreshtokenml"].ToString();
            usuario.DataAutenticacao = Convert.ToDateTime(reader["Dataautenticacao"].ToString());
            reader.Close();

            return usuario;
        }
    }
}
