﻿using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE.FuncoesSGEBD
{
    public class UpdateSGE
    {
        public static void AdicionarCadastroPrincipalPessoa(string ipServidor, string banco, int idCadastro)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "UPDATE Tcadastro SET Idcadastrop = "+idCadastro+" WHERE Idcadastro = "+idCadastro+"";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void AtualizaPedidoDeVenda(string ipServidor, string banco, int numeroPedidoVenda)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "exec pp_tpedvda " + numeroPedidoVenda;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void AtualizarQuantidadeItemProdutoV(string ipServidor, string banco, string sku, int quantidade)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "UPDATE tpv set tpv.Qde= tpv.Qde - "+quantidade+"  from  Tprodutov as tpv WHERE Sku = '"+sku+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void AtualizarQuantidadeItemProdutoMonitora(string ipServidor, string banco, string sku, int quantidade)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "UPDATE tpec set tpec.Qde_atual_ec = tpec.Qde_atual_ec - " + quantidade + "  from  Tproduto_monitora_ec as tpec WHERE Sku = '" + sku + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void AtualizarStatusPedidoDeVenda(string ipServidor, string banco, int idStatus, string idOrdem)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Update Tpedvda SET Idstatus_ec = "+idStatus+" Where Num_ordem_ec = '"+idOrdem+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }
    }
}
