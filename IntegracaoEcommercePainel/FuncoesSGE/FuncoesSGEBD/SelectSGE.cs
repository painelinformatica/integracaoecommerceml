﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE.FuncoesSGEBD
{
    public class SelectSGE
    {
        public static int VerificaCadastroClientePF(string ipServidor, string banco, string cpf)
        {
            int numeroDeClientes = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(Cpf) As QtdClienteEncontrado FROM Tcadastro WHERE Cpf = '" + Util.FormatarCPF(cpf) + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            numeroDeClientes = Convert.ToInt32(reader["QtdClienteEncontrado"].ToString());
            reader.Close();
            return numeroDeClientes;
        }

        public static int VerificaCadastroClientePJ(string ipServidor, string banco, string cnpj)
        {
            int idCadastro = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT * FROM Tcadastroe Where Cnpj = '" + Util.FormatarCNPJ(cnpj) + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    idCadastro = Convert.ToInt32(reader["Idcadastro"].ToString());
                }
                reader.Close();
            }
            return idCadastro;
        }

        public static DadosCliente RetornaClienteCadastrado(string ipServidor, string banco, string tipoPessoa, int idCadastro, string cpf)
        {
            DadosCliente cliente = new DadosCliente();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "";

            if (tipoPessoa == "F")
            {
                sql = "SELECT * FROM Tcadastro WHERE Cpf = '" + Util.FormatarCPF(cpf) + "'";
            } 
            else if( tipoPessoa == "J")
            {
                sql = "SELECT* FROM Tcadastro WHERE Idcadastro = " + idCadastro ;
            }

            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            cliente.IdCadastro = Convert.ToInt32(reader["Idcadastro"].ToString());
            cliente.Nome = reader["Nome"].ToString();
            cliente.IdCadastroP = Convert.ToInt32(reader["Idcadastrop"].ToString());
            cliente.TipoPessoa = reader["Pessoa"].ToString();
            cliente.TipoCadastro = reader["Tipo_cadastro"].ToString();
            cliente.Sexo = reader["Sexo"].ToString();
            cliente.DataCadastro = Convert.ToDateTime(reader["Dt_cadtro"].ToString());
            cliente.Cpf = reader["Cpf"].ToString();
            cliente.Rg = reader["Rg"].ToString();
            cliente.IdAtividade = Convert.ToInt32(reader["Idativ"].ToString());
            cliente.Ativo = reader["Ativo"].ToString();
            cliente.Bloqueado = reader["Bloqueado"].ToString();
            reader.Close();
            return cliente;
        }

        public static FichaExtraCliente RetornaFichaExtraCliente(string ipServidor, string banco, int cep, int idCadastro)
        {
            FichaExtraCliente fichaExtra = new FichaExtraCliente();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Tce.Nr_sequencia, Tce.Idcadastro,Tce.Idcadastroe, Tce.Fone, Tce.Cnpj, Tce.Ativo, Tce.Insc_estadual, Tce.Tipo_insc_estadual, " +
                         "Tce.Idpessoal, Tce.Idloja, Tce.Cep, Tce.Endereco, Tce.Numero, Tce.Idbairro, Tce.Idcidade, Tep.Uf, Tsi.Nome " +
                         "FROM Tcadastroe as Tce " +
                         "INNER JOIN Tcep as Tep ON Tep.Cep = Tce.Cep " +
                         "INNER JOIN Tpaisi as Tsi ON Tsi.Uf = Tep.Uf " +
                         "WHERE Tce.Cep = " + cep + " AND Tce.Idcadastro = '"+idCadastro+"'" ;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                fichaExtra.NrSequencia = Convert.ToInt32(reader["Nr_sequencia"].ToString());
                fichaExtra.IdCadastro = Convert.ToInt32(reader["Idcadastro"].ToString());
                fichaExtra.IdCadastroe = Convert.ToInt32(reader["Idcadastroe"].ToString());
                fichaExtra.Telefone = reader["Fone"].ToString();
                fichaExtra.Cnpj = reader["Cnpj"].ToString();
                fichaExtra.CadastroAtivo = reader["Ativo"].ToString();
                fichaExtra.InscricaoEstadual = reader["Insc_estadual"].ToString();
                fichaExtra.TipoInscricaoEstadual = Convert.ToInt32(reader["Tipo_insc_estadual"].ToString());
                fichaExtra.IdPessoal = Convert.ToInt32(reader["Idpessoal"].ToString());
                fichaExtra.IdLoja = Convert.ToInt32(reader["Idloja"].ToString());
                fichaExtra.Cep = Convert.ToInt32(reader["Cep"].ToString());
                fichaExtra.Endereco = reader["Endereco"].ToString();
                fichaExtra.Numero = reader["Numero"].ToString();
                fichaExtra.IdBairro = Convert.ToInt32(reader["Idbairro"].ToString());
                fichaExtra.IdCidade = Convert.ToInt32(reader["Idcidade"].ToString());
                fichaExtra.Uf = reader["Uf"].ToString();
                fichaExtra.NomeEstado = reader["Nome"].ToString();
            }
            
            reader.Close();
            return fichaExtra;
        }

        public static int VerificarAtividadesCadastrada(string ipServidor, string banco, string nomeAtividade)
        {
            int numeroAtividades = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(Atividade) As QtdAtividadeEncontrada FROM Tatividade WHERE Atividade = '"+nomeAtividade.ToUpper()+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            numeroAtividades = Convert.ToInt32(reader["QtdAtividadeEncontrada"].ToString());
            reader.Close();
            return numeroAtividades;
        }

        public static int RetornaIdAtividadeCadastrada(string ipServidor , string banco, string nomeAtividade)
        {
            int idAtividade = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT * FROM Tatividade WHERE Atividade = '" + nomeAtividade.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            idAtividade = Convert.ToInt32(reader["Idativ"].ToString());
            reader.Close();
            return idAtividade;
        }

        public static int VerificaCEPCadastrado(string ipServidor, string banco, int cep)
        {
            int cepCadastrado = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(Cep) As QtdCepEncontrados FROM Tcep WHERE Cep = " + cep;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            cepCadastrado = Convert.ToInt32(reader["QtdCepEncontrados"].ToString());
            reader.Close();
            return cepCadastrado;
        }

        public static Cep RetornaCepCadastrado(string ipServidor, string banco, int cep)
        {
            Cep retornaCep = new Cep();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT * FROM Tcep WHERE Cep = " + cep;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            retornaCep.NumeroCep = Convert.ToInt32(reader["Cep"].ToString());
            retornaCep.IdCidade = Convert.ToInt32(reader["Idcidade"].ToString());
            retornaCep.Logradouro = reader["Tipo_cep"].ToString();
            retornaCep.Endereco = reader["Endereco"].ToString();
            retornaCep.IdBairro = Convert.ToInt32(reader["Idbairro"].ToString());
            retornaCep.Bairro = reader["Bairro"].ToString();
            retornaCep.Uf = reader["Uf"].ToString();
            reader.Close();
            return retornaCep;
        }

        public static int VerificaEstadoCadastrado(string ipServidor, string banco, string nomeEstado)
        {
            int numeroEstados = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select COUNT(Nome) As QtdUfEncontrado FROM Tpaisi WHERE Nome = '" + nomeEstado.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            numeroEstados = Convert.ToInt32(reader["QtdUfEncontrado"].ToString());
            reader.Close();
            return numeroEstados;
        }

        public static Estado RetornaEstadoCadastrado(string ipServidor, string banco, string nomeEstado)
        {
            Estado estado = new Estado();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select * FROM Tpaisi WHERE Nome = '" + nomeEstado.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            estado.IdPais = Convert.ToInt32(reader["Idpais"].ToString());
            estado.Uf = reader["Uf"].ToString();
            estado.NomeEstado = reader["Nome"].ToString();
            reader.Close();
            return estado;
        }

        public static int VerificaCidadeCadastrada(string ipServidor, string banco, string nomeCidade)
        {
            int numeroCidades = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select COUNT(Cidade) As QtdCidadeEncontrada FROM Tcidade WHERE Cidade = '"+nomeCidade.ToUpper()+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            numeroCidades = Convert.ToInt32(reader["QtdCidadeEncontrada"].ToString());
            reader.Close();
            return numeroCidades;
        }

        public static Cidade RetornaCidadeCadastrada(string ipServidor, string banco, string nomeCidade)
        {
            Cidade cidade = new Cidade();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select * FROM Tcidade WHERE Cidade = '" + nomeCidade.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            cidade.IdCidade = Convert.ToInt32(reader["Idcidade"].ToString());
            cidade.NomeCidade = reader["Cidade"].ToString();
            cidade.Uf = reader["Uf"].ToString();
            cidade.IdIBGE = Convert.ToInt32(reader["Idibge"].ToString());
            reader.Close();
            return cidade;
        }

        public static int RetornaIdIBGE(string ipServidor, string banco, string nomeCidade, string uf)
        {
            int idIBGE = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT * FROM Tibge WHERE Cidade = '"+nomeCidade+"' AND Uf = '"+uf+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            idIBGE = Convert.ToInt32(reader["Idibge"].ToString());
            reader.Close();
            return idIBGE;
        }

        public static int VerificaBairroCadastrada(string ipServidor, string banco, string nomeBairro)
        {
            int numeroBairro = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select COUNT(Bairro) As QtdBairroEncontrada FROM Tbairro WHERE Bairro = '" + nomeBairro.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            numeroBairro = Convert.ToInt32(reader["QtdBairroEncontrada"].ToString());
            reader.Close();
            return numeroBairro;
        }

        public static Bairro RetornaBairroCadastrado(string ipServidor, string banco, string nomeBairro)
        {
            Bairro bairro = new Bairro();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select * FROM Tbairro WHERE Bairro = '" + nomeBairro.ToUpper() + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            bairro.IdBairro = Convert.ToInt32(reader["Idbairro"].ToString());
            bairro.NomeBairro = reader["Bairro"].ToString();
            reader.Close();
            return bairro;
        }

        public static int VerificaPedidoDeVenda(string ipServidor, string banco, long idOrder)
        {
            int qtdPedidoVenda = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "Select COUNT(Num_ordem_ec) As QtdPedidoVendaEncontrado FROM Tpedvda WHERE Num_ordem_ec = '" + idOrder + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            qtdPedidoVenda = Convert.ToInt32(reader["QtdPedidoVendaEncontrado"].ToString());
            reader.Close();
            return qtdPedidoVenda;
        }

        public static PedidoDeVenda RetornaPedidoVenda(string ipServidor, string banco, long idOrder)
        {
            PedidoDeVenda pdVenda = new PedidoDeVenda();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "select Tva.Idloja, Tva.Dt_ped, Tva.Idcadastro, Tva.Idcadastroe, Tva.Vlr_total, Tva.Vlr_prod, Tva.Num_prod, Tse.Status_ec, Tva.Num_ordem_ec, Tva.Estacao_orig, Tva.Num_envio_ec from Tpedvda as Tva " +
                         "inner join Tstatus_ec as Tse ON Tse.Idstatus_ec = Tva.Idstatus_ec Where tva.Num_ordem_ec = '"+idOrder+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            pdVenda.IdLoja = Convert.ToInt32(reader["Idloja"].ToString());
            pdVenda.IdCadastro = Convert.ToInt32(reader["Idcadastro"].ToString());
            pdVenda.IdCadastroE = Convert.ToInt32(reader["Idcadastroe"].ToString());
            pdVenda.DataPedido = Convert.ToDateTime(reader["Dt_ped"].ToString());
            pdVenda.Origem = reader["Estacao_orig"].ToString();
            pdVenda.IdOrder = reader["Num_ordem_ec"].ToString();
            pdVenda.Status = reader["Status_ec"].ToString();
            pdVenda.ValorTotal = Convert.ToDecimal(reader["Vlr_total"].ToString());
            pdVenda.ValorProduto = Convert.ToDecimal(reader["Vlr_prod"].ToString());
            pdVenda.IdEnvios = reader["Num_envio_ec"].ToString();
            reader.Close();
            return pdVenda;
        }

        public static string RetornaUltimoIdCadastrado(string ipServidor, string banco)
        {
            string ultimoId = "";
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT SCOPE_IDENTITY()";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            ultimoId = reader[0].ToString();
            reader.Close();

            return ultimoId;
        }

        public static ItemPedidoDeVenda RetornaProduto(string ipServidor, string banco, string sku)
        {
            ItemPedidoDeVenda item = new ItemPedidoDeVenda();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "select tpv.Idproduto, tpv.Idprodutov, tpi.Descricao , tpi.Idunidade from Tprodutov as tpv " +
                         "inner join Tprodutoi as tpi on tpi.Idproduto = tpv.Idproduto " +
                         "where tpv.Sku = '"+sku+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            item.IdProduto = reader["Idproduto"].ToString();
            item.IdProdutoV = Convert.ToInt32(reader["Idprodutov"].ToString());
            item.Descricao = reader["Descricao"].ToString();
            item.IdUnidade = reader["Idunidade"].ToString();
            reader.Close();
            return item;
        }

        public static int VerificaStatusPedidoVendaEc(string ipServidor, string banco, string ecommerce, string statusEc)
        {
            int verificaStatus = 0;
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT COUNT(Status_ec) as QtdStatusEncontrados FROM Tstatus_ec WHERE Ecommerce = '"+ecommerce+"' AND Status_ec = '"+statusEc+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            verificaStatus = Convert.ToInt32(reader["QtdStatusEncontrados"].ToString());
            reader.Close();

            return verificaStatus;
        }

        public static StatusPedidoVenda RetornaStatusPedidoVendaEc(string ipServidor, string banco, string ecommerce, string statusEc)
        {
            StatusPedidoVenda status = new StatusPedidoVenda();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT * FROM Tstatus_ec WHERE Ecommerce = '"+ecommerce+"' AND Status_ec = '"+statusEc+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            status.IdStatusEc = Convert.ToInt32(reader["Idstatus_ec"].ToString());
            status.Ecommerce = reader["Ecommerce"].ToString();
            status.StatusEc = reader["Status_ec"].ToString();
            status.StatusSge = reader["Status_sge"].ToString();
            reader.Close();
            return status;
        }

    }
}
