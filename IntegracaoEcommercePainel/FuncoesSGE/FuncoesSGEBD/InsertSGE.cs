﻿using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE.FuncoesSGEBD
{
    public class InsertSGE
    {
        public static void CadastrarPessoa(string ipServidor, string banco, string nome, string tipoPessoa, string tipoCadastro, string sexo, DateTime dataCadastro, 
                                           string rg, string cpf, string limiteCredito, string usuarioBloqueado, string cadastroAtivo, int idAtividade)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tcadastro (Nome, Pessoa, Tipo_cadastro, Sexo, Dt_cadtro, Rg, Cpf, Lim_cred, Bloqueado, Ativo, Idativ) " +
                                      "Values('"+nome+"','"+tipoPessoa+"','"+tipoCadastro+"','"+sexo+"', '"+dataCadastro+"','"+rg+"', '"+Util.FormatarCPF(cpf)+"', " +
                                      "'"+ limiteCredito + "', '"+usuarioBloqueado+"', '"+cadastroAtivo+"', "+idAtividade+")";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarFichaExtra(string ipServidor, string banco, int idCadastro, int idCadastroe, string telefone, string cnpj, string cadastroAtivo, DateTime dataCadastro, string inscricaoEstadual, 
                                               int tipoInscricaoEstadual, int idPessoal, int idLoja, int cep, int cepC, int cepE, string endereco, string enderecoC, string enderecoE, string numero, string numeroC, string numeroE,
                                               int idBairro, int idBairroC, int idBairroE, int idCidade, int idCidadeC, int idCidadeE)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tcadastroe (Idcadastro, Idcadastroe, Fone, Cnpj, Insc_estadual, Tipo_insc_estadual, Dt_cadtro, Idloja, Idpessoal, Ativo, Cep, Endereco, Numero, Idbairro, Idcidade, " +
                                                 "Cepc, Enderecoc, Numeroc, Idbairroc, Idcidadec, Cepe, Enderecoe, Numeroe, Idbairroe, Idcidadee) " +
                                                 "Values ("+idCadastro+", "+idCadastroe+", '"+telefone+"', '"+cnpj+"', '"+inscricaoEstadual+"', "+tipoInscricaoEstadual+", '"+dataCadastro+"', "+idLoja+", "+idPessoal+", " +
                                                 "'"+cadastroAtivo+"', "+cep+", '"+endereco+"', '"+numero+"', "+idBairro+", "+idCidade+ ","+ cepC+", '"+enderecoC+"', '"+numeroC+"', "+idBairroC+", "+idCidadeC+"," +
                                                 ""+cepE+", '"+enderecoE+"', '"+numeroE+"', "+idBairroE+", "+idCidadeE+")";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarAtividade(string ipServidor, string banco, string nomeAtividade, int codigoAtividade)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tatividade (Atividade, Codativ) Values ('"+nomeAtividade.ToUpper()+"',"+codigoAtividade+")";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarPessoal(string ipServidor, string banco, string nomePessoal, string funcao)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tpessoal (Nome, Funcao) Values ('"+nomePessoal+"', '"+funcao+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarEstado(string ipServidor, string banco, int idPais, string uf, string nomeEstado)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tpaisi (Idpais, Uf, Nome) Values ("+idPais+", '"+uf+"', '"+nomeEstado+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarCidade(string ipServidor, string banco, string nomeCidade, string uf, int idIbge)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tcidade (Cidade, Uf, Idibge) Values ('" + nomeCidade + "','" + uf + "'," + idIbge + ")";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarBairro(string ipServidor, string banco, string nomeBairro)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tbairro (Bairro) Values ('"+nomeBairro+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarCEP(string ipServidor, string banco, int cep, int idCidade, string logradouro, string endereco, int idBairro, string uf)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tcep (Cep, Idcidade, Tipo_cep, Endereco, Idbairro, Uf) Values ("+cep+", "+idCidade+",'"+logradouro+"', '"+endereco+"', "+idBairro+", '"+uf+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarPedidoDeVenda(string ipServidor, string banco, int idLoja, DateTime dataPedido, int idCadastro, int idCadastroe, int idPessoal, int idOperacao, int idTransportadora,
                                                  string observacao, decimal valorTotal, decimal valorProduto, int quantidadeItens, int valorVendaEcommerce, long idOrder, string origem, long idShipment, int idStatus)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tpedvda (Idloja, Dt_ped, Idcadastro, Idcadastroe, Idpessoal, Idoper, Idtransp, Observacao, Vlr_total, Vlr_prod, Num_prod, Vlr_pegar2, Estacao_orig, Num_ordem_ec, Num_envio_ec, Idstatus_ec) " +
                                              "Values ("+idLoja+", '"+dataPedido+"', "+idCadastro+", "+idCadastroe+", "+idPessoal+", "+idOperacao+", "+idTransportadora+", '"+observacao+"', "+valorTotal+", " +
                                              ""+valorProduto+", "+quantidadeItens+", "+valorVendaEcommerce+", '"+origem+"', '"+idOrder+"', '"+idShipment+"', '"+idStatus+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

        public static void CadastrarItemPedidoDeVenda(string ipServidor, string banco, int numeroPedidoVenda, string idProduto, int idProdutov, string descricao, string idUnidade, int quantidade, decimal valorUnitario, 
                                                      decimal valorDesconto, decimal valorSubtotal, decimal valorLiquido, decimal valorUnitariot, long idOrder)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "INSERT INTO Tpedvdai (Num_ped, Idproduto, Idprodutov, Descricao, Idunidade, Qde, Vlr_unit, Vlr_descto, Vlr_subtotal, Vlr_liquido, Vlr_unitt, Estacao_orig) " +
                                               "Values ("+numeroPedidoVenda+", '"+idProduto+"', "+idProdutov+", '"+descricao+"', '"+idUnidade+"', "+quantidade+", "+valorUnitario+", "+valorDesconto+", " +
                                               ""+valorSubtotal+", "+valorLiquido+", "+valorUnitariot+", '"+idOrder+"')";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }

    }
}
