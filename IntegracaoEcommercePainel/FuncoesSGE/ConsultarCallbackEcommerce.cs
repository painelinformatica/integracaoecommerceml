﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model.VerificaSGE;
using IntegracaoPainel.ModelML.Callback;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class ConsultarCallbackEcommerce
    {
        private static List<CallbackBDPainel> callbackAPI = new List<CallbackBDPainel>();

        public static List<CallbackBDPainel> ConsultaCallbackML(List<BuscarDataHoraCallbackSGE> callbackSGE, string idAplicacaoML)
        {
            if(callbackSGE == null)
            {
                var json = FuncoesML.FuncoesCallbackML.BuscasCallbackRecebidosBdPainel(idAplicacaoML, "2021-03-30 15:53");
                List<CallbackBDPainel> callback = JsonConvert.DeserializeObject<List<CallbackBDPainel>>(json);
                callbackAPI = callback;
            }
            else
            {
                for (int i = 0; i < callbackSGE.Count; i++)
                {
                    var json = FuncoesML.FuncoesCallbackML.BuscasCallbackRecebidosBdPainel(idAplicacaoML, callbackSGE[i].DataHoraConsulta.ToString("yyyy'-'MM'-'dd' 'HH':'mm"));
                    List<CallbackBDPainel> callback = JsonConvert.DeserializeObject<List<CallbackBDPainel>>(json);
                    callbackAPI = callback;
                }
            }

            return callbackAPI;
        }
    }
}
