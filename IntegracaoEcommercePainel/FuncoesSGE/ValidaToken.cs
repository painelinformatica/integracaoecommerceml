﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model.VerificaSGE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class ValidaToken
    {
        public static VerificaToken VerificaTokenML(string ipServidor, string banco, int idAcesso)
        {
            VerificaToken credencial = new VerificaToken();
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "select * from Tbdadosdeacessoml where Iddadosdeacessoml = " + idAcesso;
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            credencial.ValorSGE = reader["Vlr_pegar_ecommerce"].ToString();
            credencial.AccessToken = reader["Accesstokenml"].ToString();
            credencial.IdAplicacaoEC = reader["Idaplicacaoml"].ToString();
            credencial.ValorVendaEcommerce = reader["Vlr_pegar_ecommerce"].ToString();
            credencial.IdOperacaoEcommerce = Convert.ToInt32(reader["Idoper_pegar_ecommerce"].ToString());
            credencial.IdPessoalEcommerce = Convert.ToInt32(reader["Idpessoal_pegar_ecommerce"].ToString());
            string idApp = reader["Idaplicacaoml"].ToString();
            string skAPP = reader["Skaplicacao"].ToString();
            string rfToken = reader["RefreshTokenml"].ToString();
            var dataAut = Convert.ToDateTime(reader["Dataautenticacao"]);
            reader.Close();
            TimeSpan ts = dataAut - DateTime.Now;
            int diferenca = ts.Hours;

            if ((diferenca >= 6) || (diferenca <= 0))
            {
                FuncoesRespostaML.refreshToken(idApp, skAPP, rfToken, ipServidor, banco);
                String sql2 = "Select Accesstokenml from Tbdadosdeacessoml";
                SqlCommand cmd1 = new SqlCommand(sql2, ConexaoBancoDados.Conexao);
                cmd.CommandType = CommandType.Text;
                SqlDataReader reader2;
                reader2 = cmd1.ExecuteReader();
                reader2.Read();
                credencial.AccessToken = reader2[0].ToString();
                reader2.Close();
            }

            return credencial;
        }
    }
}
