﻿using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class AtualizaTabelas
    {
        public static void AtualizaPrecoQuantidade(string ipServidor, string banco, string valorSGE)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "update tpm set tpm.Vlr_venda_atual_ec = tp." + valorSGE + ", tpm.Qde_atual_ec = (tpv.Qde - isnull(vpe.QDE,0)) " +
                         "from Tproduto_monitora_ec as tpm " +
                         "inner join Tprodutov as tpv on tpm.Sku = tpv.Sku " +
                         "inner join Tproduto as tp on tpm.Idproduto = tp.Idproduto " +
                         "left outer join VP_PEND_ESTOQUE as vpe on vpe.IDPRODUTO = tpv.Idproduto and vpe.IDLOJA = tpv.Idloja and vpe.IDPRODUTOV = tpv.Idprodutov " +
                         "where (tpm.Vlr_venda_atual_ec != tp." + valorSGE + ") or (tpm.Qde_atual_ec != (tpv.Qde - isnull(vpe.QDE,0)))";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();
        }
    }
}
