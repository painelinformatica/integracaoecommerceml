﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.ModelML.EditarPrecoQuantidade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class AtualizaPrecoQuantidade
    {
        private static List<Preco> preco = new List<Preco>();
        private static List<Quantidade> quantidade = new List<Quantidade>();

        public static void AlteraQuantidadeEC(string ipServidor, string banco, string accessToken, string ecommerce)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "select tpm.Qde_atual_ec, (tpv.Qde - isnull(vpe.QDE,0)) as QdeEc, * " +
                         "from Tproduto_monitora_ec as tpm " +
                         "inner join Tprodutov as tpv on tpm.sku = tpv.sku " +
                         "inner join Tproduto as tp on tpm.Idproduto = tp.Idproduto " +
                         "left outer join VP_PEND_ESTOQUE as vpe on vpe.IDPRODUTO = tpv.Idproduto and vpe.IDLOJA = tpv.Idloja and vpe.IDPRODUTOV = tpv.Idprodutov " +
                         "where (tpm.Qde_atual_ec != tpv.Qde - isnull(vpe.QDE,0)) " +
                         "and Status_produto_ec = 0 "+
                         "and Ecommerce = '" + ecommerce + "' " +
                         "order by Idproduto_ec";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            string idProdutoECcontrole = "";

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader["Idprodutov_ec"].ToString() == "0")
                    {
                        Quantidade quantidade = new Quantidade();
                        quantidade.id = 0;
                        quantidade.available_quantity = Convert.ToInt32(Convert.ToDecimal(reader["QdeEc"].ToString()));
                        var json = JsonConvert.SerializeObject(quantidade, new JsonSerializerSettings()
                        {
                            ContractResolver = new IgnorePropertiesResolver(new[] { "id" })
                        });
                        FuncoesEditarML.EditarPrecoQuantidadeItem(reader["Idproduto_ec"].ToString(), accessToken, json);
                    }
                    else
                    {
                        if ((idProdutoECcontrole != reader["Idproduto_ec"].ToString()) && (idProdutoECcontrole != ""))
                        {
                            VariacaoQuantidade listaQuantidade = new VariacaoQuantidade();
                            listaQuantidade.variations = quantidade;
                            var json = JsonConvert.SerializeObject(listaQuantidade);

                            FuncoesEditarML.EditarPrecoQuantidadeItem(idProdutoECcontrole, accessToken, json);

                            quantidade.Clear();
                        }

                        idProdutoECcontrole = reader["Idproduto_ec"].ToString();
                        quantidade.Add(new Quantidade(long.Parse(reader["Idprodutov_ec"].ToString()), Convert.ToInt32(Convert.ToDecimal(reader["QdeEc"].ToString()))));
                    }
                }
                if (quantidade.Count > 0)
                {
                    VariacaoQuantidade listaQuantidade = new VariacaoQuantidade();
                    listaQuantidade.variations = quantidade;
                    var json = JsonConvert.SerializeObject(listaQuantidade);

                    FuncoesEditarML.EditarPrecoQuantidadeItem(idProdutoECcontrole, accessToken, json);

                    quantidade.Clear();
                }
            }
            reader.Close();
        }

        public static void AlterarPrecoEC(string ipServidor, string banco, string accessToken, string ecommerce, string valorSGE)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "select tpm.Vlr_venda_atual_ec, tp." + valorSGE + " as valorSGE, * " +
                         "from Tproduto_monitora_ec as tpm " +
                         "inner join Tprodutov as tpv on tpm.sku = tpv.sku " +
                         "inner join Tproduto as tp on tpm.Idproduto = tp.Idproduto " +
                         "where (tpm.Vlr_venda_atual_ec != tp." + valorSGE + ")  " +
                         "and Status_produto_ec = 0 " +
                         "and Ecommerce = '" + ecommerce + "' " +
                         "order by Idproduto_ec";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            string idProdutoECcontrole = "";

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader["Idprodutov_ec"].ToString() == "0")
                    {
                        Preco preco = new Preco();
                        preco.id = 0;
                        preco.price = Convert.ToDecimal(reader["valorSGE"].ToString());
                        var json = JsonConvert.SerializeObject(quantidade, new JsonSerializerSettings()
                        {
                            ContractResolver = new IgnorePropertiesResolver(new[] { "id" })
                        });
                        FuncoesEditarML.EditarPrecoQuantidadeItem(reader["Idproduto_ec"].ToString(), accessToken, json);
                    }
                    else
                    {
                        if ((idProdutoECcontrole != reader["Idproduto_ec"].ToString()) && (idProdutoECcontrole != ""))
                        {
                            VariacaoPreco listaPreco = new VariacaoPreco();
                            listaPreco.variations = preco;
                            var json = JsonConvert.SerializeObject(listaPreco);

                            FuncoesEditarML.EditarPrecoQuantidadeItem(idProdutoECcontrole, accessToken, json);

                            preco.Clear();
                        }
                        idProdutoECcontrole = reader["Idproduto_ec"].ToString();
                        preco.Add(new Preco(long.Parse(reader["Idprodutov_ec"].ToString()), Convert.ToDecimal(reader["valorSGE"].ToString())));
                    }
                }
                if (preco.Count > 0)
                {
                    VariacaoPreco listaPreco = new VariacaoPreco();
                    listaPreco.variations = preco;
                    var json = JsonConvert.SerializeObject(listaPreco);

                    FuncoesEditarML.EditarPrecoQuantidadeItem(idProdutoECcontrole, accessToken, json);

                    preco.Clear();
                }
            }
            reader.Close();
        }
    }
}
