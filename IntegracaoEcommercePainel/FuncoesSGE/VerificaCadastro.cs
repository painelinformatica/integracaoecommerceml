﻿using IntegracaoPainel.FuncoesSGE.FuncoesSGEBD;
using IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.FuncoesSGE
{
    public class VerificaCadastro
    {
        public static Endereco VerificaCadastraEndereco(string ipServidor, string banco, string uf, string nomeEstado, string nomeCidade, string bairro, string cep, string enderecoCompleto)
        {
            Endereco endereco = new Endereco();

            if (SelectSGE.VerificaEstadoCadastrado(ipServidor, banco, nomeEstado) == 0)
            {
                InsertSGE.CadastrarEstado(ipServidor, banco, 1, uf, nomeEstado);
            }
            endereco.Estado = SelectSGE.RetornaEstadoCadastrado(ipServidor, banco, nomeEstado);

            if (SelectSGE.VerificaCidadeCadastrada(ipServidor, banco, nomeCidade) == 0)
            {
                int idIbge = SelectSGE.RetornaIdIBGE(ipServidor, banco, nomeCidade, uf);
                InsertSGE.CadastrarCidade(ipServidor, banco, nomeCidade, uf, idIbge);
            }
            endereco.Cidade = SelectSGE.RetornaCidadeCadastrada(ipServidor, banco, nomeCidade);

            if (SelectSGE.VerificaBairroCadastrada(ipServidor, banco, bairro) == 0)
            {
                InsertSGE.CadastrarBairro(ipServidor, banco, bairro);
            }
            endereco.Bairro = SelectSGE.RetornaBairroCadastrado(ipServidor, banco, bairro);

            if (SelectSGE.VerificaCEPCadastrado(ipServidor, banco, Convert.ToInt32(cep)) == 0)
            {
                InsertSGE.CadastrarCEP(ipServidor, banco, Convert.ToInt32(cep), endereco.Cidade.IdCidade, "", enderecoCompleto, endereco.Bairro.IdBairro, uf);
            }
            endereco.Cep = SelectSGE.RetornaCepCadastrado(ipServidor, banco, Convert.ToInt32(cep));

            return endereco;
        }
    }
}
