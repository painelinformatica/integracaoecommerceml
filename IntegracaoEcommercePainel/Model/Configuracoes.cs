﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace IntegracaoPainel.Model
{
    public class Configuracoes
    {
        string baseXML = "c:\\painelinformatica\\";
        string caminhoXML = "ConfigGeral.xml";
        public Configuracoes(string baseXML)
        {
            this.baseXML = baseXML;
            this.Status = "Novo";
            Integracoes = new List<Integracoes>();
        }

        public Configuracoes(List<Integracoes> integracoes, string status)
        {
            Integracoes = integracoes;
            Status = status;
        }

        public List<Integracoes> Integracoes { get; set; }
        public string Status { get; set; }

        public void EscreverXML(Configuracoes configuracoes)
        {
            var xml = new XDocument(new XElement("Configuracoes",
                                        configuracoes.Integracoes.Select(i => new XElement("Integracoes",
                                            new XElement("Servidor", i.Servidor),
                                            new XElement("Habilitado", i.Habilitado),
                                            new XElement("Empresa", i.Empresa),
                                            new XElement("Loja", i.Loja),
                                            new XElement("Tipo", i.Tipo),
                                            new XElement("ArquivoXML", i.ArquivoXML),
                                            new XElement("Cnpj", i.Cnpj)
                                                                    )
                                          ),
                                         new XElement("Status", configuracoes.Status)
                                     )
                                    );


            xml.Save(configuracoes.baseXML + caminhoXML);
        }

        public Configuracoes LerXML(string baseXML)
        {
            if (File.Exists(baseXML + caminhoXML))
            {
                var xml = XDocument.Load(baseXML + caminhoXML);
                Configuracoes configuracoes = new Configuracoes(baseXML);


                Integracoes[] integracoes;
                integracoes = xml.Element("Configuracoes").Elements("Integracoes")
                                   .Select(x => new Integracoes(bool.Parse(x.Element("Habilitado").Value),
                                                            x.Element("Servidor").Value,
                                                            x.Element("Empresa").Value,
                                                            x.Element("Loja").Value,
                                                            x.Element("Tipo").Value,
                                                            x.Element("ArquivoXML").Value,
                                                            x.Element("Cnpj").Value
                                                           )).ToArray();

                configuracoes = new Configuracoes(
                        integracoes.ToList(),
                        xml.Element("Configuracoes").Element("Status").Value
                    );
                return configuracoes;
            }
            else
            {
                return new Configuracoes(baseXML);
            }

        }
    }
}
