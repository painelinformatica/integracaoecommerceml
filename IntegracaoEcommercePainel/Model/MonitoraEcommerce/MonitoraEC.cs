﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.MonitoraEcommerce
{
    public class MonitoraEC
    {
        public MonitoraEC()
        {
        }

        public MonitoraEC(string idproduto, int idLoja, int idProdutoV, string sku, string ecommerce, decimal valorVendaAtualEc, decimal quantidadeAtualEc)
        {
            IdProduto = idproduto;
            IdLoja = idLoja;
            IdProdutoV = idProdutoV;
            Sku = sku;
            Ecommerce = ecommerce;
            ValorVendaAtualEc = valorVendaAtualEc;
            QuantidadeAtualEc = quantidadeAtualEc;
        }

        public string IdProduto { get; set; }
        public int IdLoja { get; set; }
        public int IdProdutoV { get; set; }
        public string Sku { get; set; }
        public string Ecommerce { get; set; }
        public decimal ValorVendaAtualEc { get; set; }
        public decimal QuantidadeAtualEc { get; set; }
    }
}
