﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model
{
    public class Loja
    {
        public Loja()
        {
        }

        public Loja(int index, string servidor, string banco, string idloja, string loja, string cnpj, string cnpjLimpo)
        {
            this.index = index;
            this.servidor = servidor;
            this.banco = banco;
            this.idloja = idloja;
            this.loja = loja;
            this.cnpj = cnpj;
            this.cnpjLimpo = cnpjLimpo;
            
        }

        public int index { get; set; }
        public string servidor { get; set; }
        public string banco { get; set; }
        public string idloja { get; set; }
        public string loja { get; set; }
        public string cnpj { get; set; }
        public string cnpjLimpo { get; set; }
    }
}
