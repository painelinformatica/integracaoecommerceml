﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE
{
    public class BuscarDataHoraCallbackSGE
    {
        public BuscarDataHoraCallbackSGE()
        {
        }

        public BuscarDataHoraCallbackSGE(string idCallback, string tipoCallback, DateTime dataHoraConsulta, string statusConsulta)
        {
            IdCallback = idCallback;
            TipoCallback = tipoCallback;
            DataHoraConsulta = dataHoraConsulta;
            StatusConsulta = statusConsulta;
        }

        public string IdCallback { get; set; }
        public string TipoCallback { get; set; }
        public DateTime DataHoraConsulta { get; set; }
        public string StatusConsulta { get; set; }
    }
}
