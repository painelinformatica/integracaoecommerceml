﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class ItemPedidoDeVenda
    {
        public ItemPedidoDeVenda()
        {

        }

        public ItemPedidoDeVenda(string idProduto, int idProdutoV, string descricao ,string idUnidade)
        {
            IdProduto = idProduto;
            IdProdutoV = idProdutoV;
            Descricao = descricao;
            IdUnidade = idUnidade;
        }

        public string IdProduto { get; set; }
        public int IdProdutoV { get; set; }
        public string Descricao { get; set; }
        public string IdUnidade { get; set; }
    }
}
