﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class Estado
    {
        public Estado()
        {
        }

        public Estado(int idPais, string uf, string nomeEstado)
        {
            IdPais = idPais;
            Uf = uf;
            NomeEstado = nomeEstado;
        }

        public int IdPais { get; set; }
        public string Uf { get; set; }
        public string NomeEstado { get; set; }

    }
}
