﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class PedidoDeVenda
    {
        public PedidoDeVenda()
        {
        }

        public PedidoDeVenda(int idLoja, DateTime dataPedido, int idCadastro, int idCadastroE, decimal valorTotal, decimal valorProduto, int quantidadeItens, string status, string idOrder, string origem, string idEnvios)
        {
            IdLoja = idLoja;
            DataPedido = dataPedido;
            IdCadastro = idCadastro;
            IdCadastroE = idCadastroE;
            ValorTotal = valorTotal;
            ValorProduto = valorProduto;
            QuantidadeItens = quantidadeItens;
            Status = status;
            IdOrder = idOrder;
            Origem = origem;
            IdEnvios = idEnvios;
        }
        public int IdLoja { get; set; }
        public DateTime DataPedido { get; set; }
        public int IdCadastro { get; set; }
        public int IdCadastroE { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal ValorProduto { get; set; }
        public int QuantidadeItens { get; set; }
        public string Status { get; set; }
        public string IdOrder { get; set; }
        public string Origem { get; set; }
        public string IdEnvios { get; set; }
    }
}
