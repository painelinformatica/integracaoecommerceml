﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class Bairro
    {
        public Bairro()
        {
        }

        public Bairro(int idBairro, string nomeBairro)
        {
            IdBairro = idBairro;
            NomeBairro = nomeBairro;
        }

        public int IdBairro { get; set; }
        public string NomeBairro { get; set; }
    }
}
