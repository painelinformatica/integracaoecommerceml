﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class StatusPedidoVenda
    {
        public StatusPedidoVenda()
        {
        }

        public StatusPedidoVenda(int idStatusEc, string ecommerce, string statusEc, string statusSge)
        {
            IdStatusEc = idStatusEc;
            Ecommerce = ecommerce;
            StatusEc = statusEc;
            StatusSge = statusSge;
        }

        public int IdStatusEc { get; set; }
        public string Ecommerce { get; set; }
        public string StatusEc { get; set; }
        public string StatusSge { get; set; }
    }
}
