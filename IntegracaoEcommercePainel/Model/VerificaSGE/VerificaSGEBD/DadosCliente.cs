﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class DadosCliente
    {
        public DadosCliente()
        {
        }

        public DadosCliente(int idCadastro, string nome, int idCadastroP, string tipoPessoa, string tipoCadastro, string sexo, DateTime dataCadastro, string rg, string cpf, string bloqueado, string ativo, int idAtividade)
        {
            IdCadastro = idCadastro;
            Nome = nome;
            IdCadastroP = idCadastroP;
            TipoPessoa = tipoPessoa;
            TipoCadastro = tipoCadastro;
            Sexo = sexo;
            DataCadastro = dataCadastro;
            Rg = rg;
            Cpf = cpf;
            Bloqueado = bloqueado;
            Ativo = ativo;
            IdAtividade = idAtividade;
        }

        public int IdCadastro { get; set; }
        public string Nome { get; set; }
        public int IdCadastroP { get; set; }
        public string TipoPessoa { get; set; }
        public string TipoCadastro { get; set; }
        public string Sexo { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Rg { get; set; }
        public string Cpf { get; set; }
        public string Bloqueado { get; set; }
        public string Ativo { get; set; }
        public int IdAtividade { get; set; }
    }
}
