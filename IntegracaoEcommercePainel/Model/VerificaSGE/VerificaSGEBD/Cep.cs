﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class Cep
    {
        public Cep()
        {
        }

        public Cep(int numeroCep, int idCidade, string logradouro, string endereco, int idBairro, string bairro, string uf)
        {
            NumeroCep = numeroCep;
            IdCidade = idCidade;
            Endereco = endereco;
            IdBairro = idBairro;
            Bairro = bairro;
            Uf = uf;
        }

        public int NumeroCep { get; set; }
        public int IdCidade { get; set; }
        public string Logradouro { get; set; }
        public string Endereco { get; set; }
        public int IdBairro { get; set; }
        public string Bairro { get; set; }
        public string Uf { get; set; }
    }
}
