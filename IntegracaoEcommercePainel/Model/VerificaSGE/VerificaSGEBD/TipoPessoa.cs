﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class TipoPessoa
    {
        public TipoPessoa()
        {
        }

        public TipoPessoa(string tipoPessoa, string cpf, string cnpj)
        {
            TpPessoa = tipoPessoa;
            Cpf = cpf;
            Cnpj = cnpj;
        }
        
        public string TpPessoa { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }
    }
}
