﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class Endereco
    {
        public Endereco()
        {
        }

        public Endereco(Bairro bairro, Cep cep, Cidade cidade, Estado estado)
        {
            Bairro = bairro;
            Cep = cep;
            Cidade = cidade;
            Estado = estado;
        }

        public Bairro Bairro { get; set; }
        public Cep Cep { get; set; }
        public Cidade Cidade { get; set; }
        public Estado Estado { get; set; }
    }
}
