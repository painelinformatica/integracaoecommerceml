﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class FichaExtraCliente
    {
        public FichaExtraCliente()
        {
        }

        public FichaExtraCliente(long nrSequencia, int idCadastro, int idCadastroe, string telefone, string cnpj, string cadastroAtivo, string inscricaoEstadual, int tipoInscricaoEstadual, int idPessoal, int idLoja, 
                                 int cep, string endereco, string numero, int idBairro, int idCidade, string uf, string nomeEstado)
        {
            NrSequencia = nrSequencia;
            IdCadastro = idCadastro;
            IdCadastroe = idCadastroe;
            Telefone = telefone;
            Cnpj = cnpj;
            CadastroAtivo = cadastroAtivo;
            InscricaoEstadual = inscricaoEstadual;
            TipoInscricaoEstadual = tipoInscricaoEstadual;
            IdPessoal = idPessoal;
            IdLoja = idLoja;
            Cep = cep;
            Endereco = endereco;
            Numero = numero;
            IdBairro = idBairro;
            IdCidade = idCidade;
            Uf = uf;
            NomeEstado = nomeEstado;

        }

        public long NrSequencia { get; set; }
        public int IdCadastro { get; set; }
        public int IdCadastroe { get; set; }
        public string Telefone { get; set; }
        public string Cnpj { get; set; }
        public string CadastroAtivo { get; set; }
        public string InscricaoEstadual { get; set; }
        public int TipoInscricaoEstadual { get; set; }
        public int IdPessoal { get; set; }
        public int IdLoja { get; set; }
        public int Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public int IdBairro { get; set; }
        public int IdCidade { get; set; }
        public string Uf { get; set; }
        public string NomeEstado { get; set; }
    }
}
