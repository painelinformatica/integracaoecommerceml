﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD
{
    public class Cidade
    {
        public Cidade()
        {
        }

        public Cidade(int idCidade, string nomeCidade, string uf, int idIBGE)
        {
            IdCidade = idCidade;
            NomeCidade = nomeCidade;
            Uf = uf;
            IdIBGE = idIBGE;
        }

        public int IdCidade { get; set; }
        public string NomeCidade { get; set; }
        public string Uf { get; set; }
        public int IdIBGE { get; set; }
    }
}
