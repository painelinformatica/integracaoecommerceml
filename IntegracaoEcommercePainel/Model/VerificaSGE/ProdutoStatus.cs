﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE
{
    public class ProdutoStatus
    {
        public ProdutoStatus()
        {
        }

        public ProdutoStatus(string idProduto, string idLoja, string status)
        {
            IdProduto = idProduto;
            IdLoja = idLoja;
            Status = status;
        }

        public string IdProduto { get; set; }
        public string IdLoja { get; set; }
        public string Status { get; set; }
    }
}
