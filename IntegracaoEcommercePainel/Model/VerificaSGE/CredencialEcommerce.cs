﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoEcommercePainel.Model.VerificaSGE
{
    public class CredencialEcommerce
    {
        public CredencialEcommerce()
        {
        }

        public CredencialEcommerce(int idCredencialEcommerce, string ipServidor, string nomeBanco, int idLoja, string nomeEcommerce, int idDadosdeAcesso)
        {
            IdCredencialEcommerce = idCredencialEcommerce;
            IpServidor = ipServidor;
            NomeBanco = nomeBanco;
            IdLoja = idLoja;
            NomeEcommerce = nomeEcommerce;
            IdDadosDeAcesso = idDadosdeAcesso;
        }

        public int IdCredencialEcommerce { get; set; }
        public string IpServidor { get; set; }
        public string NomeBanco { get; set; }
        public int IdLoja { get; set; }
        public string NomeEcommerce { get; set; }
        public int IdDadosDeAcesso { get; set; }
    }
}
