﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model.VerificaSGE
{
    public class VerificaToken
    {
        public VerificaToken()
        {
        }

        public VerificaToken(string valorSGE, string accessToken, string idAplicacaoEC, string valorVendaEcommerce, int idOperacaoEcommerce, int idPessoalEcommerce)
        {
            ValorSGE = valorSGE;
            AccessToken = accessToken;
            IdAplicacaoEC = idAplicacaoEC;
            ValorVendaEcommerce = valorVendaEcommerce;
            IdOperacaoEcommerce = idOperacaoEcommerce;
            IdPessoalEcommerce = idPessoalEcommerce;
        }

        public string ValorSGE { get; set; }
        public string AccessToken { get; set; }
        public string IdAplicacaoEC { get; set; }
        public string ValorVendaEcommerce { get; set; }
        public int IdOperacaoEcommerce { get; set; }
        public int IdPessoalEcommerce { get; set; }
    }
}
