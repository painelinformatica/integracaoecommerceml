﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model
{
    public static class SessaoConexao
    {
        private static string idUsuario;
        private static string nickNameUsuario;
        private static string valorSGE;
        private static string accessToken;
        private static string ipServidor;
        private static string nomeBanco;
        private static int idLoja;
        private static string ecommerce;
        private static string usuarioLogadoSGE;

        public static String IdUsuario
        {
            get { return SessaoConexao.idUsuario; }
            set { SessaoConexao.idUsuario = value; }
        }

        public static String NickNameUsuario
        {
            get { return SessaoConexao.nickNameUsuario; }
            set { SessaoConexao.nickNameUsuario = value; }
        }

        public static String ValorSGE
        {
            get { return SessaoConexao.valorSGE; }
            set { SessaoConexao.valorSGE = value; }
        }

        public static String AccessToken
        {
            get { return SessaoConexao.accessToken; }
            set { SessaoConexao.accessToken = value; }
        }

        public static String IpServidor
        {
            get { return SessaoConexao.ipServidor; }
            set { SessaoConexao.ipServidor = value; }
        }

        public static String BancoDeDados
        {
            get { return SessaoConexao.nomeBanco; }
            set { SessaoConexao.nomeBanco = value; }
        }

        public static int IdLoja
        {
            get { return SessaoConexao.idLoja; }
            set { SessaoConexao.idLoja = value; }
        }

        public static String Ecommerce
        {
            get { return SessaoConexao.ecommerce; }
            set { SessaoConexao.ecommerce = value; }
        }

        public static String UsuarioLogadoSGE
        {
            get { return SessaoConexao.usuarioLogadoSGE; }
            set { SessaoConexao.usuarioLogadoSGE = value; }
        }
    }
}
