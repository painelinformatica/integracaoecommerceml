﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model
{
    public class NotificaoMenssagem
    {
        public NotificaoMenssagem()
        {
        }

        public NotificaoMenssagem(string mensagem, string titulo)
        {
            Menssagem = mensagem;
            Titulo = titulo;
        }

        public string Menssagem { get; set; }

        public string Titulo { get; set; }
    }
}
