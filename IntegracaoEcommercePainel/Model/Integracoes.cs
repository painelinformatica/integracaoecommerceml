﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model
{
    public class Integracoes
    {
        public Integracoes()
        {
        }

        public Integracoes(bool habilitado, string servidor, string empresa, string loja, string tipo, string arquivoXML, string cnpj)
        {
            Habilitado = habilitado;
            Servidor = servidor;
            Empresa = empresa;
            Loja = loja;
            Tipo = tipo;
            ArquivoXML = arquivoXML;
            Cnpj = cnpj;
        }

        public bool Habilitado { get; set; }
        public string Servidor { get; set; }
        public string Empresa { get; set; }
        public string Loja { get; set; }
        public string Cnpj { get; set; }
        public string Tipo { get; set; }
        public string ArquivoXML { get; set; }
    }
}
