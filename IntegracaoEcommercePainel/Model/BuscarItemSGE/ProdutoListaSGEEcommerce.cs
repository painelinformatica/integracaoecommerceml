﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoEcommercePainel.Model.BuscarItemSGE
{
    public class ProdutoListaSGEEcommerce
    {
        public ProdutoListaSGEEcommerce()
        {
        }

        public ProdutoListaSGEEcommerce(string idProduto, string produtoSKU, long idVariacaoEc, string variacaoEc, string tipoVariacaoSGE, string variacaoSGE, decimal quantidadeProdutoSGE, decimal precoProdutoSGE, int idProdutoV)
        {
            IdProduto = idProduto;
            ProdutoSKU = produtoSKU;
            IdVariacaoEC = idVariacaoEc;
            VariacaoEC = variacaoEc;
            TipoVariacaoSGE = tipoVariacaoSGE;
            VariacaoSGE = variacaoSGE;
            QuantidadeProdutoSGE = quantidadeProdutoSGE;
            PrecoProdutoSGE = precoProdutoSGE;
            IdProdutoV = idProdutoV;
        }

        public string IdProduto { get; set; }
        public string ProdutoSKU { get; set; }
        public long IdVariacaoEC { get; set; }
        public string VariacaoEC { get; set; }
        public string TipoVariacaoSGE { get; set; }
        public string VariacaoSGE { get; set; }
        public decimal QuantidadeProdutoSGE { get; set; }
        public decimal PrecoProdutoSGE { get; set; }
        public int IdProdutoV { get; set; }
    }
}
