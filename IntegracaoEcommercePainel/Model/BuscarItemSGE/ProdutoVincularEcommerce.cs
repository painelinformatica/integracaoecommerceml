﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace IntegracaoEcommercePainel.Model.BuscarItemSGE
{
    public class ProdutoVincularEcommerce
    {
        public ProdutoVincularEcommerce()
        {
        }

        public ProdutoVincularEcommerce(string idProduto, string descricaoSGE, string skuProduto, decimal quantidadeProduto, decimal precoProduto, string cor, Visibility corVisivel,
                                        string tamanho, Visibility tamanhoVisivel, string voltagem, Visibility voltagemVisivel, string sabor, Visibility saborVisivel, int idProdutoV, string variacao)
        {
            IdProduto = idProduto;
            DescricaoSGE = descricaoSGE;
            SkuProduto = skuProduto;
            QuantidadeProduto = quantidadeProduto;
            PrecoProduto = precoProduto;
            Cor = cor;
            CorVisivel = corVisivel;
            Tamanho = tamanho;
            TamanhoVisivel = tamanhoVisivel;
            Voltagem = voltagem;
            VoltagemVisivel = voltagemVisivel;
            Sabor = sabor;
            SaborVisivel = saborVisivel;
            IdProdutoV = idProdutoV;
            Variacao = variacao;

        }

        public string IdProduto { get; set; }
        public string DescricaoSGE { get; set; }
        public string SkuProduto { get; set; }
        public decimal QuantidadeProduto { get; set; }
        public decimal PrecoProduto { get; set; }
        public string Cor { get; set; }
        public Visibility CorVisivel { get; set; }
        public string Tamanho { get; set; }
        public Visibility TamanhoVisivel { get; set; }
        public string Voltagem { get; set; }
        public Visibility VoltagemVisivel { get; set; }
        public string Sabor { get; set; }
        public Visibility SaborVisivel { get; set; }
        public int IdProdutoV { get; set; }
        public string Variacao { get; set; }
    }
}
