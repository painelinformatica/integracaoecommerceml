﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoPainel.Model
{
    public class ConexaoBancodeDados
    {
        public ConexaoBancodeDados()
        {
        }

        public ConexaoBancodeDados(string ipServ, string banco, string loja)
        {
            IpServidor = ipServ;
            Banco      = banco;
            IdLoja     = loja;
        }

        public string IpServidor { get; set; }
        public string Banco { get; set; }
        public string IdLoja { get; set; }
    }
}
