﻿using IntegracaoEcommercePainel.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntegracaoPainel.View
{
    public partial class EditarConfiguracao : Window
    {
        private string servidor;
        private string bd;
        private string nick;
        private int idOper;
        private int idPess;
        private string itemOper;
        private string itemPessoal;

        public EditarConfiguracao(string ipServidor, string banco, string nickname, int idOperacao, int idPessoal, string valorDeVenda)
        {
            InitializeComponent();

            servidor = ipServidor;
            bd = banco;
            nick = nickname;
            idOper = idOperacao;
            idPess = idPessoal;

            CarregaComboBoxValorProdutoSGE();
            CarregarPessoal(ipServidor, banco);
            CarregarOperacao(ipServidor, banco);

            cbxPreco.SelectedItem = valorDeVenda;
            cbxPessoal.SelectedItem = itemPessoal;
            cbxOperacao.SelectedItem = itemOper;
        }

        public void CarregaComboBoxValorProdutoSGE()
        {
            cbxPreco.Items.Add("Vlr_venda1");
            cbxPreco.Items.Add("Vlr_venda2");
            cbxPreco.Items.Add("Vlr_venda3");
            cbxPreco.Items.Add("Vlr_venda4");
            cbxPreco.Items.Add("Vlr_venda5");
            cbxPreco.Items.Add("Vlr_venda6");
        }

        public void CarregarPessoal(string ipServidor, string banco)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Idpessoal, Nome FROM Tpessoal";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    cbxPessoal.Items.Add(reader["Idpessoal"].ToString() + " - " + reader["Nome"].ToString());
                    if (idPess == Convert.ToInt32(reader["Idpessoal"].ToString()))
                    {
                        itemPessoal = reader["Idpessoal"].ToString() + " - " + reader["Nome"].ToString();
                    }
                }
                reader.Close();
            }
        }

        public void CarregarOperacao(string ipServidor, string banco)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Idoper, Operacao FROM Toper WHERE Tp_oper = 0";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    cbxOperacao.Items.Add(reader["Idoper"].ToString() + " - " + reader["Operacao"].ToString());
                    if(idOper == Convert.ToInt32(reader["Idoper"].ToString()))
                    {
                        itemOper = reader["Idoper"].ToString() + " - " + reader["Operacao"].ToString();
                    }
                }
                reader.Close();
            }
        }

        private void Salvar_Click(object sender, RoutedEventArgs e)
        {
            String[] pessoal = cbxPessoal.Text.Split(" - ");
            String[] operacao = cbxOperacao.Text.Split(" - ");
            int idPessoal = Convert.ToInt32(pessoal[0]);
            int idOperacao = Convert.ToInt32(operacao[0]);
            ConexaoBancoDados.GetConexao(servidor, bd);
            string sql = "UPDATE Tbdadosdeacessoml SET Vlr_pegar_ecommerce = '"+ cbxPreco.Text+ "', Idoper_pegar_ecommerce = "+idOperacao+", Idpessoal_pegar_ecommerce = "+idPessoal+" WHERE Nicknameusuario = '"+nick+"'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Configuração Salva!", "Salvar Configuração");

            this.Close();
        }
    }
}
