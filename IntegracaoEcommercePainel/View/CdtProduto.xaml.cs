﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.Categoria;
using IntegracaoPainel.ModelML.Imagem;
using IntegracaoPainel.ModelML.Variacao;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using IntegracaoPainel.ModelML.UploadImagem;
using IntegracaoPainel.RetornoItemML;
using IntegracaoPainel.Model.MonitoraEcommerce;
using System.Text.RegularExpressions;
using IntegracaoPainel.ModelML.EnvioItemML;

namespace IntegracaoPainel.View
{
    public partial class CdtProduto : Window
    {
        List<AtributosCategoriaML> camposCategoria;
        List<MonitoraEC> monitora = new List<MonitoraEC>();

        String[] variacao;
        ObservableCollection<VariacaoDataGridML> variacaoDgML = new ObservableCollection<VariacaoDataGridML>();
        ObservableCollection<ImagemUrlAddDataGridML> listaImagensAdd = new ObservableCollection<ImagemUrlAddDataGridML>();
        ObservableCollection<ImgUrlDataGridML> listaImagensVariacoes = new ObservableCollection<ImgUrlDataGridML>();
        ObservableCollection<VariacaoDataGridSGE> variacaoDgSge = new ObservableCollection<VariacaoDataGridSGE>();
        ObservableCollection<VariacaoDataGridAddImagem> variacaoSgeAddImg = new ObservableCollection<VariacaoDataGridAddImagem>();

        private string idProduto;
        private string descricaoProduto;
        private decimal valorProduto;
        private string tpVariacao;
        private string tpVariacaoSGE;
        private string variacaoSGE;
        private string idProdutoEcommerce;
        /*private bool editarProd;

        private string corJson;
        private string tamanhoJson;
        private string voltagemJson;

        private string tipoDeAtributos;
        private string atributosCombinados;*/

        private bool reenviarPrd;
        private int variacaoErroSGE = 0;
        private bool produtoComVariacao = true;
        public bool erroVariacaoSGE = false;

        public CdtProduto(string idProdSGE, string descricaoProdSGE, decimal valorProdSGE, bool reenviarProduto)
        {
            InitializeComponent();

            if(valorProdSGE <= 0)
            {
                MessageBox.Show("Você não pode enviar produto com o preço zerado ou negativado!", "Erro ao cadastrar Produto");
                erroVariacaoSGE = true;
                this.Close();
            }
            else
            {
                idProduto = idProdSGE;
                descricaoProduto = descricaoProdSGE.Length > 60 ? descricaoProdSGE.Substring(0, 60) : descricaoProdSGE;
                valorProduto = valorProdSGE;
                reenviarPrd = reenviarProduto;

                CarregarValoresSGE();
                CarregarCamposFixosFormulario();
                CarregarVariacaoSGEProduto(idProdSGE);

                if (reenviarProduto == false)
                {
                    EsconderOuMostrarCampos(Visibility.Collapsed);
                    BuscarNomeProdutoML();
                }
                else
                {
                    CarregarValoresParaReenviarProduto();
                }
            }
        }

        private void CarregarValoresSGE()
        {
            txtNomeProduto.Text = descricaoProduto;
            txtPreco.Text = valorProduto.ToString();
        }

        private void BuscarNomeProduto_Click(object sender, RoutedEventArgs e)
        {
            BuscarNomeProdutoML();
        }

        private void txtNomeProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarNomeProdutoML();
            }
        }

        private void BuscarNomeProdutoML()
        {
            txtTipoVariacao.Items.Clear();
            SelecionarCondicaoItem.Items.Clear();
            SelecionarTipoDeGarantia.Items.Clear();
            variacaoDgML = new ObservableCollection<VariacaoDataGridML>();

            var retornaCategoriaRecomendada = FuncoesRespostaML.PreditorCategoriaML(txtNomeProduto.Text);

            for (int i = 0; i < retornaCategoriaRecomendada.Count; i++)
            {
                txtCategoria.Text = retornaCategoriaRecomendada[i].category_id + " - " + retornaCategoriaRecomendada[i].category_name;
                if (retornaCategoriaRecomendada[i].attributes != null)
                {
                    CarregarCamposSelecionadosPreditorCategoria(retornaCategoriaRecomendada[i].category_id, retornaCategoriaRecomendada[i].attributes);
                }
            }
            EsconderOuMostrarCampos(Visibility.Visible);
        }

        private void SelecionarCategoria_Click(object sender, RoutedEventArgs e)
        {
            String[] idCategoria = txtCategoria.Text.Split(" - ");
            CategoriasML categoriasML = new CategoriasML(idCategoria[0]);
            var categoriaSelecionada = categoriasML.ShowDialog();

            if((categoriaSelecionada.Item1 == null)||(categoriaSelecionada.Item1 == ""))
            {
                txtCategoria.Text = idCategoria[0] + " - " + idCategoria[1];
            }
            else
            {
                txtCategoria.Text = categoriaSelecionada.Item1 + " - " + categoriaSelecionada.Item2;
                SelecionarCondicaoItem.Items.Clear();
                SelecionarTipoDeGarantia.Items.Clear();
                CarregarCamposDaCategoria(categoriaSelecionada.Item1);
            }
        }

        public void CarregarCamposSelecionadosPreditorCategoria(string idCategoria, List<AtributosPreditorCategoriaML> atributosPreditor)
        {
            CamposCategoria(idCategoria);

            for (int b = 0; b < atributosPreditor.Count; b++)
            {
                for (int c = 0; c < txtTipoVariacao.Items.Count; c++)
                {
                    variacao = txtTipoVariacao.Items[c].ToString().Split(" - ");
                    if (atributosPreditor[b].name == variacao[0])
                    {
                        txtTipoVariacao.Items.RemoveAt(c);
                        variacaoDgML.Add(new VariacaoDataGridML(variacao[0] + " - " + variacao[1], atributosPreditor[b].value_name));
                    }
                }
            }

        }

        public void CarregarCamposDaCategoria(string idCategoriaSelecionada)
        {
            CamposCategoria(idCategoriaSelecionada);
        }

        private void CamposCategoria(string idCategoria)
        {
            camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(idCategoria);
            var garantiaCategoria = FuncoesRespostaML.GarantiaCategoriaML(idCategoria);
            List<string> variacaoCategoriaOpcional = new List<string>();
            List<string> variacaoCategoriaObrigatoria = new List<string>();
            txtTipoVariacao.Items.Clear();

            for (int i = 0; i < camposCategoria.Count; i++)
            {
                if (camposCategoria[i].id == "ITEM_CONDITION")
                {
                    for (int a = 0; a < camposCategoria[i].values.Count; a++)
                    {
                        SelecionarCondicaoItem.Items.Add(camposCategoria[i].values[a].name);
                    }
                }
                else if ((camposCategoria[i].tags.required == true) || (camposCategoria[i].tags.catalog_required == true))
                {
                    if (produtoComVariacao == false)
                    {
                        if((camposCategoria[i].name != "Linha"))
                        {
                            variacaoCategoriaObrigatoria.Add(camposCategoria[i].name + " - Obrigatorio");
                        }
                    }
                    else
                    {
                        if ((camposCategoria[i].name != "Cor") && (camposCategoria[i].name != "Tamanho") && (camposCategoria[i].name != "Sabor") && (camposCategoria[i].name != "Linha") && (camposCategoria[i].name != "Tamanho do capacete"))
                        {
                            variacaoCategoriaObrigatoria.Add(camposCategoria[i].name + " - Obrigatorio");
                        }
                    }
                    
                } 
                else if ((camposCategoria[i].hierarchy == "FAMILY")||(camposCategoria[i].hierarchy == "PRODUCT_IDENTIFIER") || (camposCategoria[i].hierarchy == "CHILD_PK") || (camposCategoria[i].hierarchy == "PARENT_PK"))
                {
                    if( produtoComVariacao == false)
                    {
                        if ((camposCategoria[i].name != "Linha"))
                        {
                            variacaoCategoriaOpcional.Add(camposCategoria[i].name + " - Opcional");
                        }
                    }
                    else
                    {
                        if ((camposCategoria[i].name != "Cor") && (camposCategoria[i].name != "Tamanho") && (camposCategoria[i].name != "Sabor") && (camposCategoria[i].name != "Linha") && (camposCategoria[i].name != "Tamanho do capacete"))
                        {
                            variacaoCategoriaOpcional.Add(camposCategoria[i].name + " - Opcional");
                        }
                    }
                }
            }

            variacaoCategoriaObrigatoria = variacaoCategoriaObrigatoria.OrderBy(x => x).ToList();
            variacaoCategoriaOpcional = variacaoCategoriaOpcional.OrderBy(x => x).ToList();

            for (int i = 0; i < variacaoCategoriaObrigatoria.Count(); i++)
            {
                txtTipoVariacao.Items.Add(variacaoCategoriaObrigatoria[i]);
            }

            for (int i = 0; i < variacaoCategoriaOpcional.Count(); i++)
            {
                txtTipoVariacao.Items.Add(variacaoCategoriaOpcional[i]);
            }

            for (int i = 0; i < garantiaCategoria.Count; i++)
            {
                if (garantiaCategoria[i].id == "WARRANTY_TYPE")
                {
                    for (int a = 0; a < garantiaCategoria[i].values.Count; a++)
                    {
                        SelecionarTipoDeGarantia.Items.Add(garantiaCategoria[i].values[a].name);
                    }
                }
            }

            variacaoDgML.Clear();
            dataGridVariacoes.ItemsSource = variacaoDgML;
        }

        public void CampoVariacaoTextBox(string nomeVariacao, int marginL, int marginT, bool campoUnitario)
        {
            var nome = Util.RemoverAcentoEspaco(nomeVariacao);

            TextBox txtOutro = new TextBox();
            txtOutro.Name = "txt" + nome;
            txtOutro.Height = 30;
            txtOutro.Width = 170;
            txtOutro.Margin = new Thickness(marginL, marginT, 0, 0);
            txtOutro.Foreground = Brushes.White;
            txtOutro.BorderBrush = Brushes.White;
            HintAssist.SetHelperText(txtOutro, "Digite " + nomeVariacao);
            txtOutro.HorizontalAlignment = HorizontalAlignment.Left;
            if(campoUnitario == true)
            {
                txtOutro.PreviewTextInput += TxtOutro_PreviewTextInput;
            }

            variacoeSP.Children.Add(txtOutro);
        }

        public void CampoVariacaoComboBox(string nomeCampo, int countValorCampos, List<ValorAtributoCategoriaML> valores)
        {
            var nome = Util.RemoverAcentoEspaco(nomeCampo);

            ComboBox cbxDinamico = new ComboBox();
            cbxDinamico.Name = "Selecionar" + nome;
            cbxDinamico.Width = 170;
            cbxDinamico.Height = 30;
            cbxDinamico.Foreground = Brushes.White;
            cbxDinamico.BorderBrush = Brushes.White;
            cbxDinamico.Margin = new Thickness(0, 0, 0, 0);
            cbxDinamico.HorizontalAlignment = HorizontalAlignment.Left;
            HintAssist.SetHelperText(cbxDinamico, "Selecione " + nomeCampo);
            ColorZoneAssist.SetMode(cbxDinamico, ColorZoneMode.Inverted);

            cbxDinamico.SelectionChanged += CbxDinamico_SelectionChanged;

            for (int i = 0; i < countValorCampos; i++)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = valores[i].name;
                cbxDinamico.Items.Add(item);
            }

            cbxDinamico.Items.Add("Outro");

            variacoeSP.Children.Add(cbxDinamico);
        }

        public void CampoUnidadePermitidasComboBox(string nomeCampo, int countValorCampos, List<ValorAtributoCategoriaML> valores)
        {
            var nome = Util.RemoverAcentoEspaco(nomeCampo);

            ComboBox cbxDinamico = new ComboBox();
            cbxDinamico.Name = nome;
            cbxDinamico.Width = 170;
            cbxDinamico.Height = 30;
            cbxDinamico.Foreground = Brushes.White;
            cbxDinamico.BorderBrush = Brushes.White;
            cbxDinamico.Margin = new Thickness(180, -30, 0, 0);
            cbxDinamico.HorizontalAlignment = HorizontalAlignment.Left;
            HintAssist.SetHelperText(cbxDinamico, "Selecione " + nomeCampo);
            ColorZoneAssist.SetMode(cbxDinamico, ColorZoneMode.Inverted);

            cbxDinamico.SelectionChanged += CbxDinamico_SelectionChanged;

            for (int i = 0; i < countValorCampos; i++)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = valores[i].name;
                cbxDinamico.Items.Add(item);
            }

            variacoeSP.Children.Add(cbxDinamico);
        }

        public void CarregarCamposFixosFormulario()
        {
            SelecionarTipoDeLista.Items.Add("gold_special - Clásica");
            SelecionarTipoDeLista.Items.Add("gold_pro - Premium");

            SelecionarPeriodoGarantia.Items.Add("dias");
            SelecionarPeriodoGarantia.Items.Add("meses");
            SelecionarPeriodoGarantia.Items.Add("anos");
        }

        public void CarregarVariacaoSGEProduto(string idProduto)
        {
            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
            string sql = "Select tpv.Idproduto, tpv.Idprodutov, tpv.Sku, tpv.Variacao, tpv.Idcor, tc.Cor, tpv.Idtamanho, tt.Tamanho, tpv.Idvoltagem, tv.Voltagem, tpv.Idsabor, ts.Sabor, tpv.Qde - isnull(vpe.QDE,0) - tpv.Qde_pend_ec as QtdDisponivelEC, " +
                         "tpi.Idgrupo, tg.Grupo, tg.Cont_variacao, tg.Cont_var_cor, tg.Cont_var_tamanho, tg.Cont_var_voltagem, tg.Cont_var_sabor " +
                         "from Tprodutov as tpv " +
                         "left outer join Tcor as tc on tc.Idcor = tpv.Idcor " +
                         "left outer join Ttamanho as tt on tt.Idtamanho = tpv.Idtamanho " +
                         "left outer join Tsabor as ts on ts.Idsabor = tpv.Idsabor " +
                         "left outer join Tvoltagem as tv on tv.Idvoltagem = tpv.Idvoltagem " +
                         "inner join Tprodutoi as tpi on tpi.Idproduto = tpv.Idproduto " +
                         "inner join Tgrupo as tg on tg.Idgrupo = tpi.Idgrupo " +
                         "left outer join VP_PEND_ESTOQUE as vpe on vpe.IDPRODUTO = tpv.Idproduto and vpe.IDLOJA = tpv.Idloja and vpe.IDPRODUTOV = tpv.Idprodutov "+ 
                         "where tpv.Idproduto = '" + idProduto + "' and Tpv.Idloja = " + SessaoConexao.IdLoja;

            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if(reader["Cont_variacao"].ToString() == "0")
                {
                    tpVariacaoSGE = "Sem Variação";
                    variacaoSGE = "Sem Variação";
                    produtoComVariacao = false;
                    variacaoDgSge.Add(new VariacaoDataGridSGE (tpVariacaoSGE, variacaoSGE, (decimal)reader["QtdDisponivelEC"], reader["Idprodutov"].ToString(), reader["Sku"].ToString(), true, false ));
                }  
                else if (reader["Cont_variacao"].ToString() == "3")
                {
                    int variacaoInconsistente = 0;
                    if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == true) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        tpVariacaoSGE = "Cor - Tamanho";

                        if((reader["Cor"].ToString() == "") || (reader["Tamanho"].ToString() == ""))
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacaoSGE = reader["Cor"].ToString() + " - " + reader["Tamanho"].ToString();
                        }
                    }
                    else if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == true))
                    {
                        tpVariacaoSGE = "Cor - Voltagem";

                        if ((reader["Cor"].ToString() == "") || (reader["Voltagem"].ToString() == ""))
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacaoSGE = reader["Cor"].ToString() + " - " + reader["Voltagem"].ToString();
                        }
                    }
                    else if (((bool)reader["Cont_var_cor"] == true) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        tpVariacaoSGE = "Cor";
                        if (reader["Cor"].ToString() == "")
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacaoSGE = reader["Cor"].ToString();
                        }
                    }
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == true) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        tpVariacaoSGE = "Tamanho";
                        if (reader["Tamanho"].ToString() == "")
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else 
                        {
                            variacaoSGE = reader["Tamanho"].ToString();
                        }
                        
                    }
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == true) && ((bool)reader["Cont_var_voltagem"] == false))
                    {
                        tpVariacaoSGE = "Sabor";
                        if (reader["Sabor"].ToString() == "")
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacaoSGE = reader["Sabor"].ToString();
                        }
                    }
                    else if (((bool)reader["Cont_var_cor"] == false) && ((bool)reader["Cont_var_tamanho"] == false) && ((bool)reader["Cont_var_sabor"] == false) && ((bool)reader["Cont_var_voltagem"] == true))
                    {
                        tpVariacaoSGE = "Voltagem";
                        if (reader["Voltagem"].ToString() == "")
                        {
                            variacaoSGE = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacaoSGE = reader["Voltagem"].ToString();
                        }
                    }
                    else
                    {
                        string msg = "Combinação de Variações inválida.";

                        MessageBox.Show("Varições inconsistentes com o Ecommerce!" + "\n" + msg + "\n" + "Favor Entrar em contato com o nosso suporte!", "Varições inconsistentes", MessageBoxButton.OK,
                        MessageBoxImage.Warning);
                        reader.Close();
                        erroVariacaoSGE = true;

                        return;
                    }

                    if(variacaoInconsistente == 0)
                    {
                        variacaoDgSge.Add(new VariacaoDataGridSGE (tpVariacaoSGE, variacaoSGE, (decimal)reader["QtdDisponivelEC"], reader["Idprodutov"].ToString(), reader["Sku"].ToString(), false,  true));
                    }
                    else
                    {
                        variacaoDgSge.Add(new VariacaoDataGridSGE (tpVariacaoSGE, variacaoSGE, (decimal)reader["QtdDisponivelEC"], reader["Idprodutov"].ToString(), reader["Sku"].ToString(), false, false ));
                        variacaoErroSGE++;
                    }
                }
                else
                {
                    string msg;
                    switch (reader["Cont_variacao"].ToString())
                    {
                        case "1":
                            msg = "Lote não é uma Variação válída para ecommerce.";
                            break;
                        case "2":
                            msg = "Grade não é uma Variação válida para ecommerce.";
                            break;
                        default:
                            msg = "Não possui variação válida cadastrada no sistema";
                            break;
                    }

                    MessageBox.Show("Varições inconsistentes com o Ecommerce!" + "\n" + msg + "\n" + "Favor Entrar em contato com o nosso suporte!", "Varições inconsistentes", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                    reader.Close();
                    erroVariacaoSGE = true;
                    return;
                }
            }
            reader.Close();

            if(variacaoErroSGE !=0)
            {
                MessageBox.Show("O produto possui variações inconsistentes no sistema." + "\n" + "Caso queira enviar essas variações para o ecommerce, " + "\n" + "corrija a variação no SGE antes de publicar o produto no Ecommerce.", "Varições inconsistentes", MessageBoxButton.OK,
                        MessageBoxImage.Warning);
            }

            if (variacaoDgSge.Count == 1)
            {
                variacaoDgSge[0].ItemSelecionadoEcommerce = true;
            }

            dataGridVariacoesSGE.ItemsSource = variacaoDgSge;
        }

        public void EsconderOuMostrarCampos(Visibility visibility)
        {
            iconTxtCategoria.Visibility = visibility;
            txtCategoria.Visibility = visibility;
            SelecionarCategoria.Visibility = visibility;

            iconTxtPreco.Visibility = visibility;
            txtPreco.Visibility = visibility;

            iconSelecionarCondicaoItem.Visibility = visibility;
            SelecionarCondicaoItem.Visibility = visibility;

            iconSelecionarTipoDeLista.Visibility = visibility;
            SelecionarTipoDeLista.Visibility = visibility;

            iconTxtTextoDescricao.Visibility = visibility;
            txtTextoDescricao.Visibility = visibility;

            iconSelecionarTipoDeGarantia.Visibility = visibility;
            SelecionarTipoDeGarantia.Visibility = visibility;

            icontxtQuantidadeGarantia.Visibility = visibility;
            txtQuantidadeGarantia.Visibility = visibility;
            SelecionarPeriodoGarantia.Visibility = visibility;

            iconTxtVariacao.Visibility = visibility;
            txtTipoVariacao.Visibility = visibility;

            dataGridVariacoes.Visibility = visibility;
            dataGridVariacoesSGE.Visibility = visibility;

            borderDgVariacao.Visibility = visibility;
            borderDgVariacaoSGE.Visibility = visibility;
            boderDgImagens.Visibility = visibility;

            btnSalvarProdutoBdJson.Visibility = visibility;
        }

        private void btnExcluirVariacao_Click(object sender, RoutedEventArgs e)
        {
            VariacaoDataGridML itemSelecionado = (VariacaoDataGridML)dataGridVariacoes.SelectedItem;

            for (int i = 0; i < variacaoDgML.Count; i++)
            {
                if (itemSelecionado.TipoAtributo == variacaoDgML[i].TipoAtributo)
                {
                    var a  = txtTipoVariacao.Items.Count;
                    txtTipoVariacao.Items.Insert(0,itemSelecionado.TipoAtributo);
                    variacaoDgML.RemoveAt(i);
                }
            }
            dataGridVariacoes.ItemsSource = variacaoDgML;
        }

        private void txtTipoVariacao_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            variacoeSP.Children.Clear();
            ComboBox cbxSelecionada = new ComboBox();
            cbxSelecionada = (ComboBox)sender;
            
            if(cbxSelecionada.SelectedItem != null) { 
                String[] itemCbx = e.AddedItems[0].ToString().Split(" - ");

                for (int i=0; i < camposCategoria.Count; i++)
                {
                    if(camposCategoria[i].name == itemCbx[0])
                    {
                        if(camposCategoria[i].values != null)
                        {
                            CampoVariacaoComboBox(camposCategoria[i].name, camposCategoria[i].values.Count, camposCategoria[i].values);
                        }
                        else
                        {
                            if(camposCategoria[i].value_type == "number_unit")
                            {
                                CampoVariacaoTextBox(camposCategoria[i].name, 0, 0, true);
                                SelecionarVariacao.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                CampoVariacaoTextBox(camposCategoria[i].name, 0, 0, false);
                                SelecionarVariacao.Visibility = Visibility.Visible;
                            }
                            
                        }

                        if (camposCategoria[i].value_type == "number_unit")
                        {
                            CampoUnidadePermitidasComboBox("Unidades Permitidas", camposCategoria[i].allowed_units.Count, camposCategoria[i].allowed_units);
                        }
                    }
                }
            }
        }

        private void CbxDinamico_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string itenCbx = e.AddedItems[0].ToString();
            var ultimoCampoAdd = variacoeSP.Children[variacoeSP.Children.Count - 1];

            if(itenCbx == "Outro")
            {
                CampoVariacaoTextBox(itenCbx, 180,-30, false);
                SelecionarVariacao.Visibility = Visibility.Visible;
            }
            else if(((FrameworkElement)ultimoCampoAdd).Name == "txtOutro")
            {
                variacoeSP.Children.RemoveAt(variacoeSP.Children.Count - 1);
                SelecionarVariacao.Visibility = Visibility.Visible;
            }
            else
            {
                SelecionarVariacao.Visibility = Visibility.Visible;
            }
        }

        private void SelecionarVariacao_Click(object sender, RoutedEventArgs e)
        {

            string cboBox = "";
            string txtBox = "";
            string undPermitida = "";

            for(int i=0; i < variacoeSP.Children.Count; i++)
            {
                var tipoFilho = variacoeSP.Children[i].GetType();
                
                if ((tipoFilho.Name == "ComboBox"))
                {
                    ComboBox itemSelecionado = new ComboBox();
                    itemSelecionado = (ComboBox)variacoeSP.Children[i];

                    if ((itemSelecionado.SelectionBoxItem.ToString() != "Outro") && (itemSelecionado.Name != "UnidadesPermitidas"))
                    {
                        if(itemSelecionado.SelectionBoxItem.ToString() == null)
                        {
                            MessageBox.Show("Selecione um atributo!", "Erro ao Inserir Atributo");
                            return;
                        }
                        else
                        {
                            cboBox = itemSelecionado.SelectionBoxItem.ToString();
                        }
                    } else if(itemSelecionado.Name == "UnidadesPermitidas")
                    {
                        if ((itemSelecionado.SelectionBoxItem.ToString() == null)||(itemSelecionado.SelectionBoxItem.ToString() == ""))
                        {
                            MessageBox.Show("Selecione uma unidade!", "Erro ao Inserir Atributo");
                            return;
                        }
                        else
                        {
                            undPermitida = itemSelecionado.SelectionBoxItem.ToString();
                        }
                    }
                }
                else if (tipoFilho.Name == "TextBox")
                {
                    TextBox itemSelecionado = new TextBox();
                    itemSelecionado = (TextBox)variacoeSP.Children[i];
                    if ((itemSelecionado.Text == "") || (itemSelecionado.Text == ""))
                    {
                        MessageBox.Show("Digite o atributo!", "Erro ao Inserir Atributo");
                        return;
                    }
                    else
                    {
                        txtBox = itemSelecionado.Text;
                    }

                }
            }

            if(cboBox != "")
            {
                string valor = cboBox + " " + undPermitida;
                variacaoDgML.Add(new VariacaoDataGridML(txtTipoVariacao.SelectedValue.ToString(),valor));
            } else if(txtBox != "")
            {
                string valor = txtBox + " " + undPermitida;
                variacaoDgML.Add(new VariacaoDataGridML(txtTipoVariacao.SelectedValue.ToString(), valor));
            }

            txtTipoVariacao.Items.RemoveAt(txtTipoVariacao.SelectedIndex);
            dataGridVariacoes.ItemsSource = variacaoDgML;
            variacoeSP.Children.Clear();
            SelecionarVariacao.Visibility = Visibility.Collapsed;
        }

        private void btnCarregarImagensVariacaSGE_Click(object sender, RoutedEventArgs e)
        {
            int prdSelecEcommerce = 0;
            SelecionarVariacaoSGE.Items.Clear();
            
            for(int i=0; i < variacaoDgSge.Count; i++)
            {
                if(variacaoDgSge[i].ItemSelecionadoEcommerce == true)
                {
                    SelecionarVariacaoSGE.Items.Add(variacaoDgSge[i].Variacao.ToString());
                    prdSelecEcommerce++;
                }
            }

            if(prdSelecEcommerce > 0)
            {
                btnCarregarImagensVariacaSGE.Command = DialogHost.OpenDialogCommand;
            }
            else
            {
                MessageBox.Show("Selecione pelo menos uma variação do SGE!","Selecionar Imagens por Variação");
                btnCarregarImagensVariacaSGE.Command = null;
            }
        }

        private void btnSelecionarImagem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                {
                    listaImagensAdd.Add(new ImagemUrlAddDataGridML(Path.GetFullPath(filename)));
                }
            }
            dataGridImagensAdd.ItemsSource = listaImagensAdd;
        }

        private void btnExcluirImagem_Click(object sender, RoutedEventArgs e)
        {
            ImagemUrlAddDataGridML imagemSeleciona = (ImagemUrlAddDataGridML)dataGridImagensAdd.SelectedItem;

            for (int i = 0; i < listaImagensAdd.Count; i++)
            {
                if (imagemSeleciona.URLImagem == listaImagensAdd[i].URLImagem)
                {
                    listaImagensAdd.RemoveAt(i);
                }
            }

            dataGridImagensAdd.ItemsSource = listaImagensAdd;

        }

        private void btnSalvarImagensVariacoesAdd_Click(object sender, RoutedEventArgs e)
        {

            for(int i=0; i < variacaoSgeAddImg.Count; i++)
            {
                for (int b=0; b < variacaoDgSge.Count; b++)
                {
                    if(variacaoDgSge[b].Variacao == variacaoSgeAddImg[i].Variacao)
                    {
                        tpVariacao = variacaoDgSge[b].TipoVariacao;
                    }
                }
                
                for (int a = 0; a < listaImagensAdd.Count; a++)
                {
                    listaImagensVariacoes.Add(new ImgUrlDataGridML(tpVariacao, variacaoSgeAddImg[i].Variacao, listaImagensAdd[a].URLImagem));
                }
            }

            if(listaImagensAdd.Count == 0)
            {
                btnSalvarImagensVariacoesAdd.Command = DialogHost.OpenDialogCommand;
                MessageBox.Show("Selecione ao menos 1(uma) imagem para a variação escolhida", "Erro so selecionar imagem", MessageBoxButton.OK);
            }
            else if(listaImagensAdd.Count != 0)
            {
                variacaoSgeAddImg.Clear();
                listaImagensAdd.Clear();
                btnSalvarImagensVariacoesAdd.Command = DialogHost.CloseDialogCommand;
                dataGridImagensVariacao.ItemsSource = listaImagensVariacoes;
           }
        }

        private void btnFecharImagensVariacoesAdd_Click(object sender, RoutedEventArgs e)
        {
            variacaoSgeAddImg.Clear();
            listaImagensAdd.Clear();
            btnFecharImagensVariacoesAdd.Command = DialogHost.CloseDialogCommand;
        }

        private void btnExcluirVariacaoSGE_Click(object sender, RoutedEventArgs e)
        {
            ImgUrlDataGridML imagemSeleciona = (ImgUrlDataGridML)dataGridImagensVariacao.SelectedItem;

            listaImagensVariacoes.Remove(imagemSeleciona);

            dataGridImagensVariacao.ItemsSource = listaImagensVariacoes;
        }

        public void inserirImagemBD(string idProduto, int idLoja, string idProdutoV, string idImagemEcommerce, string httpsUrlImagemEcommerce)
        {
            string sql = "INSERT INTO Tproduto_imagem(Idproduto, Idloja, Idprodutov, Idimagemecommerce, Urlimagem) " +
                                 "VALUES('" + idProduto + "','" + idLoja + "', '" + idProdutoV + "','" + idImagemEcommerce + "', '" + httpsUrlImagemEcommerce + "')";
            SqlCommand command = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            command.ExecuteNonQuery();
        }

        private void btnSelecionarVariacaoSGEImg_Click(object sender, RoutedEventArgs e)
        {
            variacaoSgeAddImg.Add(new VariacaoDataGridAddImagem(SelecionarVariacaoSGE.SelectedItem.ToString()));
            SelecionarVariacaoSGE.Items.Remove(SelecionarVariacaoSGE.SelectedItem.ToString());
            dataGridVariacaoAdd.ItemsSource = variacaoSgeAddImg;
        }

        private void btnExcluirVariacaoSGEImg_Click(object sender, RoutedEventArgs e)
        {
            VariacaoDataGridAddImagem variacaoSelecionada = (VariacaoDataGridAddImagem)dataGridVariacaoAdd.SelectedItem;

            
            if (variacaoSelecionada != null)
            {
                SelecionarVariacaoSGE.Items.Add(variacaoSelecionada.Variacao);
                variacaoSgeAddImg.Remove(variacaoSelecionada);
            }
            else
            {
                MessageBox.Show("Selecione uma Variação para remover da lista!", "Remover Variação");
            }

            dataGridVariacaoAdd.ItemsSource = variacaoSgeAddImg;
        }

        private ItemML MontarJsonItem()
        {
            ItemML produto = new ItemML();
            List<ImagemItemIdML> idImagens = new List<ImagemItemIdML>();
            List<ImagemItemML> urlImagem = new List<ImagemItemML>();
            List<ImagemItemML> urlImagemUpload = new List<ImagemItemML>();

            int quantidadeItens = 0;
            string tipoCondicao;

            String[] idCategoria = txtCategoria.Text.ToString().Split(" - ");
            String[] tipoLista = SelecionarTipoDeLista.Text.ToString().Split(" - ");
            var tempoGarantia = txtQuantidadeGarantia.Text + " " + SelecionarPeriodoGarantia.Text;

            produto.title = txtNomeProduto.Text;
            produto.category_id = idCategoria[0].ToString();
            produto.price = Convert.ToDecimal(txtPreco.Text);
            produto.currency_id = "BRL";
            produto.buying_mode = "buy_it_now";

            if (SelecionarCondicaoItem.Text == "Novo")
            {
                tipoCondicao = "new";
            }
            else if (SelecionarCondicaoItem.Text == "Usado")
            {
                tipoCondicao = "used";
            }
            else
            {
                tipoCondicao = "not_specified";
            }
            produto.condition = tipoCondicao;

            produto.listing_type_id = tipoLista[0];
            produto.description = new DescricaoItemML(txtTextoDescricao.Text.ToString());
            produto.video_id = "null"; // colocar no formulario um lugar para adicionar video

            List<TermosDeVendaML> termosDeVenda = new List<TermosDeVendaML>();
            termosDeVenda.Add(new TermosDeVendaML("WARRANTY_TYPE", SelecionarTipoDeGarantia.SelectedItem.ToString()));
            termosDeVenda.Add(new TermosDeVendaML("WARRANTY_TIME", tempoGarantia));

            produto.sale_terms = termosDeVenda;

            for (int a = 0; a < dataGridImagensVariacao.Items.Count; a++)
            {
                ImgUrlDataGridML item = (ImgUrlDataGridML)dataGridImagensVariacao.Items[a];

                var dadosVariacaoSGE = variacaoDgSge.Where(x => x.Variacao.Contains(item.VariacaoSGE.ToString()));

                for (int b = 0; b < dadosVariacaoSGE.ToList().Count; b++)
                {
                    var lista = dadosVariacaoSGE.ToList();
                    urlImagem.Add(new ImagemItemML(item.URLImagem, lista[b].IdProdutoV));
                }
            }

            var urlImagemUnicas = urlImagem.GroupBy(x => x.source).Select(y => y.First()).ToList();

            for (int a = 0; a < urlImagemUnicas.Count(); a++)
            {
                var url = urlImagem.FindAll(x => x.source == urlImagemUnicas[a].source);

                try
                {
                    var json = JsonConvert.DeserializeObject<UploadImagemML>(FuncoesRespostaML.UploadImagemML(SessaoConexao.AccessToken, @urlImagemUnicas[a].source));
                    ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                    for (int b = 0; b < json.variations.Count; b++)
                    {
                        if (json.variations[b].size == "400x400")
                        {
                            for (int c = 0; c < url.Count(); c++)
                            {
                                inserirImagemBD(idProduto, SessaoConexao.IdLoja, url[c].IdProdutoV, json.id, json.variations[b].secure_url);
                                idImagens.Add(new ImagemItemIdML(json.id, url[c].IdProdutoV));
                            }
                            urlImagemUpload.Add(new ImagemItemML(json.variations[b].secure_url, urlImagemUnicas[a].IdProdutoV));
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Favor entrar em contato com nosso Suporte!" + "\n" + "Erro: " + ex, "Erro ao enviar produto");
                }
            }

            produto.pictures = urlImagemUpload;

            List<AtributosCategoriaML> camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(idCategoria[0]);

            List<AtributoItemML> itemAtr = new List<AtributoItemML>();
            for (int a = 0; a < dataGridVariacoes.Items.Count; a++)
            {

                VariacaoDataGridML item = (VariacaoDataGridML)dataGridVariacoes.Items[a];
                String[] itemTipoAtributo = item.TipoAtributo.ToString().Split(" - ");
                var categoriaId = camposCategoria.FindAll(x => x.name.Contains(itemTipoAtributo[0]));

                itemAtr.Add(new AtributoItemML(categoriaId[0].id, item.Atributo));

            }

            produto.attributes = itemAtr;

            List<VariacaoItemML> variacaoItem = new List<VariacaoItemML>();

            for (int b = 0; b < dataGridVariacoesSGE.Items.Count; b++)
            {
                VariacaoDataGridSGE itemSGE = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[b];
                String[] tipoVaricaoSGE = itemSGE.TipoVariacao.ToString().Split(" - ");
                String[] variacaoSGE = itemSGE.Variacao.ToString().Split(" - ");


                if (itemSGE.ItemSelecionadoEcommerce == true)
                {
                    if (produtoComVariacao == true)
                    {
                        monitora.Add(new MonitoraEC(idProduto, SessaoConexao.IdLoja, Convert.ToInt32(itemSGE.IdProdutoV), itemSGE.SKU, SessaoConexao.Ecommerce, Convert.ToDecimal(txtPreco.Text), itemSGE.QuantidadeVariacao));

                        List<CombinacaoAtributosML> variacaoProduto = new List<CombinacaoAtributosML>();
                        List<AtributoItemML> attVariacao = new List<AtributoItemML>();
                        attVariacao.Add(new AtributoItemML("SELLER_SKU", itemSGE.SKU));

                        for (int c = 0; c < tipoVaricaoSGE.Count(); c++)
                        {
                            var categoriaId = camposCategoria.FindAll(x => x.name.Contains(tipoVaricaoSGE[c]));
                            if ((categoriaId[0].id != null) || (categoriaId[0].id != ""))
                            {
                                variacaoProduto.Add(new CombinacaoAtributosML(categoriaId[0].id, "", "", variacaoSGE[c]));
                            }
                        }

                        var idImagensSGE = idImagens.FindAll(x => x.IdProdutoV == itemSGE.IdProdutoV);
                        string[] idImgSge = new string[idImagensSGE.Count()];

                        for (int c = 0; c < idImagensSGE.Count(); c++)
                        {
                            idImgSge[c] = idImagensSGE[c].id;
                        }

                        quantidadeItens += Convert.ToInt32(itemSGE.QuantidadeVariacao);

                        variacaoItem.Add(new VariacaoItemML(variacaoProduto, Convert.ToDecimal(txtPreco.Text), Convert.ToInt32(itemSGE.QuantidadeVariacao), attVariacao, 0, idImgSge, 0, ""));
                    }
                }
            }

            produto.variations = variacaoItem;
            produto.available_quantity = quantidadeItens;

            return produto;

        }

        private ItemMLPai MontarJsonItemSemVariacao()
        {
            ItemMLPai produto = new ItemMLPai();
            List<ImagemItemML> urlImagem = new List<ImagemItemML>();
            List<ImagemItemML> urlImagemUpload = new List<ImagemItemML>();

            int quantidadeItens = 0;
            string tipoCondicao;

            String[] idCategoria = txtCategoria.Text.ToString().Split(" - ");
            String[] tipoLista = SelecionarTipoDeLista.Text.ToString().Split(" - ");
            var tempoGarantia = txtQuantidadeGarantia.Text + " " + SelecionarPeriodoGarantia.Text;

            produto.title = txtNomeProduto.Text;
            produto.category_id = idCategoria[0].ToString();
            produto.price = Convert.ToDecimal(txtPreco.Text);
            produto.currency_id = "BRL";
            produto.buying_mode = "buy_it_now";

            if (SelecionarCondicaoItem.Text == "Novo")
            {
                tipoCondicao = "new";
            }
            else if (SelecionarCondicaoItem.Text == "Usado")
            {
                tipoCondicao = "used";
            }
            else
            {
                tipoCondicao = "not_specified";
            }
            produto.condition = tipoCondicao;

            produto.listing_type_id = tipoLista[0];
            produto.description = new DescricaoItemML(txtTextoDescricao.Text.ToString());
            produto.video_id = "null"; // colocar no formulario um lugar para adicionar video

            List<TermosDeVendaML> termosDeVenda = new List<TermosDeVendaML>();
            termosDeVenda.Add(new TermosDeVendaML("WARRANTY_TYPE", SelecionarTipoDeGarantia.SelectedItem.ToString()));
            termosDeVenda.Add(new TermosDeVendaML("WARRANTY_TIME", tempoGarantia));

            produto.sale_terms = termosDeVenda;

            for (int a = 0; a < dataGridImagensVariacao.Items.Count; a++)
            {
                ImgUrlDataGridML item = (ImgUrlDataGridML)dataGridImagensVariacao.Items[a];

                var dadosVariacaoSGE = variacaoDgSge.Where(x => x.Variacao.Contains(item.VariacaoSGE.ToString()));

                for (int b = 0; b < dadosVariacaoSGE.ToList().Count; b++)
                {
                    var lista = dadosVariacaoSGE.ToList();
                    urlImagem.Add(new ImagemItemML(item.URLImagem, lista[b].IdProdutoV));
                }
            }

            var urlImagemUnicas = urlImagem.GroupBy(x => x.source).Select(y => y.First()).ToList();

            for (int a = 0; a < urlImagemUnicas.Count(); a++)
            {
                var url = urlImagem.FindAll(x => x.source == urlImagemUnicas[a].source);

                try
                {
                    var json = JsonConvert.DeserializeObject<UploadImagemML>(FuncoesRespostaML.UploadImagemML(SessaoConexao.AccessToken, @urlImagemUnicas[a].source));
                    ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                    for (int b = 0; b < json.variations.Count; b++)
                    {
                        if (json.variations[b].size == "400x400")
                        {
                            for (int c = 0; c < url.Count(); c++)
                            {
                                inserirImagemBD(idProduto, SessaoConexao.IdLoja, url[c].IdProdutoV, json.id, json.variations[b].secure_url);
                            }
                            urlImagemUpload.Add(new ImagemItemML(json.variations[b].secure_url, urlImagemUnicas[a].IdProdutoV));
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Favor entrar em contato com nosso Suporte!" + "\n" + "Erro: " + ex, "Erro ao enviar produto");
                }
            }

            produto.pictures = urlImagemUpload;

            List<AtributosCategoriaML> camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(idCategoria[0]);

            List<AtributoItemML> itemAtr = new List<AtributoItemML>();
            for (int a = 0; a < dataGridVariacoes.Items.Count; a++)
            {

                VariacaoDataGridML item = (VariacaoDataGridML)dataGridVariacoes.Items[a];
                String[] itemTipoAtributo = item.TipoAtributo.ToString().Split(" - ");
                var categoriaId = camposCategoria.FindAll(x => x.name.Contains(itemTipoAtributo[0]));

                itemAtr.Add(new AtributoItemML(categoriaId[0].id, item.Atributo));
            }

            for (int b = 0; b < dataGridVariacoesSGE.Items.Count; b++)
            {
                VariacaoDataGridSGE itemSGE = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[b];

                if (itemSGE.ItemSelecionadoEcommerce == true)
                {
                    if (produtoComVariacao == false)
                    {
                        monitora.Add(new MonitoraEC(idProduto, SessaoConexao.IdLoja, Convert.ToInt32(itemSGE.IdProdutoV), itemSGE.SKU, SessaoConexao.Ecommerce, Convert.ToDecimal(txtPreco.Text), itemSGE.QuantidadeVariacao));
                        itemAtr.Add(new AtributoItemML("SELLER_SKU", itemSGE.SKU));
                        quantidadeItens += Convert.ToInt32(itemSGE.QuantidadeVariacao);
                    }
                }
            }

            produto.attributes = itemAtr;
            produto.available_quantity = quantidadeItens;

            return produto;
        }

        private void btnSalvarProdutoBdJson_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string jsonItemMLEnvio = "";

                if (produtoComVariacao == false)
                {
                    jsonItemMLEnvio = JsonConvert.SerializeObject(MontarJsonItemSemVariacao(),Formatting.Indented);
                } 
                else if(produtoComVariacao == true)
                {
                    jsonItemMLEnvio = JsonConvert.SerializeObject(MontarJsonItem(), Formatting.Indented);

                }

                string logEnvioProduto = "Produto Enviado às "+ DateTime.Now + "\n\n" +"Dados enviados: "+"\n\n"+ jsonItemMLEnvio +"\n\n"+ "----------------------------\n" + "Enviado por "+SessaoConexao.UsuarioLogadoSGE+ " às " + DateTime.Now;
                Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\produtoEcommerce\"+idProduto,"EnvioProduto-"+Util.NormalizaHora(DateTime.Now)+ ".txt", logEnvioProduto);

                ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);

                if (reenviarPrd == false)
                {
                    string sql = "INSERT INTO Tprodutoec(Idproduto, Idloja, Ecommerce, Titulo_ec, Descricao_ec, IdProduto_ec, Json_ec_envio, Json_ec_retorno)" +
                                      "VALUES('" + idProduto + "'," + SessaoConexao.IdLoja + ",'" + SessaoConexao.Ecommerce + "','" + txtNomeProduto.Text + "','" + txtTextoDescricao.Text.ToString() + "'," +
                                      "'0' , '" + jsonItemMLEnvio + "', '0')";
                    SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                    cmd.ExecuteNonQuery();
                } 
                else if(reenviarPrd == true)
                {
                    string sql = "UPDATE Tprodutoec SET Titulo_ec = '"+txtNomeProduto.Text + "', Descricao_ec = '" + txtTextoDescricao.Text.ToString() + "', Json_ec_envio = '" + jsonItemMLEnvio + "' WHERE Idproduto = '" + idProduto + "'";
                    SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                    cmd.ExecuteNonQuery();

                    string sql2 = "DELETE FROM Tproduto_monitora_ec WHERE Idproduto = '" + idProduto + "'";
                    SqlCommand cmd2 = new SqlCommand(sql2, ConexaoBancoDados.Conexao);
                    cmd2.ExecuteNonQuery();
                }

                var jsonItemMLResposta = FuncoesRespostaML.EnviarProdutoML(SessaoConexao.AccessToken, jsonItemMLEnvio);
                var retornoItem = JsonConvert.DeserializeObject<ItemMLRetorno>(jsonItemMLResposta);

                string logRespostaProduto = "Resposta do Ecommercer Recebida às " + DateTime.Now + "\n\n" + "Dados recebidos: " + "\n\n" + jsonItemMLResposta + "\n\n" + "----------------------------\n" + "Recebido às " + DateTime.Now;
                Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\produtoEcommerce\" + idProduto, "RetornoProduto-" + Util.NormalizaHora(DateTime.Now) + ".txt", logRespostaProduto);

                string sqlUpdate = "UPDATE Tprodutoec SET Idproduto_ec = '" + retornoItem.id + "', Json_ec_retorno = '" + jsonItemMLResposta + "' WHERE Idproduto = '" + idProduto +"'";
                SqlCommand cmdupdate = new SqlCommand(sqlUpdate, ConexaoBancoDados.Conexao);
                cmdupdate.ExecuteNonQuery();

                if(produtoComVariacao == true)
                {
                    for (int a = 0; a < monitora.Count; a++)
                    {
                        for (int b = 0; b < retornoItem.variations.Count; b++)
                        {
                            if (retornoItem.variations[b].attributes[0].value_name == monitora[a].Sku)
                            {
                                string sqlm = "INSERT INTO Tproduto_monitora_ec(Idproduto, Idloja, Idprodutov, Sku, Ecommerce, Idproduto_ec, Idprodutov_ec, Vlr_venda_atual_ec, Qde_atual_ec, Status_produto_ec)" +
                                      "VALUES('" + monitora[a].IdProduto + "'," + monitora[a].IdLoja + "," + monitora[a].IdProdutoV + ",'" + monitora[a].Sku + "'," +
                                      "'" + monitora[a].Ecommerce + "','" + retornoItem.id + "'," + retornoItem.variations[b].id + "," + Util.ConverteValorSQL(monitora[a].ValorVendaAtualEc.ToString()) + "," + Util.ConverteValorSQL(monitora[a].QuantidadeAtualEc.ToString()) + ", 0)";
                                SqlCommand cmdm = new SqlCommand(sqlm, ConexaoBancoDados.Conexao);
                                cmdm.ExecuteNonQuery();
                            }
                        }
                    }
                }
                else if(produtoComVariacao == false)
                {
                    for (int a = 0; a < monitora.Count; a++)
                    {
                        string sqlm = "INSERT INTO Tproduto_monitora_ec(Idproduto, Idloja, Idprodutov, Sku, Ecommerce, Idproduto_ec, Idprodutov_ec, Vlr_venda_atual_ec, Qde_atual_ec, Status_produto_ec)" +
                                      "VALUES('" + monitora[a].IdProduto + "'," + monitora[a].IdLoja + "," + monitora[a].IdProdutoV + ",'" + monitora[a].Sku + "'," +
                                      "'" + monitora[a].Ecommerce + "','" + retornoItem.id + "'," + 0 + "," + Util.ConverteValorSQL(monitora[a].ValorVendaAtualEc.ToString()) + "," + Util.ConverteValorSQL(monitora[a].QuantidadeAtualEc.ToString()) + ", 0)";
                        SqlCommand cmdm = new SqlCommand(sqlm, ConexaoBancoDados.Conexao);
                        cmdm.ExecuteNonQuery();
                    }
                }
                MessageBox.Show("Produto publicado no Ecommerce com Sucesso!" + "\n", "Produto Ecommerce", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex, "Erro");
            }
        }

        private void btnSalvarProdutoBdJson_Click_1(object sender, RoutedEventArgs e)
        {
            int numErro = 0;

            if ((txtNomeProduto.Text == "") || (txtNomeProduto.Text == null))
            {
                MessageBox.Show("Campo Nome do Produto não pode ser vazio!", "Erro ao Enviar");
                HintAssist.SetHint(txtNomeProduto, "Campo Nome do Produto não pode ser vazio");
                txtNomeProduto.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if ((txtCategoria.Text == "") || (txtCategoria.Text == null))
            {
                MessageBox.Show("Selecione uma Categoria!", "Erro ao Enviar");
                HintAssist.SetHint(txtCategoria, "Selecione uma Categoria");
                txtCategoria.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }
            
            if (SelecionarCondicaoItem.SelectedItem == null)
            {
                MessageBox.Show("Selecione uma Condição!", "Erro ao Enviar");
                HintAssist.SetHint(SelecionarCondicaoItem, "Selecione uma Condição");
                SelecionarCondicaoItem.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if ((txtTextoDescricao.Text == "") || (txtTextoDescricao.Text == null))
            {
                MessageBox.Show("Digite a descrição do Produto!", "Erro ao Enviar");
                HintAssist.SetHint(txtTextoDescricao, "Digite a descrição do Produto");
                txtTextoDescricao.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if(SelecionarTipoDeGarantia.SelectedItem == null)
            {
                MessageBox.Show("Selecione um Tipo de Garantia!", "Erro ao Enviar");
                HintAssist.SetHint(SelecionarTipoDeGarantia, "Selecione um Tipo de Garantia");
                SelecionarTipoDeGarantia.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if ((txtQuantidadeGarantia.Text == "") || (txtQuantidadeGarantia.Text == null))
            {
                MessageBox.Show("Digite o quantidade unitária da Garantia!", "Erro ao Enviar");
                HintAssist.SetHint(txtQuantidadeGarantia, "Digite o quantidade unitária da Garantia");
                txtQuantidadeGarantia.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if (SelecionarPeriodoGarantia.SelectedItem == null)
            {
                MessageBox.Show("Selecione um Periodo da Garantia!", "Erro ao Enviar");
                HintAssist.SetHint(SelecionarPeriodoGarantia, "Selecione um Periodo da Garantia");
                SelecionarPeriodoGarantia.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if (SelecionarTipoDeLista.SelectedItem == null)
            {
                MessageBox.Show("Selecione Tipo de Lista do Ecommerce que será enviado o Produto!", "Erro ao Enviar");
                HintAssist.SetHint(SelecionarTipoDeLista, "Selecione Tipo de Lista do Ecommerce");
                SelecionarTipoDeLista.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            int numVariObrigatorio = 0;
            for (int a = 0; a < txtTipoVariacao.Items.Count; a++)
            {
                String[] variObrigatorio = txtTipoVariacao.Items[a].ToString().Split(" - ");
                if (variObrigatorio[1] == "Obrigatorio")
                {
                    numVariObrigatorio++;
                }
            }

            if(numVariObrigatorio != 0)
            {
                MessageBox.Show("Selecione todos os atributos obrigatórios!", "Erro ao Enviar");
                txtTipoVariacao.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            int numVariSGE = 0;
            for (int i = 0; i < dataGridVariacoesSGE.Items.Count; i++)
            {
                VariacaoDataGridSGE item = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[i];
                if(item.ItemSelecionadoEcommerce == true)
                {
                    numVariSGE++;
                    if(item.QuantidadeVariacao == 0)
                    {
                        MessageBox.Show("ATENÇÃO!!! " + "\n\n" +
                                "Você está enviando 0(Zero) unidades deste produto para o Ecommerce!" + "\n" +
                                "Antes de Enviar entre no SGE e adicione no minímo 1(Uma) unidade " + "\n" +
                                "na reserva de estoque do Ecommerce!" + "\n" +
                                "Qualquer dúvida entre em contato com o nosso suporte!"
                                , "Erro ao Enviar");

                        dataGridVariacoesSGE.Focus();

                        btnSalvarProdutoBdJson.Command = null;
                        numErro++;
                        return;
                    }
                }
            }

            if(numVariSGE == 0)
            {
                MessageBox.Show("Selecione algum Tipo de Variação do SGE (mesmo que esteja escrito 'Sem Variação' !", "Erro ao Enviar");
                dataGridVariacoesSGE.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            if(dataGridImagensVariacao.Items.Count == 0)
            {
                MessageBox.Show("Atribuia pelo menos uma imagem a uma variação!", "Erro ao Enviar");
                dataGridImagensVariacao.Focus();

                btnSalvarProdutoBdJson.Command = null;
                numErro++;
                return;
            }

            for (int i = 0; i < dataGridVariacoesSGE.Items.Count; i++)
            {
                int variacaoSemImagem = 0;
                VariacaoDataGridSGE item = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[i];
                if (item.ItemSelecionadoEcommerce == true)
                {
                    for (int a = 0; a < dataGridImagensVariacao.Items.Count; a++)
                    {
                        ImgUrlDataGridML tvImagem = (ImgUrlDataGridML)dataGridImagensVariacao.Items[a];
                        if(item.Variacao == tvImagem.VariacaoSGE)
                        {
                            variacaoSemImagem++;
                        }
                    }
                    if(variacaoSemImagem == 0)
                    {
                        MessageBox.Show("Atribuia pelo menos uma imagem a variação " + item.Variacao, "Erro ao Enviar");
                        dataGridImagensVariacao.Focus();

                        btnSalvarProdutoBdJson.Command = null;
                        numErro++;
                        return;
                    }
                }
            }


            if (numErro == 0)
            {
                btnSalvarProdutoBdJson.Command = DialogHost.OpenDialogCommand;
                MensagemConfirmacaoDadosProduto();
            }
        }

        private void MensagemConfirmacaoDadosProduto()
        {
            msgSalvarJson.Children.Clear();

            iconMsgSalvar.Kind = PackIconKind.CheckBold;
            txtMsgSalvar.Text = "Confirme Antes de Enviar";

            PackIcon iconNomePdr = CriarCamposView.CriarIcone(PackIconKind.TextBoxMultiple, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconNomePdr);

            TextBlock nomePdr = CriarCamposView.CriarTextBlock("Produto: " + txtNomeProduto.Text, 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(nomePdr);

            PackIcon iconCtg = CriarCamposView.CriarIcone(PackIconKind.TextBoxMultiple, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconCtg);

            TextBlock ctg = CriarCamposView.CriarTextBlock("Categoria: " + txtCategoria.Text.ToString(), 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(ctg);

            PackIcon iconPreco = CriarCamposView.CriarIcone(PackIconKind.Money, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconPreco);

            TextBlock preco = CriarCamposView.CriarTextBlock("Preço: " + Convert.ToDecimal(txtPreco.Text), 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(preco);

            PackIcon iconLista = CriarCamposView.CriarIcone(PackIconKind.TextBoxMultiple, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconLista);

            TextBlock lista = CriarCamposView.CriarTextBlock("Tipo de Lista: " + SelecionarTipoDeLista.Text.ToString(), 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(lista);

            PackIcon iconGarantia = CriarCamposView.CriarIcone(PackIconKind.TextBoxMultiple, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconGarantia);

            TextBlock garantia = CriarCamposView.CriarTextBlock("Garantia Oferecida por: " + SelecionarTipoDeGarantia.SelectedItem.ToString(), 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(garantia);

            PackIcon iconGarantiaT = CriarCamposView.CriarIcone(PackIconKind.Calendar, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconGarantiaT);

            TextBlock garantiaT = CriarCamposView.CriarTextBlock("Tempo de Garantia: " + txtQuantidadeGarantia.Text + " " + SelecionarPeriodoGarantia.Text, 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(garantiaT);

            PackIcon iconVariacao = CriarCamposView.CriarIcone(PackIconKind.ViewList, HorizontalAlignment.Left, 20, 20, 0, 0, 0, 0);
            msgSalvarJson.Children.Add(iconVariacao);

            TextBlock variacao = CriarCamposView.CriarTextBlock("Atributos", 30, HorizontalAlignment.Left, 35, -20, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(variacao);

            for (int a = 0; a < dataGridVariacoes.Items.Count; a++)
            {
                var item = (VariacaoDataGridML)dataGridVariacoes.Items[a];
                String[] v1 = item.TipoAtributo.ToString().Split(" - ");

                PackIcon iconVari = CriarCamposView.CriarIcone(PackIconKind.ArrowRight, HorizontalAlignment.Left, 15, 15, 0, 0, 0, 0);
                msgSalvarJson.Children.Add(iconVari);

                TextBlock vari = CriarCamposView.CriarTextBlock(v1[0].ToString() + " - " + item.Atributo.ToString(), 20, HorizontalAlignment.Left, 45, -17, 0, 0, Brushes.Black, Brushes.Transparent);
                msgSalvarJson.Children.Add(vari);
            }

            int quantidade = 0;

            PackIcon iconVar = CriarCamposView.CriarIcone(PackIconKind.ViewList, HorizontalAlignment.Left, 20, 20, 0, 10, 0, 0);
            msgSalvarJson.Children.Add(iconVar);

            TextBlock varc = CriarCamposView.CriarTextBlock("Variacações", 30, HorizontalAlignment.Left, 35, -20, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(varc);

            for (int a = 0; a < dataGridVariacoesSGE.Items.Count; a++)
            {
                var item = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[a];
                if (item.ItemSelecionadoEcommerce == true)
                {
                    PackIcon iconQtdade = CriarCamposView.CriarIcone(PackIconKind.ArrowRight, HorizontalAlignment.Left, 15, 15, 0, 0, 0, 0);
                    msgSalvarJson.Children.Add(iconQtdade);

                    TextBlock qtdade = CriarCamposView.CriarTextBlock(item.TipoVariacao + " - " + item.Variacao + " / Quantidade : " + Convert.ToInt32(item.QuantidadeVariacao), 20, HorizontalAlignment.Left, 45, -17, 0, 0, Brushes.Black, Brushes.Transparent);
                    msgSalvarJson.Children.Add(qtdade);
                    quantidade += Convert.ToInt32(item.QuantidadeVariacao);
                }
            }


            PackIcon iconQuantidade = CriarCamposView.CriarIcone(PackIconKind.HighQuality, HorizontalAlignment.Left, 20, 20, 0, 20, 0, 0);
            msgSalvarJson.Children.Add(iconQuantidade);

            TextBlock qtd = CriarCamposView.CriarTextBlock("Quantidade Total: " + quantidade, 30, HorizontalAlignment.Left, 35, -22, 0, 0, Brushes.Black, Brushes.Transparent);
            msgSalvarJson.Children.Add(qtd);
            
        }

        private void txtQuantidadeGarantia_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }

        private void TxtOutro_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }

        private void CarregarValoresParaReenviarProduto()
        {
            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
            var sql = "SELECT Idproduto, Titulo_ec, Descricao_ec, Idproduto_ec, Json_ec_envio FROM Tprodutoec WHERE Idproduto = '" + idProduto + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            txtNomeProduto.Text = reader[1].ToString();
            txtTextoDescricao.Text = reader[2].ToString();
            idProdutoEcommerce = reader[3].ToString();
            var json = JsonConvert.DeserializeObject<ItemML>(reader[4].ToString());
            reader.Close();

            var nomeCategoria = FuncoesRespostaML.buscarCategoriaFilhoML(json.category_id);
            CamposCategoria(json.category_id);

            txtCategoria.Text = json.category_id + " - " + nomeCategoria.name;

            if (json.condition == "new")
            {
                SelecionarCondicaoItem.SelectedItem = "Novo";
            }
            else if (SelecionarCondicaoItem.Text == "used")
            {
                SelecionarCondicaoItem.SelectedItem = "Usado";
            }
            else
            {
                SelecionarCondicaoItem.SelectedItem = "Não especificado";
            }

            if (json.listing_type_id == "gold_special")
            {
                SelecionarTipoDeLista.SelectedItem = "gold_special - Clásica";
            }
            else if (json.listing_type_id == "gold_pro")
            {
                SelecionarTipoDeLista.SelectedItem = "gold_pro - Premium";
            }

            for (int a = 0; a < json.sale_terms.Count; a++)
            {
                if (json.sale_terms[a].id == "WARRANTY_TYPE")
                {
                    SelecionarTipoDeGarantia.SelectedItem = json.sale_terms[a].value_name;
                }
                else if (json.sale_terms[a].id == "WARRANTY_TIME")
                {
                    String[] tempo = json.sale_terms[a].value_name.Split(" ");
                    txtQuantidadeGarantia.Text = tempo[0];
                    SelecionarPeriodoGarantia.SelectedItem = tempo[1];
                }
            }

            List<AtributosCategoriaML> camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(json.category_id);

            for (int a = 0; a < json.attributes.Count; a++)
            {

                var categoriaNome = camposCategoria.FindAll(x => x.id.Contains(json.attributes[a].id));

                for (int b = 0; b < txtTipoVariacao.Items.Count; b++)
                {
                    String[] variacao = txtTipoVariacao.Items[b].ToString().Split(" - ");
                    if (variacao[0] == categoriaNome[0].name)
                    {
                        txtTipoVariacao.Items.RemoveAt(b);
                        variacaoDgML.Add(new VariacaoDataGridML(variacao[0] + " - " + variacao[1], json.attributes[a].value_name));
                    }
                }
            }

            dataGridVariacoes.ItemsSource = variacaoDgML;
        }

    }
}
