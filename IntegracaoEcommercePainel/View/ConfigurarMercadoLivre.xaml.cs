﻿using System;
using System.Windows;
using System.Diagnostics;
using System.Windows.Navigation;
using IntegracaoEcommercePainel;
using Newtonsoft.Json;
using RestSharp;
using IntegracaoPainel.ModelML.Credencial;
using IntegracaoEcommercePainel.Util;
using IntegracaoEcommercePainel.FuncoesML;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Media;
using System.IO;
using System.Collections.Generic;
using IntegracaoPainel.Model.VerificaSGE.VerificaSGEBD;
using System.Linq;

namespace IntegracaoPainel
{
    public partial class ConfigurarMercadoLivre : Window
    {
        public string diretorioCredencialJson = @"C:\painelinformatica\ecommerce\credenciais";
        public string nomeArquivoCredencialML = "credencialML.json";
        public string conexaoBD = "conexaoBD.json";

        public string idAplica;
        public string skAplica;
        public string nicknameUsuario;
        public string emailUsuario;
        public string accessTokenML;
        public string refreshTokenML;
        public int    usuarioId;
        public double expiresIn;
        public string valorVendaEcommerce;
        public int idPessoal;
        public int idOperacao;

        public string ipServidorBD;
        public string nomeBD;
        

        public ConfigurarMercadoLivre(string ipServidor, string banco)
        {
            ipServidorBD = ipServidor;
            nomeBD = banco;
  
            InitializeComponent();
            CarregaComboBoxValorProdutoSGE();
            CarregarPessoal(ipServidor, banco);
            CarregarOperacao(ipServidor, banco);

            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void MoverBox(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnFechar(object sender, RoutedEventArgs e)
        {
            this.Close();
            Console.WriteLine("click fechar");
        }
        public new(string, string, string, string, string, string, int, double, string, int, int) ShowDialog()
        {
            base.ShowDialog();

            return (idAplica, skAplica, nicknameUsuario, emailUsuario, accessTokenML, refreshTokenML, usuarioId, expiresIn, valorVendaEcommerce, idPessoal, idOperacao);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true });
        }

        private void BtnNavegador(object sender, RoutedEventArgs e)
        {
            if (((IDAplicacaoML.Text == null) || (IDAplicacaoML.Text == "")) && ((SKAplicacaoML.Text == null) || (SKAplicacaoML.Text == "")))
            {
                /*MsgIDConfigML.Visibility = Visibility.Visible;
                MsgSKConfigML.Visibility = Visibility.Visible;*/
            }
            else if ((IDAplicacaoML.Text == null) || (IDAplicacaoML.Text == ""))
            {
               /* MsgIDConfigML.Visibility = Visibility.Visible;*/

            }
            else if ((SKAplicacaoML.Text == null) || (SKAplicacaoML.Text == "") || (SKAplicacaoML.Text.Length > 32))
            {
                /*MsgIDConfigML.Visibility = Visibility.Collapsed;
                MsgSKConfigML.Visibility = Visibility.Visible;*/
            }
            else
            {
                /*MsgIDConfigML.Visibility = Visibility.Collapsed;
                MsgSKConfigML.Visibility = Visibility.Collapsed;*/

                Navegador navegador = new Navegador(IDAplicacaoML.Text);

                string code = navegador.ShowDialog();

                string codeML = code;

                if ((codeML != null) && (codeML != ""))
                {
                    var msgML = TokenAcessML(codeML, IDAplicacaoML.Text, SKAplicacaoML.Text);
                    StatusAutenticarML.Content = msgML;

                }
                else
                {
                    /*MsgIDConfigML.Visibility = Visibility.Visible;*/
                }

            }
        }

        public String TokenAcessML(string codeML, string idAplicacao, string skAplicacao)
        {
            var codeGerado = codeML;
            var idApp = idAplicacao;
            var skApp = skAplicacao;
            ConexaoBancoDados.GetBanco(ipServidorBD);
            ConexaoBancoDados.GetConexao(ipServidorBD, nomeBD);

            var acessToken = new RestClient("https://api.mercadolibre.com/oauth/token?grant_type=authorization_code&client_id=" + idApp + "&client_secret=" + skApp + "&code=" + codeGerado + "&redirect_uri=https://onvio.painelinformatica.com.br");
            acessToken.Timeout = -1;

            var request = new RestRequest(Method.POST);
            IRestResponse response = acessToken.Execute(request);

            try
            {
                TokenML token = JsonConvert.DeserializeObject<TokenML>(response.Content);

                if (token.status == 400)
                {
                    return "Erro ao gerar o Token de Acesso";
                }
                else
                {
                    var validaAcesso = FuncoesRespostaML.verificarAcessTokenValido(token.access_token, token.user_id, ipServidorBD, nomeBD);

                    if (validaAcesso == false)
                    {
                        return "Erro ao gerar o Token de Acesso";
                    }
                    else
                    {
                        if (nomeBD.ToString().Substring(0, 5).ToUpper() == "DBSGE")
                        {
                            string sqlquery2 = "SELECT COUNT(*) FROM Tbtoken";
                            SqlCommand cmd3 = new SqlCommand(sqlquery2, ConexaoBancoDados.Conexao);
                            cmd3.CommandType = CommandType.Text;
                            SqlDataReader reader3;
                            reader3 = cmd3.ExecuteReader();
                            reader3.Read();
                            string vTk = reader3[0].ToString();
                            reader3.Close();
                            if (vTk != "0")
                            {

                                string sql4 = "TRUNCATE TABLE Tbtoken";
                                SqlCommand cmd4 = new SqlCommand(sql4, ConexaoBancoDados.Conexao);
                                cmd4.ExecuteNonQuery();
                            }
                            String query = "INSERT INTO Tbtoken (Accesstoken,Tokentype, Expiresin, Scope, Userid, Refreshtoken) VALUES ('" + token.access_token + "','" + token.token_type + "' , " + token.expires_in + ",'" + token.scope + "','" + token.user_id + "', '" + token.refresh_token + "')";
                            SqlCommand command = new SqlCommand(query, ConexaoBancoDados.Conexao);
                            command.ExecuteNonQuery();

                            string sqlquery = "SELECT Idusuario, Nickname, Email FROM Tbdadosusuarioecommerceml";
                            SqlCommand cmd2 = new SqlCommand(sqlquery, ConexaoBancoDados.Conexao);
                            cmd2.CommandType = CommandType.Text;
                            SqlDataReader reader2;
                            reader2 = cmd2.ExecuteReader();
                            reader2.Read();
                            nicknameUsuario = reader2["Nickname"].ToString();
                            emailUsuario = reader2["Email"].ToString();
                            reader2.Close();
                            usuarioId = token.user_id;
                            idAplica = idApp;
                            skAplica = skApp;
                            accessTokenML = token.access_token;
                            refreshTokenML = token.refresh_token;
                            expiresIn = token.expires_in;
                            valorVendaEcommerce = cbxPreco.Text;

                            String[] pessoal = cbxPessoal.Text.Split(" - ");
                            String[] operacao = cbxOperacao.Text.Split(" - ");
                            idPessoal = Convert.ToInt32(pessoal[0]);
                            idOperacao = Convert.ToInt32(operacao[0]);

                            AutenticarML.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                            AutenticarML.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString("#49b675");
                            SalvarAutenticacao.Visibility = Visibility.Visible;

                            /*var listaStatus = RetornaListaStatusMercadoLivre();
                            List<StatusPedidoVenda> cdtStatusBd = new List<StatusPedidoVenda>();
                            string queryStatus = "SELECT * FROM Tstatus_ec WHERE Ecommerce = 'Mercado Livre'";
                            SqlCommand cmdStatus = new SqlCommand(queryStatus, ConexaoBancoDados.Conexao);
                            cmdStatus.CommandType = CommandType.Text;
                            SqlDataReader readerStatus;
                            readerStatus = cmdStatus.ExecuteReader();
                            if (readerStatus.HasRows)
                            {
                                while (readerStatus.Read())
                                {
                                    cdtStatusBd.Add(new StatusPedidoVenda(Convert.ToInt32(readerStatus[""])));
                                }
                            }
                            
                            readerStatus.Close();*/


                            return "Autorizado";
                        }
                        else
                        {
                            MessageBox.Show("Não foi possivel salvar no Banco entre em contato com o Suporte!");
                            return "Erro";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro: " + response.StatusDescription);
                string erroATML = "Erro: " + response.StatusCode.ToString();

                return erroATML;
            }
        }

        public void CarregaComboBoxValorProdutoSGE()
        {
            cbxPreco.Items.Add("Vlr_venda1");
            cbxPreco.Items.Add("Vlr_venda2");
            cbxPreco.Items.Add("Vlr_venda3");
            cbxPreco.Items.Add("Vlr_venda4");
            cbxPreco.Items.Add("Vlr_venda5");
            cbxPreco.Items.Add("Vlr_venda6");
        }

        public void CarregarPessoal(string ipServidor, string banco)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Idpessoal, Nome FROM Tpessoal";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    cbxPessoal.Items.Add(reader["Idpessoal"].ToString() + " - " + reader["Nome"].ToString());
                }
                reader.Close();
            }
        }

        public void CarregarOperacao(string ipServidor, string banco)
        {
            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sql = "SELECT Idoper, Operacao FROM Toper WHERE Tp_oper = 0";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    cbxOperacao.Items.Add(reader["Idoper"].ToString() + " - " + reader["Operacao"].ToString());
                }
                reader.Close();
            }
        }

        public List<StatusPedidoVenda> RetornaListaStatusMercadoLivre()
        {
            List<StatusPedidoVenda> status = new List<StatusPedidoVenda>();
            status.Add(new StatusPedidoVenda(0,"Mercado Livre", "Pendente", "pending"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Em manejo", "handling"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Pronto para Envio", "ready_to_ship"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Enviado", "shipped"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Entregue", "delivered"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Não entregue", "not_delivered"));
            status.Add(new StatusPedidoVenda(0, "Mercado Livre", "Cancelado", "cancelled"));
            return status;
        }
    }
}
