﻿ using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.FuncoesML;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.Callback;
using IntegracaoPainel.ModelML.Produtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using IntegracaoEcommercePainel.Controller.Produtos;
using IntegracaoPainel.RetornoItemML;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.ModelML.EnvioItemML;
using IntegracaoPainel.ModelML.RetornoItemML;
using IntegracaoEcommercePainel.View;

namespace IntegracaoPainel.View
{

    public partial class Produtos : Window
    {
        /*ObservableCollection<ListaProduto> produtosSGE = new ObservableCollection<ListaProduto>();*/
        List<ListaProduto> produtosSGE = new List<ListaProduto>();
        List<ProdutoImportEcommerce> produtoEC = new List<ProdutoImportEcommerce>();
        public int pageIndex = 1;
        private int numeroPaginas = 30;
        private enum PagingMode { Next = 1, Previous = 2};

        public Produtos()
        {
            InitializeComponent();
            btnAnteriorProdutoDG.IsEnabled = false;
            btnProximoProdutoDG.IsEnabled = false;
            dpNumeroProdutos.Visibility = Visibility.Collapsed;
            CarregaDataGridProdutosEcommerce();
            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void btnTodosProdutosSGE_Click(object sender, RoutedEventArgs e)
        {
            /*produtosSGE.Clear();
            string sql = "Select Tpi.Idproduto,Tpec.Idproduto_ec,Tpi.Descricao,Tpec.Titulo_ec,Tp." + SessaoConexao.ValorSGE + " as ValorSGE " +
                         "from Tprodutoi as Tpi " +
                         "FULL OUTER JOIN Tprodutoec as Tpec on Tpec.Idproduto = Tpi.Idproduto " +
                         "FULL OUTER JOIN Tproduto as Tp on Tp.Idproduto = Tpi.Idproduto ";
            produtosSGE = CarregarProdutosEcommerce(sql);
            CarregaDataGridListaProdutos();*/

            /*var teste = FuncoesCallbackML.BuscasCallbackRecebidosBdPainel();

            var callback = JsonConvert.DeserializeObject<List<CallbackBDPainel>>(teste);

            for (int i = 0; i < callback.Count; i++)
            {
                var retorno = FuncoesCallbackML.ConsultarCallbackEcommerce(callback[i].resource, SessaoConexao.AccessToken);
                Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\credenciais", i + "teste" + callback[i].topic.ToString() + ".json", retorno);
            }*/
        }

        private void btnProdutosEcommerceSGE_Click(object sender, RoutedEventArgs e)
        {
            dataGridListaDeProdutos.Visibility = Visibility.Visible;
            CarregaDataGridProdutosEcommerce();
        }

        private void CarregaDataGridProdutosEcommerce()
        {
            produtosSGE.Clear();
            string sql = "Select Tpi.Idproduto,Tpec.Idproduto_ec,Tpi.Descricao,Tpec.Titulo_ec,Tp." + SessaoConexao.ValorSGE + " as ValorSGE " +
                         "from Tprodutoi as Tpi " +
                         "FULL OUTER JOIN Tprodutoec as Tpec on Tpec.Idproduto = Tpi.Idproduto " +
                         "FULL OUTER JOIN Tproduto as Tp on Tp.Idproduto = Tpi.Idproduto " +
                         "WHERE Tpi.E_commerce = 1 and Tp.Idloja = " + SessaoConexao.IdLoja;

            produtosSGE = CarregarProdutosEcommerce(sql);
            dgListaProdutosEcommerceSGE.Visibility = Visibility.Visible;
            dgListaProdutosEc.Visibility = Visibility.Collapsed;
            CarregaDataGridListaProdutos();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true });
        }

        private void btnAnteriorProdutoDG_Click(object sender, RoutedEventArgs e)
        {
            Navigate((int)PagingMode.Previous);
        }

        private void btnProximoProdutoDG_Click(object sender, RoutedEventArgs e)
        {
            Navigate((int)PagingMode.Next);
        }

        public void CarregaDataGridListaProdutos()
        {
            dataGridListaDeProdutos.ItemsSource = produtosSGE.Take(numeroPaginas);
            int count = produtosSGE.Take(numeroPaginas).Count();
            lbNumeroProdutosInicial.Content = count;
            lbNumeroProdutosFinal.Content = produtosSGE.Count();
            if (produtosSGE.Count() > 30)
            {
                btnProximoProdutoDG.IsEnabled = true;
            }
            dpNumeroProdutos.Visibility = Visibility.Visible;
        }

        public List<ListaProduto> CarregarProdutosEcommerce(string sqlConsulta)
        {
            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
            SqlCommand cmd = new SqlCommand(sqlConsulta, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();

            string status = "";
            while (reader.Read())
            {
                if ((reader["Idproduto_ec"].ToString() == null) || (reader["Idproduto_ec"].ToString() == ""))
                {
                    produtosSGE.Add(new ListaProduto(reader["Idproduto"].ToString(), reader["Idproduto_ec"].ToString(), reader["Descricao"].ToString(), reader["Titulo_ec"].ToString(), decimal.Parse(reader["ValorSGE"].ToString()), 0, "Não Enviado", true, false, "", false, false, "Gray", false));
                    
                }
                else
                {
                    try
                    {
                        var produtoEC = FuncoesRespostaML.BuscarItemML(reader["Idproduto_ec"].ToString());
                        switch (produtoEC.status.ToString())
                        {
                            case "active":
                                status = "Ativo";
                                break;
                            case "paused":
                                status = "Pausado";
                                break;
                            case "closed":
                                status = "Finalizado";
                                break;
                            default:
                                status = "Não Enviado";
                                break;
                        }
                        if(status == "Finalizado")
                        {
                            produtosSGE.Add(new ListaProduto(reader["Idproduto"].ToString(), reader["Idproduto_ec"].ToString(), reader["Descricao"].ToString(), reader["Titulo_ec"].ToString(), produtoEC.price, 0, status, false, true, produtoEC.permalink, true, true, "White", true));
                        }
                        else
                        {
                            produtosSGE.Add(new ListaProduto(reader["Idproduto"].ToString(), reader["Idproduto_ec"].ToString(), reader["Descricao"].ToString(), reader["Titulo_ec"].ToString(), produtoEC.price, produtoEC.initial_quantity - produtoEC.sold_quantity, status, false, true, produtoEC.permalink, true, false, "White", false));
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro: \n" + ex, "Erro");
                    }
                }
            }

            reader.Close();

            return produtosSGE;
        }
    
        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnAnteriorProdutoDG.IsEnabled = true;
                    btnProximoProdutoDG.IsEnabled = true;
                    if (produtosSGE.Count >= (pageIndex * numeroPaginas))
                    {
                        if (produtosSGE.Skip(pageIndex *
                        numeroPaginas).Take(numeroPaginas).Count() == 0)
                        {
                            dataGridListaDeProdutos.ItemsSource = null;
                            dataGridListaDeProdutos.ItemsSource = produtosSGE.Skip((pageIndex *
                            numeroPaginas) - numeroPaginas).Take(numeroPaginas);
                            count = (pageIndex * numeroPaginas) +
                            (produtosSGE.Skip(pageIndex *
                            numeroPaginas).Take(numeroPaginas)).Count();
                        }
                        else
                        {
                            dataGridListaDeProdutos.ItemsSource = null;
                            dataGridListaDeProdutos.ItemsSource = produtosSGE.Skip(pageIndex *
                            numeroPaginas).Take(numeroPaginas);
                            count = (pageIndex * numeroPaginas) +
                            (produtosSGE.Skip(pageIndex *
                            numeroPaginas).Take(numeroPaginas)).Count();
                            pageIndex++;
                        }

                        lbNumeroProdutosInicial.Content = count;
                        lbNumeroProdutosFinal.Content = produtosSGE.Count();
                    }

                    else
                    {
                        btnAnteriorProdutoDG.IsEnabled = true;
                        btnProximoProdutoDG.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnAnteriorProdutoDG.IsEnabled = true;
                    btnProximoProdutoDG.IsEnabled = true;
                    if (pageIndex > 1)
                    {
                        pageIndex -= 1;
                        dataGridListaDeProdutos.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            dataGridListaDeProdutos.ItemsSource = produtosSGE.Take(numeroPaginas);
                            count = produtosSGE.Take(numeroPaginas).Count();
                            lbNumeroProdutosInicial.Content = count;
                            lbNumeroProdutosFinal.Content = produtosSGE.Count();
                        }
                        else
                        {
                            dataGridListaDeProdutos.ItemsSource = produtosSGE.Skip
                            (pageIndex * numeroPaginas).Take(numeroPaginas);
                            count = Math.Min(pageIndex * numeroPaginas, produtosSGE.Count);
                            lbNumeroProdutosInicial.Content = count;
                            lbNumeroProdutosFinal.Content = produtosSGE.Count();
                        }
                    }
                    else
                    {
                        btnAnteriorProdutoDG.IsEnabled = false;
                        btnProximoProdutoDG.IsEnabled = true;
                    }
                    break;
            }
        }

        private void btnProdutoEcommerce_Click(object sender, RoutedEventArgs e)
        {
            ListaProduto produtoSelecionado = (ListaProduto)dataGridListaDeProdutos.SelectedItem;
            CdtProduto prod = new CdtProduto(produtoSelecionado.IdProduto, produtoSelecionado.DescricaoSGE, produtoSelecionado.Preco, false);
            if(prod.erroVariacaoSGE == false)
            {
                var produto = prod.ShowDialog();
            }
            CarregaDataGridProdutosEcommerce();
        }

        private void btnProdutoEcommerceEditar_Click(object sender, RoutedEventArgs e)
        {
            ListaProduto produtoSelecionado = (ListaProduto)dataGridListaDeProdutos.SelectedItem;
            EditarProdutoML editar = new EditarProdutoML(produtoSelecionado.IdProduto, produtoSelecionado.IdProdutoEcommerce);
            var editarr = editar.ShowDialog();
            CarregaDataGridProdutosEcommerce();
        }

        private void btnProdutoEcommerceReenviar_Click(object sender, RoutedEventArgs e)
        {
            ListaProduto produtoSelecionado = (ListaProduto)dataGridListaDeProdutos.SelectedItem;
            CdtProduto prod = new CdtProduto(produtoSelecionado.IdProduto, produtoSelecionado.DescricaoSGE, produtoSelecionado.Preco, true);
            if (prod.erroVariacaoSGE == false)
            {
                var produto = prod.ShowDialog();
            }
            CarregaDataGridProdutosEcommerce();
        }

        private void btnProdutoEcommerceExcluir_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ATENÇÂO!!!" + "\n\n" +
                                "Você realmente quer excluir todos os dados do Painel Ecommerce deste produto? \n" +
                                "Todos os dados que você cadastrou serão excluidos!\n" +
                                "Caso queira enviar novamente o produto será necessário refazer todo o cadastro,\n" +
                                "tanto no SGE quanto no Painel Ecommerce.\n\n" +
                                "Deseja realmente deseja excluir os dados do Ecommerce deste Produto?", "Finalizar Excluir Dados do Produto no Ecommerce", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
            {
            }
            else
            {
                ListaProduto produtoSelecionado = (ListaProduto)dataGridListaDeProdutos.SelectedItem;
                ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);

                string sql2 = "DELETE FROM Tproduto_monitora_ec WHERE Idproduto = '" + produtoSelecionado.IdProduto + "'";
                SqlCommand cmd2 = new SqlCommand(sql2, ConexaoBancoDados.Conexao);
                cmd2.ExecuteNonQuery();

                string sql1 = "DELETE FROM Tprodutoec WHERE Idproduto = '" + produtoSelecionado.IdProduto + "'";
                SqlCommand cmd1 = new SqlCommand(sql1, ConexaoBancoDados.Conexao);
                cmd2.ExecuteNonQuery();

                string sql3 = "UPDATE Tprodutoi SET E_commerce = 0 WHERE Idproduto = '" + produtoSelecionado.IdProduto + "'";
                SqlCommand cmd3 = new SqlCommand(sql3, ConexaoBancoDados.Conexao);
                cmd3.ExecuteNonQuery();

                CarregaDataGridProdutosEcommerce();
            }
        }

        private void btnProdutosEcommerce_Click(object sender, RoutedEventArgs e)
        {
            CarregarProdutosImportaEcommerce();
        }

        private void CarregarProdutosImportaEcommerce()
        {
            produtoEC.Clear();
            dgListaProdutosEcommerceSGE.Visibility = Visibility.Collapsed;
            dgListaProdutosEc.Visibility = Visibility.Visible;

            string listaItensUsuario = "";
            List<String> listaItens = new List<String>();
            List<RespostaBuscaItemML> retornoItensEcommerce = new List<RespostaBuscaItemML>();

            var listaPrdSemVinculacaoSGE = ProdutosController.RetornaProdutosEcommerceNaoVinculadosSGE(FuncoesRespostaML.BuscarItemCadastradoML(SessaoConexao.IdUsuario));

            if (listaPrdSemVinculacaoSGE.Count > 20)
            {
                int controle = 0;

                for (int i = 0; i < listaPrdSemVinculacaoSGE.Count; i++)
                {
                    listaItensUsuario = listaItensUsuario + listaPrdSemVinculacaoSGE[i] + ",";
                    controle++;

                    if (controle == 20)
                    {
                        controle = 0;
                        listaItens.Add(new string(listaItensUsuario));
                        listaItensUsuario = "";
                    }
                }

                int verificaLista = listaItens.Where(x => x == listaItensUsuario).Count();

                if (verificaLista == 0)
                {
                    listaItens.Add(new string(listaItensUsuario));
                }
            }
            else
            {
                for (int i = 0; i < listaPrdSemVinculacaoSGE.Count; i++)
                {
                    listaItensUsuario = listaItensUsuario + listaPrdSemVinculacaoSGE[i] + ",";
                }

                listaItens.Add(new string(listaItensUsuario));
            }

            if (listaItensUsuario != "")
            {
                for (int i = 0; i < listaItens.Count; i++)
                {
                    var retornoItens = FuncoesRespostaML.BuscarVariosItemCadastradoML(listaItensUsuario.Substring(0, listaItens[i].Length - 1));

                    for (int a = 0; a < retornoItens.Length; a++)
                    {
                        retornoItensEcommerce.Add(new RespostaBuscaItemML(retornoItens[a].code, retornoItens[a].body));
                    }
                }

                for (int i = 0; i < retornoItensEcommerce.Count; i++)
                {
                    if (retornoItensEcommerce[i].code == 200)
                    {
                        ItemML item = new ItemML();
                        item.title = retornoItensEcommerce[i].body.title;
                        item.category_id = retornoItensEcommerce[i].body.category_id;
                        item.price = retornoItensEcommerce[i].body.price;
                        item.currency_id = retornoItensEcommerce[i].body.currency_id;
                        item.available_quantity = retornoItensEcommerce[i].body.available_quantity;
                        item.buying_mode = retornoItensEcommerce[i].body.buying_mode;
                        item.condition = retornoItensEcommerce[i].body.condition;
                        item.listing_type_id = retornoItensEcommerce[i].body.listing_type_id;
                        item.description = FuncoesRespostaML.BuscaDescricaoItemML(retornoItensEcommerce[i].body.id)[0];
                        item.video_id = retornoItensEcommerce[i].body.video_id;
                        item.sale_terms = retornoItensEcommerce[i].body.sale_terms;

                        List<ImagemItemML> imgItem = new List<ImagemItemML>();

                        for (int a = 0; a < retornoItensEcommerce[i].body.pictures.Count; a++)
                        {
                            imgItem.Add(new ImagemItemML(retornoItensEcommerce[i].body.pictures[a].secure_url, ""));
                        }

                        item.pictures = imgItem;

                        item.attributes = retornoItensEcommerce[i].body.attributes;
                        item.variations = retornoItensEcommerce[i].body.variations;

                        string jsonEnvioEcommerce;

                        if (retornoItensEcommerce[i].body.variations.Count == 0)
                        {
                            jsonEnvioEcommerce = JsonConvert.SerializeObject(item, new JsonSerializerSettings()
                            {
                                ContractResolver = new IgnorePropertiesResolver(new[] { "variations" })
                            });
                        }
                        else
                        {
                            jsonEnvioEcommerce = JsonConvert.SerializeObject(item);
                        }

                        string status;

                        switch (retornoItensEcommerce[i].body.status)
                        {
                            case "active":
                                status = "Ativo";
                                break;
                            case "paused":
                                status = "Pausado";
                                break;
                            case "closed":
                                status = "Finalizado";
                                break;
                            default:
                                status = "Não Enviado";
                                break;
                        }

                        produtoEC.Add(new ProdutoImportEcommerce(retornoItensEcommerce[i].body.id, retornoItensEcommerce[i].body.title, retornoItensEcommerce[i].body.available_quantity,
                                                                 retornoItensEcommerce[i].body.price, status, true, retornoItensEcommerce[i].body.permalink, true, "White", jsonEnvioEcommerce));
                    }
                }
            }
            else
            {
                MessageBox.Show("Não tem nenhum produto para ser vinculado no Ecommerce", "Vincular Produto");
            }


            dataGridListaProdutosEc.ItemsSource = produtoEC;
        }

        private void btnProdutoEcommerceVincular_Click(object sender, RoutedEventArgs e)
        {
            ProdutoImportEcommerce pdrImport = (ProdutoImportEcommerce)dataGridListaProdutosEc.SelectedItem;
            VincularItemEcommerce itemv = new VincularItemEcommerce(pdrImport.JsonEnvio, pdrImport.IdProdutoEcommerce);
            var vincularItem = itemv.ShowDialog();
        }
    }
}
