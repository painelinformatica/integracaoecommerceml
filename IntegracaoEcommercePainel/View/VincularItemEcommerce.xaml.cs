﻿using IntegracaoEcommercePainel.Controller.VincularItemEcommerce;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using MaterialDesignThemes.Wpf;
using System.Linq;
using IntegracaoEcommercePainel.Model.BuscarItemSGE;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.ModelML.EnvioItemML;
using IntegracaoEcommercePainel.ModelML.AtributosEditarML;

namespace IntegracaoEcommercePainel.View
{
    public partial class VincularItemEcommerce : Window
    {
        string jsonItem;
        string idProduto;
        string idProdutoEC;
        bool produtoComVariacao;
        List<string> VariacaoEC = new List<string>();
        List<ProdutoListaSGEEcommerce> skuVariacaoEC = new List<ProdutoListaSGEEcommerce>();

        public VincularItemEcommerce(string jsonItemEcommerce, string idProdutoEc)
        {
            InitializeComponent();
            jsonItem = jsonItemEcommerce;
            idProdutoEC = idProdutoEc;
            List<String> listaVariacao = new List<string>();

            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void btnBuscarProdutosSGE_Click(object sender, RoutedEventArgs e)
        {
            BuscarItemVincularEcommerce buscarItem = new BuscarItemVincularEcommerce();
            var buscar = buscarItem.ShowDialog();
            idProduto = buscar;
            CarregarProduto(jsonItem, idProduto);
        }

        public void CarregarProduto(string jsonItem, string idProduto)
        {
            dgListaPdr.Visibility = Visibility.Visible;
            btnSalvarVincularProduto.Visibility = Visibility.Visible;
            dataGridListaDeProdutos.Width = 1200;
            VariacaoEC.Clear();
            //colunaVariacao.ItemsSource = "";

            var item = VincularItemEcommerceController.CarregarItemSGE(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, idProduto);
            colunaCor.Visibility = item[0].CorVisivel;
            if(item[0].CorVisivel == Visibility.Collapsed)
            {
                dataGridListaDeProdutos.Width -= 100;
            }
            colunaTamanho.Visibility = item[0].TamanhoVisivel;
            if (item[0].TamanhoVisivel == Visibility.Collapsed)
            {
                dataGridListaDeProdutos.Width -= 100;
            }
            colunaVoltagem.Visibility = item[0].VoltagemVisivel;
            if (item[0].VoltagemVisivel == Visibility.Collapsed)
            {
                dataGridListaDeProdutos.Width -= 100;
            }
            colunaSabor.Visibility = item[0].SaborVisivel;
            if (item[0].SaborVisivel == Visibility.Collapsed)
            {
                dataGridListaDeProdutos.Width -= 100;
            }

            ItemML itemML = JsonConvert.DeserializeObject<ItemML>(jsonItem);
            if(itemML.variations != null)
            {
                for (int i = 0; i < itemML.variations.Count; i++)
                {
                    string variacao = "";

                    string idVariacao = itemML.variations[i].id.ToString();

                    /*if(itemML.variations[i].available_quantity > 0)
                    {*/
                        for (int a = 0; a < itemML.variations[i].attribute_combinations.Count; a++)
                        {
                            variacao = variacao + itemML.variations[i].attribute_combinations[a].value_name + "/";
                        }

                        VariacaoEC.Add(new string(idVariacao + " - " + variacao.Substring(0, variacao.Length - 1)));
                    /*}*/
                }
                produtoComVariacao = true;
            }
            else
            {
                VariacaoEC.Add(new string("Sem Variação"));
                produtoComVariacao = false;
            }

            /*for (int i = 0; i < item.Count; i++)
            {
                item[i].VariacaoEC = VariacaoEC;
            }*/

            colunaVariacao.ItemsSource = VariacaoEC;
            dataGridListaDeProdutos.ItemsSource = item;
        }

        private void btnSalvarVincularProduto_Click(object sender, RoutedEventArgs e)
        {
            int qtdVariacao = VariacaoEC.Count();
            int controleQtdVariacao = 0;
            skuVariacaoEC.Clear();

            if (produtoComVariacao == true)
            {
                for (int i = 0; i < dataGridListaDeProdutos.Items.Count; i++)
                {
                    ProdutoVincularEcommerce itemve = (ProdutoVincularEcommerce)dataGridListaDeProdutos.Items[i];

                    if (itemve.Variacao != "")
                    {

                        String[] variacaoec = itemve.Variacao.Split(" - ");
                        int contemVariacao = skuVariacaoEC.Where(x => x.VariacaoEC == variacaoec[1]).Count();
                        if (contemVariacao > 0)
                        {
                            MessageBox.Show("Não pode vincular a mesma variação do Ecommerce em mais de uma \n variação do SGE!", "Erro ao Vincular");
                            return;
                        }
                        else
                        {
                            string tipoVariacaoSGE = "";
                            string variacaoSGE = "";
                            controleQtdVariacao++;

                            if ((itemve.CorVisivel == Visibility.Visible) && (itemve.TamanhoVisivel == Visibility.Visible) && (itemve.SaborVisivel == Visibility.Collapsed) && (itemve.VoltagemVisivel == Visibility.Collapsed)) {
                                tipoVariacaoSGE = "Cor - Tamanho";
                                variacaoSGE = itemve.Cor + " / " + itemve.Tamanho;
                            }
                            else if ((itemve.CorVisivel == Visibility.Visible) && (itemve.TamanhoVisivel == Visibility.Collapsed) && (itemve.SaborVisivel == Visibility.Collapsed) && (itemve.VoltagemVisivel == Visibility.Visible))
                            {
                                tipoVariacaoSGE = "Cor - Voltagem";
                                variacaoSGE = itemve.Cor + " / " + itemve.Voltagem;
                            }
                            else if ((itemve.CorVisivel == Visibility.Visible) && (itemve.TamanhoVisivel == Visibility.Collapsed) && (itemve.SaborVisivel == Visibility.Collapsed) && (itemve.VoltagemVisivel == Visibility.Collapsed))
                            {
                                tipoVariacaoSGE = "Cor";
                                variacaoSGE = itemve.Cor;
                            }
                            else if ((itemve.CorVisivel == Visibility.Collapsed) && (itemve.TamanhoVisivel == Visibility.Visible) && (itemve.SaborVisivel == Visibility.Collapsed) && (itemve.VoltagemVisivel == Visibility.Collapsed))
                            {
                                tipoVariacaoSGE = "Tamanho";
                                variacaoSGE = itemve.Tamanho;
                            }
                            else if ((itemve.CorVisivel == Visibility.Collapsed) && (itemve.TamanhoVisivel == Visibility.Collapsed) && (itemve.SaborVisivel == Visibility.Visible) && (itemve.VoltagemVisivel == Visibility.Collapsed))
                            {
                                tipoVariacaoSGE = "Sabor";
                                variacaoSGE = itemve.Sabor;
                            }
                            else if ((itemve.CorVisivel == Visibility.Collapsed) && (itemve.TamanhoVisivel == Visibility.Collapsed) && (itemve.SaborVisivel == Visibility.Collapsed) && (itemve.VoltagemVisivel == Visibility.Visible))
                            {
                                tipoVariacaoSGE = "Voltagem";
                                variacaoSGE = itemve.Voltagem;
                            }

                            skuVariacaoEC.Add(new ProdutoListaSGEEcommerce(itemve.IdProduto, itemve.SkuProduto, Convert.ToInt64(variacaoec[0]), variacaoec[1], tipoVariacaoSGE, variacaoSGE, itemve.QuantidadeProduto, itemve.PrecoProduto, itemve.IdProdutoV));

                        }
                    }
                }

                if (controleQtdVariacao != VariacaoEC.Count())
                {
                    MessageBox.Show("Todas as variações do Ecommerce devem estar vinculadas as variações do SGE!", "Erro ao Vincular");
                    return;
                }
            }
            else if (produtoComVariacao == false)
            {
                string tipoVariacao = "Sem Variação";
                string variacao = "Sem Variação";

                ProdutoVincularEcommerce itemve = (ProdutoVincularEcommerce)dataGridListaDeProdutos.Items[0];

                skuVariacaoEC.Add(new ProdutoListaSGEEcommerce(itemve.IdProduto, itemve.SkuProduto, 0, "Sem Variação", tipoVariacao, variacao, itemve.QuantidadeProduto, itemve.PrecoProduto, itemve.IdProdutoV));
            }

             
            btnSalvarVincularProduto.Command = DialogHost.OpenDialogCommand;
            MensagemConfirmacaoDadosProduto();

        }

        private void btnVincularPrdEc_Click(object sender, RoutedEventArgs e)
        {
            /*var teste1 = jsonItem;
            var teste2 = idProduto;
            var teste3 = skuVariacaoEC;*/

            string vincular = "";

            if (produtoComVariacao == true)
            {
                for (int i = 0; i < skuVariacaoEC.Count; i++)
                {
                    AtributosEditar atributos = new AtributosEditar();

                    List<AtributoItemML> addAtributos = new List<AtributoItemML>();
                    addAtributos.Add(new AtributoItemML("SELLER_SKU", skuVariacaoEC[i].ProdutoSKU));

                    atributos.attributes = addAtributos;

                    vincular = FuncoesML.FuncoesEditarML.AdicionarAtributoSKUVariacaoItem(idProdutoEC, SessaoConexao.AccessToken, skuVariacaoEC[i].IdVariacaoEC.ToString(), atributos);
                }
            }
            else if (produtoComVariacao == false)
            {
                AtributosEditar atributos = new AtributosEditar();
                List<AtributoItemML> addAtributos = new List<AtributoItemML>();

                for (int i = 0; i < skuVariacaoEC.Count; i++)
                {
                    addAtributos.Add(new AtributoItemML("SELLER_SKU", skuVariacaoEC[i].ProdutoSKU));
                }

                atributos.attributes = addAtributos;

                vincular = FuncoesML.FuncoesEditarML.AdicionarAtributoSKUItem(idProdutoEC, SessaoConexao.AccessToken, atributos);
            }

            if(vincular == "OK")
            {
                var item = JsonConvert.DeserializeObject<ItemMLPai>(jsonItem);
                VincularItemEcommerceController.HabilitarProdutoEcommerce(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, idProduto);
                VincularItemEcommerceController.InserirDadosEC(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, idProduto, SessaoConexao.IdLoja, SessaoConexao.Ecommerce, item.title, item.description.plain_text, idProdutoEC, jsonItem);
                VincularItemEcommerceController.InserirDadosMonitoraEC(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, skuVariacaoEC, SessaoConexao.IdLoja, SessaoConexao.Ecommerce, idProdutoEC);

                MessageBox.Show("Produto Vinculado com Sucesso", "Vincular Produto");
                this.Close();
            }
            else
            {
                MessageBox.Show("Erro ao vincular o produto, tente novamente ou entre em contato com nosso Suporte!", "Erro ao Vincular");
            }
        }

        private void MensagemConfirmacaoDadosProduto()
        {
            iconVincular.Kind = PackIconKind.CheckBold;
            txtMsgVincular.Text = "Vincular Produto";

            PackIcon iconVar = CriarCamposView.CriarIcone(PackIconKind.ViewList, HorizontalAlignment.Left, 20, 20, 0, 10, 0, 0);
            msgVincular.Children.Add(iconVar);

            TextBlock varc = CriarCamposView.CriarTextBlock("Variacações", 30, HorizontalAlignment.Left, 35, -20, 0, 0, Brushes.Black, Brushes.Transparent);
            msgVincular.Children.Add(varc);

            for (int a = 0; a < skuVariacaoEC.Count; a++)
            {
                var item = (ProdutoListaSGEEcommerce)skuVariacaoEC[a];
                
                PackIcon iconVari = CriarCamposView.CriarIcone(PackIconKind.ArrowRight, HorizontalAlignment.Left, 15, 15, 0, 0, 0, 0);
                msgVincular.Children.Add(iconVari);

                TextBlock tipovari = CriarCamposView.CriarTextBlock(item.TipoVariacaoSGE,  20, HorizontalAlignment.Left, 45, -17, 0, 0, Brushes.Black, Brushes.Transparent);
                msgVincular.Children.Add(tipovari);

                TextBlock vari = CriarCamposView.CriarTextBlock("VariacaoSGE: " + item.VariacaoSGE + " - Variação Ecommerce: " + item.VariacaoEC, 20, HorizontalAlignment.Left, 45, -2, 0, 0, Brushes.Black, Brushes.Transparent);
                msgVincular.Children.Add(vari);
            }

            TextBlock msg = CriarCamposView.CriarTextBlock("Você deseja realmente vincular essas variações \ndo ecommerce com as variações do SGE?", 50, HorizontalAlignment.Left, 45, 20, 0, 0, Brushes.Black, Brushes.Transparent);
            msgVincular.Children.Add(msg);
        }
    }
}
