﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoPainel.ModelML.Categoria;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IntegracaoPainel.View
{
    public partial class CategoriasML : Window
    {
        public string diretorioCredencialJson = @"C:\painelinformatica\ecommerce\credenciais";
        public string conexaoBD = "conexaoBD.json";
        public string idCategoriaML;
        public string decricaoCategoriaML;
        public int contador = 1;
        public string idCategoriaMLSelecionada;
        public string categoriaMLSelecionada;

        public CategoriasML(string idCategoriaML)
        {
            InitializeComponent();
            if((idCategoriaML == null)||(idCategoriaML == ""))
            {
                CriarComboBoxCategoria();
            }
            else
            {
                EditarCategoria(idCategoriaML);
            }
            
            /*EditarCategoria();*/
            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void MoverBox(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnFechar(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public new (string,string) ShowDialog()
        {
            base.ShowDialog();

            return (idCategoriaMLSelecionada,categoriaMLSelecionada);
        }

        private void EditarCategoria(string idCategoriaML)
        {
            CategoriaML categoriasFilhos = FuncoesRespostaML.buscarCategoriaFilhoML(idCategoriaML);
            for(int i=0; i < categoriasFilhos.path_from_root.Count(); i++)
            {
                contador = i;
                ComboBox cboxp = new ComboBox();
                cboxp.Width = 250;
                cboxp.Height = 25;
                cboxp.IsReadOnly = true;
                cboxp.Foreground = Brushes.White;
                cboxp.BorderBrush = Brushes.White;
                ColorZoneAssist.SetMode(cboxp, ColorZoneMode.Inverted);
                if (i == 0)
                {
                    cboxp.Name = "CategoriaPai";
                    cboxp.Margin = new Thickness(0, 20, 0, 0);

                    ComboBoxItem cboxitem = new ComboBoxItem();
                    cboxitem.Content = categoriasFilhos.path_from_root[i].id + " - " + categoriasFilhos.path_from_root[i].name;
                    /*cboxp.Text = cboxitem.Content.ToString();*/
                    cboxp.SelectedValuePath = "Content";
                    cboxp.SelectedValue = cboxitem.Content.ToString();
                    cboxp.Items.Add(cboxitem);

                    List<CategoriaML> categorias = FuncoesRespostaML.buscarCategoriaPaisML("MLB");
                    for (int a = 0; a < categorias.Count(); a++)
                    {
                        ComboBoxItem cboxitem2 = new ComboBoxItem();
                        cboxitem2.Content = categorias[a].id + " - " + categorias[a].name;
                        if(cboxitem2.Content.ToString() != cboxitem.Content.ToString())
                        {
                            cboxp.Items.Add(cboxitem2);
                        }
                    }
                }
                else
                {
                    cboxp.Name = "CategoriaFilho_" + contador;
                    cboxp.Margin = new Thickness(0, 20, 0, 0);
                    ComboBoxItem cboxitem = new ComboBoxItem();
                    cboxitem.Content = categoriasFilhos.path_from_root[i].id + " - " + categoriasFilhos.path_from_root[i].name;
                    /*cboxp.Text = cboxitem.Content.ToString();*/
                    cboxp.SelectedValuePath = "Content";
                    cboxp.SelectedValue = cboxitem.Content.ToString();
                    cboxp.Items.Add(cboxitem);

                    CategoriaML categoriasFilhos2 = FuncoesRespostaML.buscarCategoriaFilhoML(categoriasFilhos.path_from_root[i-1].id);
                    for (int b = 0; b < categoriasFilhos2.children_categories.Count(); b++)
                    {
                        ComboBoxItem cboxitem2 = new ComboBoxItem();
                        cboxitem2.Content = categoriasFilhos2.children_categories[b].id + " - " + categoriasFilhos2.children_categories[b].name;
                        if (cboxitem2.Content.ToString() != cboxitem.Content.ToString())
                        {
                            cboxp.Items.Add(cboxitem2);
                        }
                    }
                }
                cboxp.SelectionChanged += selecionarCategoriaFilho;
                LayoutCategoria.Children.Add(cboxp);
                btnSalvarCategoria.IsEnabled = true;
            }
        }

        private void CriarComboBoxCategoria()
        {
            ComboBox cboxp = new ComboBox();
            cboxp.Name = "CategoriaPai";
            cboxp.Width = 250;
            cboxp.Height = 25;
            cboxp.Margin = new Thickness(0, 20, 0, 0);
            cboxp.IsReadOnly = true;
            cboxp.Foreground = Brushes.White;
            cboxp.BorderBrush = Brushes.White;
            ColorZoneAssist.SetMode(cboxp, ColorZoneMode.Inverted);
            List<CategoriaML> categorias = FuncoesRespostaML.buscarCategoriaPaisML("MLB");
            for(int i = 0; i < categorias.Count(); i++)
            {
                ComboBoxItem cboxitem = new ComboBoxItem();
                cboxitem.Content = categorias[i].id +" - "+ categorias[i].name;
                cboxp.Items.Add(cboxitem);
            }

            cboxp.SelectionChanged += selecionarCategoriaFilho;
            LayoutCategoria.Children.Add(cboxp);
        }

        private void selecionarCategoriaFilho(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nameCbx = new ComboBox();
            ComboBoxItem itemCbx = new ComboBoxItem();
            nameCbx = (ComboBox)sender;
            itemCbx = (ComboBoxItem)e.AddedItems[0];
            

            if (nameCbx.Name != "CategoriaPai")
            {
                String[] nivelFilho = nameCbx.Name.ToString().Split("_");
                
                int nivel = Convert.ToInt32(nivelFilho[1]);
                if(nivel < contador)
                {
                    while (LayoutCategoria.Children.Count > nivel+1)
                    {
                        LayoutCategoria.Children.RemoveAt(LayoutCategoria.Children.Count - 1);
                    }
                    btnSalvarCategoria.IsEnabled = false;
                    contador = nivel+1;
                }
                else
                {
                    contador++;
                }
            } 
            else if (nameCbx.Name == "CategoriaPai")
            {
                while (LayoutCategoria.Children.Count > 1)
                {
                    LayoutCategoria.Children.RemoveAt(LayoutCategoria.Children.Count - 1);
                    contador = 1;
                    btnSalvarCategoria.IsEnabled = false;
                }
            }

            String[] idCategoria = itemCbx.Content.ToString().Split(" - ");

            

            ComboBox cbox = new ComboBox();
            cbox.Name = "CategoriaFilho_"+contador;
            cbox.Width = 250;
            cbox.Height = 25;
            cbox.Margin = new Thickness(0, 20, 0, 0);
            cbox.Foreground = Brushes.White;
            cbox.BorderBrush = Brushes.White;
            cbox.IsReadOnly = true;
            ColorZoneAssist.SetMode(cbox, ColorZoneMode.Inverted);
            CategoriaML categoriasFilhos = FuncoesRespostaML.buscarCategoriaFilhoML(idCategoria[0]);
            for (int i = 0; i < categoriasFilhos.children_categories.Count(); i++)
            { 
                if (categoriasFilhos.settings.status == "enabled") {
                    ComboBoxItem cboxitem = new ComboBoxItem();
                    cboxitem.Content = categoriasFilhos.children_categories[i].id + " - " + categoriasFilhos.children_categories[i].name;
                    cbox.Items.Add(cboxitem);
                }
            }
            cbox.SelectionChanged += selecionarCategoriaFilho;
            
            if (categoriasFilhos.children_categories.Count() == 0)
            {
                idCategoriaMLSelecionada = idCategoria[0];
                categoriaMLSelecionada = idCategoria[1];
                idCategoriaML = idCategoria[0];
                decricaoCategoriaML = idCategoria[1];
                cbox.Visibility = Visibility.Collapsed;
                btnSalvarCategoria.IsEnabled = true;
            }

            LayoutCategoria.Children.Add(cbox);
        }

        private void btnSalvarCategoria_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
