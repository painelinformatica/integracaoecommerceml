﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace IntegracaoPainel.View
{
    /// <summary>
    /// Lógica interna para MinhaLoja.xaml
    /// </summary>
    public partial class MinhaLoja : Window
    {
        public string diretorio = @"C:\painelinformatica\ecommerce\credenciais";
        public string arqUsuarioTeste = "usuarioTeste.json";
        public string ipServ;
        public string bd;
        public string lj;
        public string accessToken;

        public MinhaLoja(string ipServidor, string banco, string loja)
        {
            ipServ = ipServidor;
            bd = banco;
            lj = loja;
            InitializeComponent();
        }

        private void MoverBox(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnFechar(object sender, RoutedEventArgs e)
        {
            this.Close();
            Console.WriteLine("click fechar");
        }

        private void CriarUsuarioTeste(object sender, RoutedEventArgs e)
        {
            dataGridMinhaLoja.Columns.Clear();

            DataGridTextColumn id = new DataGridTextColumn();
            id.Header = "Id";
            id.Binding = new Binding("id");
            dataGridMinhaLoja.Columns.Add(id);

            DataGridTextColumn nickname = new DataGridTextColumn();
            nickname.Header = "Nickname";
            nickname.Binding = new Binding("nickname");
            dataGridMinhaLoja.Columns.Add(nickname);

            DataGridTextColumn password = new DataGridTextColumn();
            password.Header = "Password";
            password.Binding = new Binding("password");
            dataGridMinhaLoja.Columns.Add(password);

            DataGridTextColumn siteStatus = new DataGridTextColumn();
            siteStatus.Header = "Site Status";
            siteStatus.Binding = new Binding("site_status");
            dataGridMinhaLoja.Columns.Add(siteStatus);

            DataGridTextColumn email = new DataGridTextColumn();
            email.Header = "Email";
            email.Binding = new Binding("email");
            dataGridMinhaLoja.Columns.Add(email);

            var listaUsuarioTeste = File.ReadAllText(Path.Combine(diretorio, arqUsuarioTeste));
            List<UsuarioTeste> usuarioTeste = JsonConvert.DeserializeObject<List<UsuarioTeste>>(listaUsuarioTeste);

            dataGridMinhaLoja.ItemsSource = usuarioTeste;
        }

        private void VerVendas(object sender, RoutedEventArgs e)
        {
            dataGridMinhaLoja.Columns.Clear();

            DataGridTextColumn teste1 = new DataGridTextColumn();
            teste1.Header = "Teste1";
            teste1.Binding = new Binding("Teste1");
            dataGridMinhaLoja.Columns.Add(teste1);

            DataGridTextColumn teste2 = new DataGridTextColumn();
            teste2.Header = "Teste2";
            teste2.Binding = new Binding("Teste2");
            dataGridMinhaLoja.Columns.Add(teste2);

            DataGridTextColumn teste3 = new DataGridTextColumn();
            teste3.Header = "Teste3";
            teste3.Binding = new Binding("Teste3");
            dataGridMinhaLoja.Columns.Add(teste3);
        }

        private void CarregarProdutos(object sender, RoutedEventArgs e)
        {
            dataGridMinhaLoja.Columns.Clear();

            DataGridTextColumn idProduto = new DataGridTextColumn();
            idProduto.Header = "IdProduto";
            idProduto.Binding = new Binding("IdProduto");
            dataGridMinhaLoja.Columns.Add(idProduto);

            DataGridTextColumn titulo = new DataGridTextColumn();
            titulo.Header = "Título";
            titulo.Binding = new Binding("Titulo");
            dataGridMinhaLoja.Columns.Add(titulo);

            DataGridTextColumn descricao = new DataGridTextColumn();
            descricao.Header = "Descrição";
            descricao.Binding = new Binding("Descricao");
            dataGridMinhaLoja.Columns.Add(descricao);

            /*DataGridTextColumn categoria = new DataGridTextColumn();
            categoria.Header = "Categoria";
            categoria.Binding = new Binding("categoria");
            dataGridMinhaLoja.Columns.Add(categoria);*/

            var btnCategoria = new FrameworkElementFactory(typeof(Button));
            btnCategoria.SetBinding(Button.ContentProperty, new Binding("Categoria"));
            btnCategoria.AddHandler(Button.ClickEvent, new RoutedEventHandler(MostrarCategorias));
            dataGridMinhaLoja.Columns.Add(new DataGridTemplateColumn() { Header = "Categoria", Width=80 ,CellTemplate = new DataTemplate() { VisualTree = btnCategoria } });

            DataGridTextColumn preco = new DataGridTextColumn();
            preco.Header = "Preço";
            preco.Binding = new Binding("Preco");
            dataGridMinhaLoja.Columns.Add(preco);

            DataGridTextColumn tipoAnuncio = new DataGridTextColumn();
            tipoAnuncio.Header = "Tipo de Anúncio";
            tipoAnuncio.Binding = new Binding("TipoAnuncio");
            dataGridMinhaLoja.Columns.Add(tipoAnuncio);

            DataGridTextColumn garantia = new DataGridTextColumn();
            garantia.Header = "Garantia";
            garantia.Binding = new Binding("Garantia");
            dataGridMinhaLoja.Columns.Add(garantia);

            var btnVariacao = new FrameworkElementFactory(typeof(Button));
            btnVariacao.SetBinding(Button.ContentProperty, new Binding("Variações"));
            btnVariacao.AddHandler(Button.ClickEvent, new RoutedEventHandler(MostrarVariacoes));
            dataGridMinhaLoja.Columns.Add(new DataGridTemplateColumn(){Header = "Variação", CellTemplate = new DataTemplate() { VisualTree = btnVariacao } });

            string sql = "select tp.Idproduto, tpi.Descricao, tp.Vlr_venda1 from Tproduto as tp inner join Tprodutoi as tpi on tp.Idproduto = tpi.Idproduto ";
            ConexaoBancoDados.GetConexao(ipServ, bd);
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            List<ProdutoML> produtoML = new List<ProdutoML>();
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                int i = 0;
                while (reader.Read())
                {
                    produtoML.Add(new ProdutoML(reader["Idproduto"].ToString(), "Teste", "Categorias", (decimal)reader["Vlr_venda1"], "Gold", reader["Descricao"].ToString(), "Fabrica"));
                    i++;
                }
            }
            reader.Close();
            dataGridMinhaLoja.ItemsSource = produtoML;

        }

        private void MostrarVariacoes(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("TesteVariacao");
        }
        
        private void MostrarCategorias(object sender, RoutedEventArgs e)
        {
            /*ConexaoBancoDados.GetConexao(ipServ, bd);
            string sqlquery2 = "SELECT Acesstokenml FROM Tbdadosdeacessoml";
            SqlCommand cmd3 = new SqlCommand(sqlquery2, ConexaoBancoDados.Conexao);
            cmd3.CommandType = CommandType.Text;
            SqlDataReader reader3;
            reader3 = cmd3.ExecuteReader();
            reader3.Read();
            string ac = reader3[0].ToString();
            reader3.Close();

            var teste = FuncoesRespostaML.UploadImagemML(ac);
            string a = "";*/

            /*CategoriasML categoriasML = new CategoriasML();
            var categoriaRetorno = categoriasML.ShowDialog();*/

            /*var teste = FuncoesRespostaML.EnviarProdutoML();
            string a = "";*/

            /*CadastroProduto cadastroProduto = new CadastroProduto();
            var produto = cadastroProduto.ShowDialog();*/

            /*testeMaterialDesign teste = new testeMaterialDesign();
            var teste2 = teste.ShowDialog();*/

            /*CdtProduto cadastroProduto = new CdtProduto();
            var produto = cadastroProduto.ShowDialog();*/
        }
    }
}
