﻿using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.Categoria;
using IntegracaoPainel.ModelML.Produtos;
using IntegracaoPainel.ModelML.Status;
using IntegracaoPainel.ModelML.Variacao;
using IntegracaoPainel.RetornoItemML;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;


namespace IntegracaoPainel.View
{
    public partial class EditarProdutoML : Window
    {
        private string idProdutoEc;
        private string idProdutoWSGE;

        ObservableCollection<VariacaoDataGridSGE> variacaoDgSge = new ObservableCollection<VariacaoDataGridSGE>();
        ObservableCollection<EditarProduto> editar = new ObservableCollection<EditarProduto>();

        private string corJson;
        private string tamanhoJson;
        private string voltagemJson;
        private string saborJson;

        private string atributosCombinados;
        private string tipoDeAtributos;

        private int variacaoErroSGE = 0;

        public EditarProdutoML(string idProdutoSGE, string idProdutoEcommerce)
        {
            InitializeComponent();
            idProdutoWSGE = idProdutoSGE;
            idProdutoEc = idProdutoEcommerce;
            CarregarDadosItemEcommerce(idProdutoSGE, idProdutoEcommerce);
            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void CarregarDadosItemEcommerce(string idProdutoSGE,string idProdutoEcommerce)
        {
            string tipoVariacao = "";
            string variacao = "";

            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
            string sql = "Select * " +
                         "from Tprodutoi as tpi " +
                         "inner join Tprodutoec as tpec on tpec.Idproduto = tpi.Idproduto " +
                         "inner join Tgrupo as tg on tg.Idgrupo = tpi.Idgrupo " +
                         "where tpi.Idproduto = '" + idProdutoSGE + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            txtNomeProduto.Text = reader["Titulo_ec"].ToString();
            txtTextoDescricao.Text = reader["Descricao_ec"].ToString();
            var contVariacao = reader["Cont_variacao"].ToString();
            reader.Close();

            ItemMLRetorno jsonRepostaItem = FuncoesRespostaML.BuscarItemML(idProdutoEcommerce);
            VariacaoRetornoAtributo itemEcommerceVariacao = FuncoesRespostaML.BuscarVariacoesItemML(idProdutoEcommerce);
            var nomeCategoria = FuncoesRespostaML.buscarCategoriaFilhoML(jsonRepostaItem.category_id);
            txtCategoriaProduto.Text = jsonRepostaItem.category_id + " - " + nomeCategoria.name;
            txtPreco.Text = Convert.ToDecimal(jsonRepostaItem.price).ToString();

            if(contVariacao == "0")
            {
                tipoVariacao = "Sem Variação";
                variacao = "Sem Variação";

                string sqlv = "SELECT Qde_atual_ec from Tproduto_monitora_ec where Idproduto_ec = '" + idProdutoEcommerce + "'";
                SqlCommand cmdv = new SqlCommand(sqlv, ConexaoBancoDados.Conexao);
                cmdv.CommandType = CommandType.Text;
                SqlDataReader readerv;
                readerv = cmdv.ExecuteReader();
                readerv.Read();
                decimal estoque = Convert.ToDecimal(readerv["Qde_atual_ec"].ToString());
                readerv.Close();

                variacaoDgSge.Add(new VariacaoDataGridSGE(tipoVariacao, variacao, estoque, "", "", true, false));
                btnEnviarNovaVariacao.IsEnabled = false;

            } 
            else if(contVariacao == "3")
            {
               /* List<AtributosCategoriaML> camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(jsonRepostaItem.category_id);*/
                for (int a = 0; a < jsonRepostaItem.variations.Count; a++)
                {
                    for (int b = 0; b < jsonRepostaItem.variations[a].attribute_combinations.Count; b++)
                    {
                        var categoriaNome = jsonRepostaItem.variations[a].attribute_combinations[b].name;
                        switch (categoriaNome)
                        {
                            case "Cor":
                                corJson = jsonRepostaItem.variations[a].attribute_combinations[b].value_name;
                                break;
                            case "Tamanho":
                                tamanhoJson = jsonRepostaItem.variations[a].attribute_combinations[b].value_name;
                                break;
                            case "Tamanho do capacete":
                                tamanhoJson = jsonRepostaItem.variations[a].attribute_combinations[b].value_name;
                                break;
                            case "Voltagem de entrada":
                                voltagemJson = jsonRepostaItem.variations[a].attribute_combinations[b].value_name;
                                break;
                            case "Sabor":
                                saborJson = jsonRepostaItem.variations[a].attribute_combinations[b].value_name;
                                break;
                        }
                    }

                    if ((corJson != null) && (tamanhoJson != null))
                    {
                        atributosCombinados = corJson + " - " + tamanhoJson;
                        tipoDeAtributos = "Cor - Tamanho";
                    }
                    else if ((corJson == null) && (tamanhoJson != null))
                    {
                        atributosCombinados = tamanhoJson;
                        tipoDeAtributos = "Tamanho";
                    }
                    else if ((corJson != null) && (voltagemJson != null))
                    {
                        atributosCombinados = corJson + " - " + voltagemJson;
                        tipoDeAtributos = "Cor - Voltagem";
                    }
                    else if ((corJson == null) && (voltagemJson != null))
                    {
                        atributosCombinados = voltagemJson;
                        tipoDeAtributos = "Voltagem";
                    }
                    else if (saborJson != null)
                    {
                        atributosCombinados = saborJson;
                        tipoDeAtributos = "Sabor";
                    }

                    string sqlv = "SELECT Qde_atual_ec from Tproduto_monitora_ec where Idprodutov_ec = " + jsonRepostaItem.variations[a].id;
                    SqlCommand cmdv = new SqlCommand(sqlv, ConexaoBancoDados.Conexao);
                    cmdv.CommandType = CommandType.Text;
                    SqlDataReader readerv;
                    readerv = cmdv.ExecuteReader();
                    readerv.Read();
                    decimal estoque = Convert.ToDecimal(readerv["Qde_atual_ec"].ToString());
                    readerv.Close();

                    for (int i = 0; i < itemEcommerceVariacao.variations.Count; i++)
                    {
                        if(itemEcommerceVariacao.variations[i].id == jsonRepostaItem.variations[a].id)
                        {
                            for (int d = 0; d < itemEcommerceVariacao.variations[i].attributes.Count; d++)
                            {
                                if(itemEcommerceVariacao.variations[i].attributes[d].id == "SELLER_SKU")
                                {
                                    variacaoDgSge.Add(new VariacaoDataGridSGE(tipoDeAtributos, atributosCombinados, estoque, "", itemEcommerceVariacao.variations[i].attributes[d].value_name, true, false));
                                }
                            }
                        }
                    }
                }
            }

            dataGridVariacoesSGE.ItemsSource = variacaoDgSge;

            txtTextoDescricao.IsEnabled = false;
            btnSalvarDescricao.IsEnabled = false;

            switch (jsonRepostaItem.status.ToString())
            {
                case "active":
                    txtStatus.Text = "Ativo";
                    btnStatusAtivo.IsEnabled = false;
                    btnStatusFinalizado.IsEnabled = false;
                    break;
                case "paused":
                    txtStatus.Text = "Pausado";
                    btnStatusAtivo.IsEnabled = true;
                    btnStatusFinalizado.IsEnabled = true;
                    btnStatusPausado.IsEnabled = false;
                    break;
                case "closed":
                    txtStatus.Text = "Finalizado";
                    btnStatusAtivo.IsEnabled = false;
                    btnStatusFinalizado.IsEnabled = false;
                    btnStatusPausado.IsEnabled = false;
                    btnEditarDescricao.IsEnabled = false;
                    btnEditarDescricao.IsEnabled = false;
                    btnEnviarNovaVariacao.IsEnabled = false;
                    break;
            }
        }

        private void btnEditarDescricao_Click(object sender, RoutedEventArgs e)
        {
            txtTextoDescricao.IsEnabled = true;
            btnEditarDescricao.IsEnabled = false;
            btnSalvarDescricao.IsEnabled = true;
        }

        private void btnSalvarDescricao_Click(object sender, RoutedEventArgs e)
        {
            DescricaoItemML descriscaoNova = new DescricaoItemML();
            descriscaoNova.plain_text = txtTextoDescricao.Text;

            ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
            string sql = "Select Idproduto, Idloja, Ecommerce, Descricao_ec, Json_ec_envio from Tprodutoec Where Idproduto_ec = '" + idProdutoEc + "'";
            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            reader.Read();
            string DescricaoAntiga = reader["Descricao_ec"].ToString();
            ItemML json3 = JsonConvert.DeserializeObject<ItemML>(reader["Json_ec_envio"].ToString());
            reader.Close();

            json3.description.plain_text = txtTextoDescricao.Text;

            if (MessageBox.Show("Deseja realmente alterar a descrição no Ecommerce deste Produto?", "Editar Descrição do Produto Ecommerce", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
            {
            }
            else
            {
                var jsonEditado = JsonConvert.SerializeObject(json3);
                string sql1 = "Update Tprodutoec Set Descricao_ec = '" + txtTextoDescricao.Text + "', Json_ec_envio = '" + jsonEditado + "' Where Idproduto_ec = '" + idProdutoEc + "'";
                SqlCommand cmd1 = new SqlCommand(sql1, ConexaoBancoDados.Conexao);
                cmd1.ExecuteNonQuery();

                var json2 = JsonConvert.SerializeObject(descriscaoNova);
                var json = FuncoesEditarML.EditarDescricaoItem(idProdutoEc, SessaoConexao.AccessToken, json2);

                string logRespostaProduto = "Editar Descrição do Ecommercer editada às " + DateTime.Now + "\n\n" + "Dados recebidos: " + "\n\n" + json + "\n\n" + "----------------------------\n" + "Editada às " + DateTime.Now + "\nPor: " +SessaoConexao.UsuarioLogadoSGE;
                Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\produtoEcommerce\" + idProdutoWSGE, "EditarProduto-" + Util.NormalizaHora(DateTime.Now) + ".txt", logRespostaProduto);

                /*Util.CriarArquivoCredencialJson(@"C:\painelinformatica\ecommerce\credenciais", "JsonAlteracaoProduto-Id-" + idProdutoWSGE + " - " + DateTime.Now.ToString("yyyyMMddHHmmss") + ".json", json);*/

                MessageBox.Show("Descrição Alterada com Sucesso!", "Editar Descrição do Produto Ecommerce");

                btnEditarDescricao.IsEnabled = true;
                btnSalvarDescricao.IsEnabled = false;
                txtTextoDescricao.IsEnabled = false;
            }
        }

        private void btnStatusAtivo_Click(object sender, RoutedEventArgs e)
        {
            StatusItemML status = new StatusItemML();
            status.status = "active";

            string statusAtivo = JsonConvert.SerializeObject(status);

            FuncoesEditarML.EditarStatusItem(idProdutoEc, SessaoConexao.AccessToken, statusAtivo);
            MessageBox.Show("Anúncio Ativado com Sucesso", "Mudança de Status do Produto");

            try
            {
                ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                string sql = "UPDATE Tproduto_monitora_ec SET Status_produto_ec = 0 WHERE Idproduto = '" + idProdutoWSGE + "'";
                SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Entre em contato com nosso suporte!\n Erro: " + ex, "Erro");
            }

            txtStatus.Text = "Ativo";
            btnStatusPausado.IsEnabled = true;
            btnStatusAtivo.IsEnabled = false;
            btnStatusFinalizado.IsEnabled = false;
        }

        private void btnStatusPausado_Click(object sender, RoutedEventArgs e)
        {
            StatusItemML status = new StatusItemML();
            status.status = "paused";

            string statusAtivo = JsonConvert.SerializeObject(status);

            FuncoesEditarML.EditarStatusItem(idProdutoEc, SessaoConexao.AccessToken, statusAtivo);
            MessageBox.Show("Anúncio Pausado com Sucesso", "Mudança de Status do Produto");

            try
            {
                ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                string sql = "UPDATE Tproduto_monitora_ec SET Status_produto_ec = 1 WHERE Idproduto = '" + idProdutoWSGE + "'";
                SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Entre em contato com nosso suporte!\n Erro: " + ex, "Erro");
            }

            txtStatus.Text = "Pausado";
            btnStatusPausado.IsEnabled = false;
            btnStatusAtivo.IsEnabled = true;
            btnStatusFinalizado.IsEnabled = true;
        }

        private void btnStatusFinalizado_Click(object sender, RoutedEventArgs e)
        {
            
            if (MessageBox.Show("ATENÇÂO!!!" + "\n\n" +
                                "Depois que você finalizar o anúncio não sera possível reativar o anúncio! \n" +
                                "Caso queira anúnciar este produto novamente tera que fazer um novo anúncio.\n"+
                                "Você perderá todos os dados do item como: \n" +
                                "Número de vendas, comentários, avaliações entre outras informações.\n\n"+
                                "Deseja realmente deseja finalizar o anúncio deste Produto?", "Finalizar Anúncio do Ecommerce", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
            {
            }
            else
            {
                StatusItemML status = new StatusItemML();
                status.status = "closed";

                string statusAtivo = JsonConvert.SerializeObject(status);
                FuncoesEditarML.EditarStatusItem(idProdutoEc, SessaoConexao.AccessToken, statusAtivo);

                DeleteItemML delete = new DeleteItemML();
                delete.deleted = "true";

                string deleteTrue = JsonConvert.SerializeObject(delete);
                FuncoesEditarML.EditarStatusItem(idProdutoEc, SessaoConexao.AccessToken, deleteTrue);

                MessageBox.Show("Anúncio Finalizado com Sucesso", "Mudança de Status do Produto");
                try
                {
                    ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                    string sql = "DELETE FROM Tproduto_monitora_ec WHERE Idproduto = '" + idProdutoWSGE + "'";
                    SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Entre em contato com nosso suporte!\n Erro: " + ex, "Erro");
                }
                

                txtStatus.Text = "Finalizado";
                btnStatusPausado.IsEnabled = false;
                btnStatusAtivo.IsEnabled = false;
                btnStatusFinalizado.IsEnabled = false;
            }
        }

        private void btnVoltar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnEnviarNovaVariacao_Click(object sender, RoutedEventArgs e)
        {
            String[] idCategoria = txtCategoriaProduto.Text.Split(" - ");

            EditarAddVariacao editarVariacao = new EditarAddVariacao(variacaoDgSge, idProdutoWSGE, txtPreco.Text,idCategoria[0], idProdutoEc);
            if(editarVariacao.erroVariacaoSGE == false)
            {
                var addVariacao = editarVariacao.ShowDialog();
            }


            variacaoDgSge.Clear();
            CarregarDadosItemEcommerce(idProdutoWSGE, idProdutoEc);
        }
    }
}
