﻿using IntegracaoEcommercePainel.FuncoesML;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace IntegracaoPainel.View
{
    /// <summary>
    /// Lógica interna para CadastroProduto.xaml
    /// </summary>
    public partial class CadastroProduto : Window
    {
        public int contVariacao = 1;

        public CadastroProduto()
        {
            InitializeComponent();
        }

        private void MoverBox(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void BtnFechar(object sender, RoutedEventArgs e)
        {
            this.Close();
            Console.WriteLine("click fechar");
        }

        private void AddVariacao(object sender, RoutedEventArgs e)
        {
            TextBox txtVariacao = new TextBox();
            txtVariacao.Name = "txtVariacao" + contVariacao;
            txtVariacao.Height = 30;
            txtVariacao.Width = 152;
            txtVariacao.Margin = new Thickness(0, 5, 0, 0);
            txtVariacao.HorizontalAlignment = HorizontalAlignment.Left;
            carregarVariacoes.Children.Add(txtVariacao);

            ComboBox cbxVariacao = new ComboBox();
            cbxVariacao.Name = "txtTipoVariacao" + contVariacao;
            cbxVariacao.Height = 30;
            cbxVariacao.Width = 152;
            cbxVariacao.Margin = new Thickness(157, -30, 0, 0);
            cbxVariacao.HorizontalAlignment = HorizontalAlignment.Left;
            cbxVariacao.IsEditable = true;
            cbxVariacao.IsReadOnly = true;
            cbxVariacao.SelectedValue = new Binding("cbxVariacao" + contVariacao);
            carregarVariacoes.Children.Add(cbxVariacao);

            Button delVariacao = new Button();
            BitmapImage btm = new BitmapImage(new Uri("/Assets/Img/minus.png", UriKind.Relative));
            Image img = new Image();
            img.Source = btm;
            delVariacao.Name = "btnDelVariacao" + contVariacao;
            delVariacao.Background = Brushes.Transparent;
            delVariacao.Foreground = Brushes.Transparent;
            delVariacao.BorderBrush = Brushes.Transparent;
            delVariacao.Style = this.Resources["ImageButtonStyle2"] as Style;
            delVariacao.Height = 29;
            delVariacao.Width = 29;
            delVariacao.Margin = new Thickness(314, -30, 0, 0);
            delVariacao.HorizontalAlignment = HorizontalAlignment.Left;
            delVariacao.Content = img;
            delVariacao.Click += DelVariacao;
            carregarVariacoes.Children.Add(delVariacao);
            contVariacao++;
        }

        private void DelVariacao(object sender, RoutedEventArgs e)
        {
            carregarVariacoes.Children.RemoveAt(carregarVariacoes.Children.Count - 3);
        }

        private void btnSelecionarImagens_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var teste = lbImagens.Items.Count;
            
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                    lbImagens.Items.Add(Path.GetFullPath(filename));
            }
        }

        private void btnDeletarImagens_Click(object sender, RoutedEventArgs e)
        {
            lbImagens.Items.Remove(lbImagens.SelectedItem);
        }
    }
}
