﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using IntegracaoEcommercePainel.Controller.BuscarItemVincularEcommerce;
using IntegracaoEcommercePainel.Model.BuscarItemSGE;
using IntegracaoPainel.Model;

namespace IntegracaoEcommercePainel.View
{
    /// <summary>
    /// Lógica interna para BuscarItemVincularEcommerce.xaml
    /// </summary>
    public partial class BuscarItemVincularEcommerce : Window
    {
        public int pageIndex = 1;
        public int numeroTotalItens = 0;
        public int numeroItensPorPagina = 50;
        public int numeroDePaginas = 0;
        List<ProdutoVincularEcommerce> listaProdutos = new List<ProdutoVincularEcommerce>();

        public string idProduto;

        public BuscarItemVincularEcommerce()
        {
            InitializeComponent();
            numeroTotalItens = BuscarItemVincularEcommerceController.RetornaQuantidadeProdutosSGE(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, "");
            BuscaProdutoSGE("", 0);
            btnAnteriorProdutoDG.IsEnabled = false;
            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        public new String ShowDialog()
        {
            base.ShowDialog();
            return idProduto;
        }

        private void btnAnteriorProdutoDG_Click(object sender, RoutedEventArgs e)
        {
            pageIndex -= 1;
            if (pageIndex != 1)
            {
                int offSet = numeroItensPorPagina * pageIndex;
                BuscaProdutoSGE(txtBuscarItemSGE.Text, offSet);
                lbNumeroProdutosInicial.Content = pageIndex;
                btnAnteriorProdutoDG.IsEnabled = true;
                btnProximoProdutoDG.IsEnabled = true;
            }
            else if (pageIndex == 1)
            {
                BuscaProdutoSGE(txtBuscarItemSGE.Text, 0);
                btnAnteriorProdutoDG.IsEnabled = false;
            }
        }

        private void btnProximoProdutoDG_Click(object sender, RoutedEventArgs e)
        {
            pageIndex += 1;
            if (pageIndex != numeroDePaginas)
            {
                int offSet = numeroItensPorPagina * pageIndex;
                BuscaProdutoSGE(txtBuscarItemSGE.Text, offSet);
                lbNumeroProdutosInicial.Content = pageIndex;
                btnAnteriorProdutoDG.IsEnabled = true;
                btnProximoProdutoDG.IsEnabled = true;
            }
            else if (pageIndex == numeroDePaginas)
            {
                int offSet = (numeroTotalItens) - (numeroTotalItens - (numeroItensPorPagina * pageIndex));
                BuscaProdutoSGE(txtBuscarItemSGE.Text, Math.Abs(offSet));
                lbNumeroProdutosInicial.Content = pageIndex;
                btnProximoProdutoDG.IsEnabled = false;
            }
        }


        private void BuscaProdutoSGE(string itemBuscar, int offset)
        {
            listaProdutos = BuscarItemVincularEcommerceController.BuscarProdutosProdutosSGE(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, itemBuscar, offset);
            dataGridListaDeProdutos.ItemsSource = listaProdutos;
            var nPag = numeroTotalItens / 50.0;
            numeroDePaginas = (int)Math.Ceiling(nPag) - 1;
            if (Math.Ceiling(nPag) == 1)
            {
                btnAnteriorProdutoDG.IsEnabled = false;
                btnProximoProdutoDG.IsEnabled = false;
            }
            else
            {
                btnProximoProdutoDG.IsEnabled = true;
            }
            lbNumeroProdutosInicial.Content = 1;
            lbNumeroProdutosFinal.Content = numeroDePaginas;
        }

        private void txtBuscarItemSGE_TextChanged(object sender, TextChangedEventArgs e)
        {  
            Thread.Sleep(300);
            numeroTotalItens = BuscarItemVincularEcommerceController.RetornaQuantidadeProdutosSGE(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados, txtBuscarItemSGE.Text.ToUpper());
            BuscaProdutoSGE(txtBuscarItemSGE.Text.ToUpper(), 0);
            pageIndex = 1;
        }

        private void btnSelecionarItemSGE_Click(object sender, RoutedEventArgs e)
        {
            ProdutoVincularEcommerce prdSelecionado = (ProdutoVincularEcommerce)dataGridListaDeProdutos.SelectedItem;

            if (MessageBox.Show("ATENÇÃO!" + "\n\n" +
                                "O produto que você quer vincular: \n" +
                                "IDProduto: "+prdSelecionado.IdProduto+" \n" +
                                "Descricao: "+prdSelecionado.DescricaoSGE+" \n\n" +
                                "Você tem certeza que quer vincular este produto?", "Vincular Produto", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
            {
            }
            else
            {
                idProduto = prdSelecionado.IdProduto;
                this.Close();
            }
        }
    }
}
