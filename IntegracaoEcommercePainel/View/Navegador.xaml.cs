﻿using System;
using System.Windows;
using System.Windows.Threading;
using CefSharp;

namespace IntegracaoEcommercePainel
{

    public partial class Navegador : Window
    {
        private String Code;
        /*private String idAplicacao;
        private String skAplicacao;*/
        public string diretorioCredencialJson = @"C:\painelinformatica\ecommerce\credenciais";
        public string nomeArquivoCredencialML = "credencial.json";

        public Navegador(String IDAplicacao)
        {
            InitializeComponent();
            /*idAplicacao = IDAplicacao;
            skAplicacao = SKAPlicacao;*/
            UrlWebBrowser(IDAplicacao);
        }

        public new String ShowDialog()
        {
            base.ShowDialog();
            return Code;
        }

        public void UrlWebBrowser(String IDAplicacao)
        {
            string url = "https://auth.mercadolivre.com.br/authorization?response_type=code&client_id="+IDAplicacao+"&redirect_uri=https://onvio.painelinformatica.com.br";
            Browser.Address = url;

        }

        private void PegarCode(object sender, FrameLoadEndEventArgs e)
        {
            string url = e.Frame.Url;
            e.Frame.Url.ToString();
            Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                if (url.StartsWith("https://onvio.painelinformatica.com.br"))
                {
                    int posicao = url.IndexOf("=");
                    Code = url.Substring(posicao + 1, 37);

                    if (Code != null)
                    {
                        Browser.GetCookieManager().DeleteCookies();
                        this.Close();
                    }
                }
                else if (Title.ToString() == "Ops! Ocorreu um erro")
                {
                    Browser.GetCookieManager().DeleteCookies();
                    this.Close();
                }
            }));
        }
    }
}
