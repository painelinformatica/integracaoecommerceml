﻿using IntegracaoEcommercePainel.Util;
using IntegracaoPainel.ModelML.Imagem;
using IntegracaoPainel.ModelML.Produtos;
using IntegracaoPainel.ModelML.Variacao;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Data;
using System.Data.SqlClient;
using IntegracaoPainel.EnvioItemML;
using Newtonsoft.Json;
using IntegracaoPainel.Model;
using IntegracaoPainel.ModelML.UploadImagem;
using IntegracaoEcommercePainel.FuncoesML;
using IntegracaoPainel.ModelML.Categoria;
using IntegracaoPainel.Model.MonitoraEcommerce;
using NJsonSchema.Infrastructure;

namespace IntegracaoPainel.View
{
    public partial class EditarAddVariacao : Window
    {
        List<AtributosCategoriaML> camposCategoria;
        List<MonitoraEC> monitora = new List<MonitoraEC>();

        ObservableCollection<VariacaoDataGridSGE> variacoesProdutoEcommerce = new ObservableCollection<VariacaoDataGridSGE>();
        ObservableCollection<EditarProduto> editar = new ObservableCollection<EditarProduto>();
        ObservableCollection<VariacaoDataGridSGE> variacaoDGSge = new ObservableCollection<VariacaoDataGridSGE>();

        ObservableCollection<ImagemUrlAddDataGridML> listaImagensAdd = new ObservableCollection<ImagemUrlAddDataGridML>();
        ObservableCollection<ImgUrlDataGridML> listaImagensVariacoes = new ObservableCollection<ImgUrlDataGridML>();
        ObservableCollection<VariacaoDataGridAddImagem> variacaoSgeAddImg = new ObservableCollection<VariacaoDataGridAddImagem>();

        private string idProduto;
        private string idProdutoEc;
        private string precoProduto;
        private string idCategoriaProduto;
        private string tipoVariacao = "";
        private string variacao = "";
        private int variacaoErroSGE = 0;
        public bool erroVariacaoSGE = false;

        public EditarAddVariacao(ObservableCollection<VariacaoDataGridSGE> variacoesEcommerce, string idProdutoSGE, string preco, string idCategoria, string idProdutoEcommerce)
        {
            InitializeComponent();
            variacoesProdutoEcommerce = variacoesEcommerce;
            idProduto = idProdutoSGE;
            idProdutoEc = idProdutoEcommerce;
            precoProduto = preco;
            idCategoriaProduto = idCategoria;
            CarregarVariacoesSGESemEcommerce(idProdutoSGE);

            if(variacaoDGSge.Count == 0)
            {
                MessageBox.Show("Você não possui mais nenhuma variação para enviar!\nCaso queira enviar uma nova variação para o ecommerce,\ncadastre no SGE antes para poder prosseguir com o cadastro!","Produto sem Variação Cadastrada");
                erroVariacaoSGE = true;
                this.Close();
            }
            
            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }

        private void CarregarVariacoesSGESemEcommerce(string idProdutoSGE)
        {
            string sql = "Select tpv.Idproduto, tpec.Idproduto_ec, tpv.Idprodutov, tpv.Sku, tpec.Titulo_ec, tpec.Descricao_ec, tpv.Idcor, tc.Cor, tpv.Idtamanho, tt.Tamanho, tpv.Idvoltagem, tv.Voltagem, tpv.Idsabor, ts.Sabor, tpv.Qde_pend_ec, tpec.Json_ec_envio, tpi.Idgrupo, tg.Grupo, tg.Cont_variacao, tg.Cont_var_cor, tg.Cont_var_tamanho, tg.Cont_var_voltagem, tg.Cont_var_sabor " +
                         "from Tprodutov as tpv " +
                         "left join Tcor as tc on tc.Idcor = tpv.Idcor " +
                         "left join Ttamanho as tt on tt.Idtamanho = tpv.Idtamanho " +
                         "left join Tsabor as ts on ts.Idsabor = tpv.Idsabor " +
                         "left join Tvoltagem as tv on tv.Idvoltagem = tpv.Idvoltagem " +
                         "inner join Tprodutoi as tpi on tpi.Idproduto = tpv.Idproduto " +
                         "inner join Tprodutoec as tpec on tpec.Idproduto = tpv.Idproduto " +
                         "inner join Tgrupo as tg on tg.Idgrupo = tpi.Idgrupo " +
                         "where tpv.Idproduto = '" + idProdutoSGE + "'";

            SqlCommand cmd = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                
                editar.Add(new EditarProduto(reader["Idproduto"].ToString(), reader["Idproduto_ec"].ToString(), reader["Titulo_ec"].ToString(), reader["Descricao_ec"].ToString(),
                reader["Json_ec_envio"].ToString(), reader["Idgrupo"].ToString(), reader["Grupo"].ToString(), reader["Cont_variacao"].ToString(), (bool)reader["Cont_var_cor"], (bool)reader["Cont_var_tamanho"],
                (bool)reader["Cont_var_voltagem"], (bool)reader["Cont_var_sabor"], reader["Idprodutov"].ToString(), reader["Sku"].ToString(), reader["Idcor"].ToString(), reader["Cor"].ToString(),
                reader["Idtamanho"].ToString(), reader["Tamanho"].ToString(), reader["Idvoltagem"].ToString(), reader["Voltagem"].ToString(), reader["Idsabor"].ToString(), reader["Sabor"].ToString(), reader["Qde_pend_ec"].ToString()));
                  
            }
            reader.Close();

           for (int i = 0; i < editar.Count; i++)
            {
                if(editar[i].ContVariacao == "3")
                {
                    int variacaoInconsistente = 0;
                    if ((editar[i].ContVarCor == true) && (editar[i].ContVarTamanho == true) && (editar[i].ContVarSabor == false) && (editar[i].ContVarVoltagem == false))
                    {
                        tipoVariacao = "Cor - Tamanho";

                        if ((editar[i].Cor == "") || (editar[i].Tamanho == ""))
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Cor + " - " + editar[i].Tamanho;
                        }

                    }
                    else if ((editar[i].ContVarCor == true) && (editar[i].ContVarTamanho == false) && (editar[i].ContVarSabor == false) && (editar[i].ContVarVoltagem == true))
                    {
                        tipoVariacao = "Cor - Voltagem";

                        if ((editar[i].Cor == "") || (editar[i].Voltagem == ""))
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Cor  + " - " + editar[i].Voltagem;
                        }
                    }
                    else if ((editar[i].ContVarCor == true) && (editar[i].ContVarTamanho == false) && (editar[i].ContVarSabor == false) && (editar[i].ContVarVoltagem == false))
                    {
                        tipoVariacao = "Cor";
                        if (editar[i].Cor == "")
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Cor;
                        }
                    }
                    else if ((editar[i].ContVarCor == false) && (editar[i].ContVarTamanho == true) && (editar[i].ContVarSabor == false) && (editar[i].ContVarVoltagem == true))
                    {
                        tipoVariacao = "Tamanho";
                        if (editar[i].Tamanho == "")
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Tamanho;
                        }

                    }
                    else if ((editar[i].ContVarCor == false) && (editar[i].ContVarTamanho == false) && (editar[i].ContVarSabor == true) && (editar[i].ContVarVoltagem == true))
                    {
                        tipoVariacao = "Sabor";
                        if (editar[i].Sabor == "")
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Sabor;
                        }
                    }
                    else if ((editar[i].ContVarCor == false) && (editar[i].ContVarTamanho == false) && (editar[i].ContVarSabor == false) && (editar[i].ContVarVoltagem == true))
                    {
                        tipoVariacao = "Voltagem";
                        if (editar[i].Voltagem == "")
                        {
                            variacao = "Variação Inconsistente";
                            variacaoInconsistente++;
                        }
                        else
                        {
                            variacao = editar[i].Voltagem;
                        }
                    }

                    if (variacaoInconsistente == 0)
                    {
                        var possuiSku = variacoesProdutoEcommerce.ToList().Exists(x => x.SKU == editar[i].Sku);

                        if(possuiSku == false)
                        {
                            variacaoDGSge.Add(new VariacaoDataGridSGE(tipoVariacao, variacao, Convert.ToDecimal(editar[i].QdePendEc), editar[i].IdProdutoV, editar[i].Sku, false, true));
                        }
                    }
                    else
                    {
                        variacaoDGSge.Add(new VariacaoDataGridSGE(tipoVariacao, variacao, Convert.ToDecimal(editar[i].QdePendEc), editar[i].IdProdutoV, editar[i].Sku, false, false));
                        variacaoErroSGE++;
                    }
                }
            }

            if (variacaoErroSGE != 0)
            {
                MessageBox.Show("O produto possui variações inconsistentes no sistema." + "\n" + "Caso queira enviar essas variações para o ecommerce, " + "\n" + "corrija a variação no SGE antes de enviar novas variações.", "Varições inconsistentes", MessageBoxButton.OK,
                        MessageBoxImage.Warning);
            }

            dataGridVariacoesSGE.ItemsSource = variacaoDGSge;
        }

        private void btnCarregarImagensVariacaSGE_Click(object sender, RoutedEventArgs e)
        {
            int prdSelecEcommerce = 0;
            SelecionarVariacaoSGE.Items.Clear();

            for (int i = 0; i < variacaoDGSge.Count; i++)
            {
                if (variacaoDGSge[i].ItemSelecionadoEcommerce == true)
                {
                    SelecionarVariacaoSGE.Items.Add(variacaoDGSge[i].Variacao.ToString());
                    prdSelecEcommerce++;
                }
            }

            if (prdSelecEcommerce > 0)
            {
                btnCarregarImagensVariacaSGE.Command = DialogHost.OpenDialogCommand;
            }
            else
            {
                MessageBox.Show("Selecione pelo menos uma variação do SGE!", "Selecionar Imagens por Variação");
                btnCarregarImagensVariacaSGE.Command = null;
            }
        }

        private void btnExcluirVariacaoSGE_Click(object sender, RoutedEventArgs e)
        {
            ImgUrlDataGridML imagemSeleciona = (ImgUrlDataGridML)dataGridImagensVariacao.SelectedItem;

            listaImagensVariacoes.Remove(imagemSeleciona);

            dataGridImagensVariacao.ItemsSource = listaImagensVariacoes;
        }

        private void btnSelecionarVariacaoSGEImg_Click(object sender, RoutedEventArgs e)
        {
            variacaoSgeAddImg.Add(new VariacaoDataGridAddImagem(SelecionarVariacaoSGE.SelectedItem.ToString()));
            SelecionarVariacaoSGE.Items.Remove(SelecionarVariacaoSGE.SelectedItem.ToString());
            dataGridVariacaoAdd.ItemsSource = variacaoSgeAddImg;
        }

        private void btnExcluirVariacaoSGEImg_Click(object sender, RoutedEventArgs e)
        {
            VariacaoDataGridAddImagem variacaoSelecionada = (VariacaoDataGridAddImagem)dataGridVariacaoAdd.SelectedItem;


            if (variacaoSelecionada != null)
            {
                SelecionarVariacaoSGE.Items.Add(variacaoSelecionada.Variacao);
                variacaoSgeAddImg.Remove(variacaoSelecionada);
            }
            else
            {
                MessageBox.Show("Selecione uma Variação para remover da lista!", "Remover Variação");
            }

            dataGridVariacaoAdd.ItemsSource = variacaoSgeAddImg;
        }

        private void btnSelecionarImagem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                {
                    listaImagensAdd.Add(new ImagemUrlAddDataGridML(Path.GetFullPath(filename)));
                }
            }
            dataGridImagensAdd.ItemsSource = listaImagensAdd;
        }

        private void btnExcluirImagem_Click(object sender, RoutedEventArgs e)
        {
            ImagemUrlAddDataGridML imagemSeleciona = (ImagemUrlAddDataGridML)dataGridImagensAdd.SelectedItem;

            for (int i = 0; i < listaImagensAdd.Count; i++)
            {
                if (imagemSeleciona.URLImagem == listaImagensAdd[i].URLImagem)
                {
                    listaImagensAdd.RemoveAt(i);
                }
            }

            dataGridImagensAdd.ItemsSource = listaImagensAdd;

        }

        private void btnSalvarImagensVariacoesAdd_Click(object sender, RoutedEventArgs e)
        {

            for (int i = 0; i < variacaoSgeAddImg.Count; i++)
            {
                for (int b = 0; b < variacaoDGSge.Count; b++)
                {
                    if (variacaoDGSge[b].Variacao == variacaoSgeAddImg[i].Variacao)
                    {
                        tipoVariacao = variacaoDGSge[b].TipoVariacao;
                    }
                }

                for (int a = 0; a < listaImagensAdd.Count; a++)
                {
                    listaImagensVariacoes.Add(new ImgUrlDataGridML(tipoVariacao, variacaoSgeAddImg[i].Variacao, listaImagensAdd[a].URLImagem));
                }
            }

            if (listaImagensAdd.Count == 0)
            {
                btnSalvarImagensVariacoesAdd.Command = DialogHost.OpenDialogCommand;
                MessageBox.Show("Selecione ao menos 1(uma) imagem para a variação escolhida", "Erro so selecionar imagem", MessageBoxButton.OK);
            }
            else if (listaImagensAdd.Count != 0)
            {
                variacaoSgeAddImg.Clear();
                listaImagensAdd.Clear();
                btnSalvarImagensVariacoesAdd.Command = DialogHost.CloseDialogCommand;
                dataGridImagensVariacao.ItemsSource = listaImagensVariacoes;
            }
        }

        private void btnFecharImagensVariacoesAdd_Click(object sender, RoutedEventArgs e)
        {
            variacaoSgeAddImg.Clear();
            listaImagensAdd.Clear();
            btnFecharImagensVariacoesAdd.Command = DialogHost.CloseDialogCommand;
        }

        private void btnEnviarVariacao_Click(object sender, RoutedEventArgs e)
        {
            VariacaoItemML variacaoAdd = new VariacaoItemML();
            List<ImagemItemIdML> idImagens = new List<ImagemItemIdML>();
            List<ImagemItemML> urlImagem = new List<ImagemItemML>();
            List<ImagemItemML> urlImagemUpload = new List<ImagemItemML>();

            try
            {
                for (int a = 0; a < dataGridImagensVariacao.Items.Count; a++)
                {
                    ImgUrlDataGridML item = (ImgUrlDataGridML)dataGridImagensVariacao.Items[a];

                    var dadosVariacaoSGE = variacaoDGSge.Where(x => x.Variacao.Contains(item.VariacaoSGE.ToString()));

                    for (int b = 0; b < dadosVariacaoSGE.ToList().Count; b++)
                    {
                        var lista = dadosVariacaoSGE.ToList();
                        urlImagem.Add(new ImagemItemML(item.URLImagem, lista[b].IdProdutoV));
                    }
                }
                var urlImagemUnicas = urlImagem.GroupBy(x => x.source).Select(y => y.First()).ToList();

                for (int a = 0; a < urlImagemUnicas.Count(); a++)
                {
                    var url = urlImagem.FindAll(x => x.source == urlImagemUnicas[a].source);

                    try
                    {
                        var json = JsonConvert.DeserializeObject<UploadImagemML>(FuncoesRespostaML.UploadImagemML(SessaoConexao.AccessToken, @urlImagemUnicas[a].source));
                        ConexaoBancoDados.GetConexao(SessaoConexao.IpServidor, SessaoConexao.BancoDeDados);
                        for (int b = 0; b < json.variations.Count; b++)
                        {
                            if (json.variations[b].size == "400x400")
                            {
                                for (int c = 0; c < url.Count(); c++)
                                {
                                    inserirImagemBD(idProduto, SessaoConexao.IdLoja, url[c].IdProdutoV, json.id, json.variations[b].secure_url);
                                    idImagens.Add(new ImagemItemIdML(json.id, url[c].IdProdutoV));
                                }
                                urlImagemUpload.Add(new ImagemItemML(json.variations[b].secure_url, urlImagemUnicas[a].IdProdutoV));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Favor entrar em contato com nosso Suporte!" + "\n" + "Erro: " + ex, "Erro Enviar Imagem Adicionar Variação");
                    }
                }

                List<AtributosCategoriaML> camposCategoria = FuncoesRespostaML.AtributosItemPorCategoria(idCategoriaProduto);

                VariacaoItemML variacaoItem = new VariacaoItemML();

                for (int b = 0; b < dataGridVariacoesSGE.Items.Count; b++)
                {
                    ImagemItemIdML vincularImgEcommerce = new ImagemItemIdML();

                    VariacaoDataGridSGE itemSGE = (VariacaoDataGridSGE)dataGridVariacoesSGE.Items[b];
                    String[] tipoVaricaoSGE = itemSGE.TipoVariacao.ToString().Split(" - ");
                    String[] variacaoSGE = itemSGE.Variacao.ToString().Split(" - ");
                    

                    if (itemSGE.ItemSelecionadoEcommerce == true)
                    {

                        MonitoraEC monitora = new MonitoraEC();
                        monitora.IdProduto = idProduto;
                        monitora.IdLoja = SessaoConexao.IdLoja;
                        monitora.IdProdutoV = Convert.ToInt32(itemSGE.IdProdutoV);
                        monitora.Sku = itemSGE.SKU;
                        monitora.Ecommerce = SessaoConexao.Ecommerce;
                        monitora.ValorVendaAtualEc = Convert.ToDecimal(precoProduto);
                        monitora.QuantidadeAtualEc = itemSGE.QuantidadeVariacao;


                        List<CombinacaoAtributosML> variacaoProduto = new List<CombinacaoAtributosML>();
                        List<AtributoItemML> attVariacao = new List<AtributoItemML>();
                        attVariacao.Add(new AtributoItemML("SELLER_SKU", itemSGE.SKU));

                        try
                        {
                            for (int c = 0; c < tipoVaricaoSGE.Count(); c++)
                            {
                                var categoriaId = camposCategoria.FindAll(x => x.name.Contains(tipoVaricaoSGE[c]));
                                if ((categoriaId[0].id != null) || (categoriaId[0].id != ""))
                                {
                                    variacaoProduto.Add(new CombinacaoAtributosML(categoriaId[0].id, categoriaId[0].name, null, variacaoSGE[c]));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Favor entrar em contato com nosso Suporte!" + "\n" + "Erro: " + ex, "Erro enviar Categoria Adicionar Variação");
                        }

                        var idImagensSGE = idImagens.FindAll(x => x.IdProdutoV == itemSGE.IdProdutoV);
                        string[] idImgSge = new string[idImagensSGE.Count()];

                        for (int c = 0; c < idImagensSGE.Count(); c++)
                        {
                            idImgSge[c] = idImagensSGE[c].id;
                        }

                        variacaoItem.attribute_combinations = variacaoProduto;
                        variacaoItem.price = Convert.ToDecimal(precoProduto);
                        variacaoItem.available_quantity = Convert.ToInt32(itemSGE.QuantidadeVariacao);
                        variacaoItem.attributes = attVariacao;
                        variacaoItem.sold_quantity = 0;
                        variacaoItem.picture_ids = idImgSge;
                        variacaoItem.id = 0;
                        variacaoItem.catalog_product_id = null;

                        var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
                        jsonResolver.IgnoreProperty(typeof(CombinacaoAtributosML), "name");
                        jsonResolver.IgnoreProperty(typeof(CombinacaoAtributosML), "value_id");
                        jsonResolver.IgnoreProperty(typeof(VariacaoItemML), "id");
                        jsonResolver.IgnoreProperty(typeof(VariacaoItemML), "catalog_product_id");

                        var serializerSettings = new JsonSerializerSettings();
                        serializerSettings.ContractResolver = jsonResolver;
                        serializerSettings.Formatting = Formatting.Indented;

                        var jsonItemMLAddVariacao = JsonConvert.SerializeObject(variacaoItem, serializerSettings);

                        for(int z = 0; z < idImgSge.Length; z++)
                        {
                            vincularImgEcommerce.id = idImgSge[z];
                            vincularImgEcommerce.IdProdutoV = null;

                            var jsonVincularImagem = JsonConvert.SerializeObject(vincularImgEcommerce);
                            FuncoesEditarML.VincularImagemItem(idProdutoEc, SessaoConexao.AccessToken, jsonVincularImagem);
                        }

                        string logEnvioProduto = "Variacao Enviada às " + DateTime.Now + "\n\n" + "Dados enviados: " + "\n\n" + jsonItemMLAddVariacao + "\n\n" + "----------------------------\n" + "Enviado por " + SessaoConexao.UsuarioLogadoSGE + " às " + DateTime.Now;
                        Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\produtoEcommerce\" + idProduto, "AddVariacao-" + Util.NormalizaHora(DateTime.Now) + ".txt", logEnvioProduto);

                        var respostaItemAddVariacao = FuncoesEditarML.AdicionarVariacaoItem(idProdutoEc,SessaoConexao.AccessToken, jsonItemMLAddVariacao);
                        string logRespostaProduto = "Resposta Recebida às " + DateTime.Now + "\n\n" + "Dados enviados: " + "\n\n" + respostaItemAddVariacao + "\n\n" + "----------------------------\n" + "Recebido  às " + DateTime.Now;
                        Util.CriarPastaArquivo(@"C:\painelinformatica\ecommerce\produtoEcommerce\" + idProduto, "RetornoAddVariacao-" + Util.NormalizaHora(DateTime.Now) + ".txt", logRespostaProduto);

                        var retornoItemAddVariacao = JsonConvert.DeserializeObject<List<VariacaoItemML>>(respostaItemAddVariacao);

                        for (int d = 0; d < retornoItemAddVariacao.Count; d++)
                        {

                            if (retornoItemAddVariacao[d].attributes[0].value_name == monitora.Sku)
                            {
                                string sqlm = "INSERT INTO Tproduto_monitora_ec(Idproduto, Idloja, Idprodutov, Sku, Ecommerce, Idproduto_ec, Idprodutov_ec, Vlr_venda_atual_ec, Qde_atual_ec)" +
                                        "VALUES('" + monitora.IdProduto + "'," + monitora.IdLoja + "," + monitora.IdProdutoV + ",'" + monitora.Sku + "'," +
                                        "'" + monitora.Ecommerce + "','" + idProdutoEc+ "'," + retornoItemAddVariacao[d].id + "," + Util.ConverteValorSQL(monitora.ValorVendaAtualEc.ToString()) + "," + Util.ConverteValorSQL(monitora.QuantidadeAtualEc.ToString()) + ")";
                                SqlCommand cmdm = new SqlCommand(sqlm, ConexaoBancoDados.Conexao);
                                cmdm.ExecuteNonQuery();
                            }
                        }
                    }
                }

                MessageBox.Show("Variação cadastrada com sucesso!", "Cadastro de Variação");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor entrar em contato com nosso Suporte!" + "\n" + "Erro: " + ex, "Erro enviar Adicionar Variação");
            }
            
        }

        public void inserirImagemBD(string idProduto, int idLoja, string idProdutoV, string idImagemEcommerce, string httpsUrlImagemEcommerce)
        {
            string sql = "INSERT INTO Tproduto_imagem(Idproduto, Idloja, Idprodutov, Idimagemecommerce, Urlimagem) " +
                                 "VALUES('" + idProduto + "','" + idLoja + "', '" + idProdutoV + "','" + idImagemEcommerce + "', '" + httpsUrlImagemEcommerce + "')";
            SqlCommand command = new SqlCommand(sql, ConexaoBancoDados.Conexao);
            command.ExecuteNonQuery();
        }
    }
}
