﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntegracaoPainel.View
{
    /// <summary>
    /// Lógica interna para LIstaDeVendas.xaml
    /// </summary>
    public partial class LIstaDeVendas : Window
    {
        public LIstaDeVendas()
        {
            InitializeComponent();

            tbCopyright.Text = "Todos os direitos Reservados © - " + DateTime.Now.Year;
        }
    }
}
