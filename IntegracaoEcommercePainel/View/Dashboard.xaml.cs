﻿using IntegracaoPainel.Model;
using System.Windows;


namespace IntegracaoPainel.View
{
    /// <summary>
    /// Lógica interna para Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {

        public Dashboard()
        {
            InitializeComponent();
        }

        private void btnListaProduto_Click(object sender, RoutedEventArgs e)
        {

            Produtos produtos = new Produtos();
            var prd = produtos.ShowDialog();
        }
    }
}
