﻿using IntegracaoEcommercePainel.ModelML.AtributosEditarML;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.RetornoItemML;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegracaoEcommercePainel.FuncoesML
{
    public class FuncoesEditarML
    {
        public static string EditarDescricaoItem(string idItemML, string accessToken, string jsonNovaDescricao)
        {

            var editarProduto = new RestClient("https://api.mercadolibre.com/items/"+idItemML+"/description?api_version=2&access_token=" + accessToken);
            editarProduto.Timeout = -1;

            var request = new RestRequest(Method.PUT);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(jsonNovaDescricao);
            IRestResponse response = editarProduto.Execute(request);
            var itemML = response.Content;

            return itemML;
        }

        public static ItemMLRetorno EditarPrecoQuantidadeItem(string idItemML, string accessToken, string jsonEnvio)
        {
            var editarPreco = new RestClient("https://api.mercadolibre.com/items/" + idItemML);
            editarPreco.Timeout = -1;

            var request = new RestRequest(Method.PUT);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(jsonEnvio);
            IRestResponse response = editarPreco.Execute(request);
            var retornaPreco = JsonConvert.DeserializeObject<ItemMLRetorno>(response.Content);

            return retornaPreco;
        }

        public static string EditarStatusItem(string idItemML, string accessToken, string tipoStatus) 
        {
            var editarStatus = new RestClient("https://api.mercadolibre.com/items/"+idItemML+"?access_token="+accessToken);
            editarStatus.Timeout = -1;

            var request = new RestRequest(Method.PUT);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(tipoStatus);
            IRestResponse response = editarStatus.Execute(request);
            var retornoStatus = response.Content;

            return retornoStatus;
        }

        public static string AdicionarVariacaoItem(string idItemML, string accessToken, string variacao)
        {
            var addVariacao = new RestClient("https://api.mercadolibre.com/items/"+idItemML+"/variations");
            addVariacao.Timeout = -1;

            var request = new RestRequest(Method.POST);
            request.AddParameter("Authorization",string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(variacao);
            IRestResponse response = addVariacao.Execute(request);
            var retornoAddVariacao = response.Content;
            return retornoAddVariacao;
        }

        public static string VincularImagemItem(string idItemML, string accessToken, string imagem)
        {
            var vincularImagem = new RestClient("https://api.mercadolibre.com/items/" + idItemML + "/pictures");
            vincularImagem.Timeout = -1;

            var request = new RestRequest(Method.POST);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(imagem);
            IRestResponse response = vincularImagem.Execute(request);
            var retornoVincularImagem = response.Content;

            return retornoVincularImagem;
        }

        public static string AdicionarAtributoSKUVariacaoItem(string idItemML, string accessToken, string idVariacao, AtributosEditar atributo)
        {
            var addSKU = new RestClient("https://api.mercadolibre.com/items/"+idItemML+"/variations/"+idVariacao);
            addSKU.Timeout = -1;

            var request = new RestRequest(Method.PUT);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(atributo);
            IRestResponse response = addSKU.Execute(request);
            var retornoAddSKU = response.StatusCode.ToString();

            return retornoAddSKU;
        }
        
        public static string AdicionarAtributoSKUItem(string idItemML, string accessToken, AtributosEditar atributo)
        {
            var addSKU = new RestClient("https://api.mercadolibre.com/items/" + idItemML);
            addSKU.Timeout = -1;

            var request = new RestRequest(Method.PUT);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            request.AddJsonBody(atributo);
            IRestResponse response = addSKU.Execute(request);
            var retornoAddSKU = response.StatusCode.ToString();

            return retornoAddSKU;
        }
    }
}
