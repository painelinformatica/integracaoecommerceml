﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace IntegracaoPainel.FuncoesML
{
    public class FuncoesCallbackML
    {
        public static string BuscasCallbackRecebidosBdPainel(string idAplicacaoML, string dataHoraUltimaBusca)
        {
            string jsonRetorno = "";
            string dadosPOST = "application_id=" + idAplicacaoML;//aqui trocar o '328713340666240' pelo application_id
            dadosPOST += "&data=" + dataHoraUltimaBusca;//aqui trocar o '2021-01-07 13:00' pela data que vc buscou pela ultima vez
            var dados = Encoding.UTF8.GetBytes(dadosPOST);
            var requisicaoWeb = WebRequest.CreateHttp("http://onvio.painelinformatica.com.br/Buscar");
            requisicaoWeb.Method = "POST";
            requisicaoWeb.ContentType = "application/x-www-form-urlencoded";
            requisicaoWeb.ContentLength = dados.Length;


            //precisamos escrever os dados post para o stream
            using (var stream = requisicaoWeb.GetRequestStream())
            {
                stream.Write(dados, 0, dados.Length);
                stream.Close();
            }
            //ler e exibir a resposta
            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();
                streamDados.Close();
                resposta.Close();
                jsonRetorno = objResponse.ToString();
            }

            return jsonRetorno;
        }

        public static string ConsultarCallbackEcommerce(string resource, string accessToken)
        {
            var consulta = new RestClient("https://api.mercadolibre.com" + resource);
            consulta.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            IRestResponse response = consulta.Execute(request);
            var retornoConsulta = response.Content;

            return retornoConsulta;
        }
    }
}
