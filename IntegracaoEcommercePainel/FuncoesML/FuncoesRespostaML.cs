﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using IntegracaoPainel.Model;
using Newtonsoft.Json;
using IntegracaoEcommercePainel.Util;
using System.Data.SqlClient;
using System.Data;
using IntegracaoPainel.ModelML.Categoria;
using IntegracaoPainel.ModelML.Credencial;
using IntegracaoPainel.ModelML;
using IntegracaoPainel.EnvioItemML;
using IntegracaoPainel.RetornoItemML;
using IntegracaoPainel.ModelML.Variacao;
using IntegracaoPainel.ModelML.BuscarListaItemUsuario;

namespace IntegracaoEcommercePainel.FuncoesML
{
    public class FuncoesRespostaML
    {
        public static string diretorio = @"C:\painelinformatica\ecommerce\credenciais";
        public static string nomeArquivoVerificarAcessRokenValido = "dadosUsuarioML.json";
        public static string dadosAcessoML = "dadosAcessoML.json";
        public static string nomeArquivoCredencialML = "credencialML.json";
        public static string dadosUsuarioML = "dadosUsuarioML.json";
        public static string usuarioTeste = "usuarioTeste.json";
        public static string itemTeste = "itemTeste3.json";

        public static bool verificarAcessTokenValido(string accessToken, int idUsuario, string ipServidor, string banco)
        {
            var verificaCliente = new RestClient("https://api.mercadolibre.com/users/" + idUsuario + "?access_token=" + accessToken);
            verificaCliente.Timeout = -1;

            var request = new RestRequest(Method.GET);
            IRestResponse response = verificaCliente.Execute(request);

            try
            {
                RetornaDadosUsuarioML userML = JsonConvert.DeserializeObject<RetornaDadosUsuarioML>(response.Content);
                if (idUsuario == userML.id)
                {
                    ConexaoBancoDados.GetConexao(ipServidor, banco);
                    string sqlquery2 = "SELECT COUNT(*) FROM Tbdadosusuarioecommerceml";
                    SqlCommand cmd3 = new SqlCommand(sqlquery2, ConexaoBancoDados.Conexao);
                    cmd3.CommandType = CommandType.Text;
                    SqlDataReader reader3;
                    reader3 = cmd3.ExecuteReader();
                    reader3.Read();
                    string vUml = reader3[0].ToString();
                    reader3.Close();
                    if (vUml != "0")
                    {
                        string sql4 = "TRUNCATE TABLE Tbdadosusuarioecommerceml";
                        SqlCommand cmd4 = new SqlCommand(sql4, ConexaoBancoDados.Conexao);
                        cmd4.ExecuteNonQuery();
                    }
                    String query = "INSERT INTO Tbdadosusuarioecommerceml (Idusuario, Nickname, Email, Coutryid, Siteid) VALUES (" + userML.id.ToString() + ",'" + userML.nickname + "' , '" + userML.email + "','" + userML.country_id + "', '" + userML.site_id + "')";
                    SqlCommand command = new SqlCommand(query, ConexaoBancoDados.Conexao);
                    command.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
                return false;
            }
        }

        public static void refreshToken(string idAPP, string skAPP, string refreshToken, string ipServidor, string banco)
        {
            string idAplicacao = idAPP;
            string skAplicacao = skAPP;
            string refreshTokenAplicacao = refreshToken;

            var refreshTK = new RestClient("https://api.mercadolibre.com/oauth/token?grant_type=refresh_token&client_id=" + idAplicacao + "&client_secret=" + skAplicacao + "&refresh_token=" + refreshTokenAplicacao);
            refreshTK.Timeout = -1;

            var request = new RestRequest(Method.POST);
            IRestResponse response = refreshTK.Execute(request);

            TokenML credML = JsonConvert.DeserializeObject<TokenML>(response.Content);

            ConexaoBancoDados.GetConexao(ipServidor, banco);

            String query = "UPDATE Tbtoken SET Accesstoken = '" + credML.access_token + "' WHERE Refreshtoken='" + refreshToken + "'";
            SqlCommand command = new SqlCommand(query, ConexaoBancoDados.Conexao);
            command.ExecuteNonQuery();

            DateTime dataCriacaoArquivo = DateTime.Now;
            DateTime dataValidadeToken = dataCriacaoArquivo.AddSeconds(credML.expires_in);

            String query2 = "set dateformat dmy; UPDATE Tbdadosdeacessoml SET Accesstokenml = '" + credML.access_token + "', Dataautenticacao = '" + dataValidadeToken + "' WHERE Refreshtokenml='" + refreshToken + "'";
            SqlCommand command2 = new SqlCommand(query2, ConexaoBancoDados.Conexao);
            command2.ExecuteNonQuery();

        }

        public static String importaItensLojaML(string idUsuario, string accessToken)
        {
            return null;
        }

        public static void criarUsuarioTesteML()
        {
            var jsonUsuarioML = File.ReadAllText(Path.Combine(diretorio, dadosAcessoML));
            DadosDeAcessoML dadosAcesso = JsonConvert.DeserializeObject<DadosDeAcessoML>(jsonUsuarioML);

            var criarUsuarioTeste = new RestClient("https://api.mercadolibre.com/users/test_user?access_token=" + dadosAcesso.AccessTokenML);
            criarUsuarioTeste.Timeout = -1;

            var request = new RestRequest(Method.POST);
            /*request.AddHeader("site_id", "MLB");
            request.AddHeader("Content-Type", "application/json");*/
            request.AddParameter("application/json", "{site_id:MLB}", ParameterType.RequestBody);


            IRestResponse response = criarUsuarioTeste.Execute(request);
            var verificaArquivo = Util.Util.VerificarSeArquivoExiste(diretorio, usuarioTeste);

            if (verificaArquivo == "ArquivoExiste")
            {
                var json = File.ReadAllText(Path.Combine(diretorio, usuarioTeste));
                List<UsuarioTeste> usuariosCadastrados = JsonConvert.DeserializeObject<List<UsuarioTeste>>(json);
                UsuarioTeste novoUsuarioTeste = JsonConvert.DeserializeObject<UsuarioTeste>(response.Content);

                usuariosCadastrados.Add(novoUsuarioTeste);

                string novoUsuarioLista = JsonConvert.SerializeObject(usuariosCadastrados, Formatting.Indented);

                Util.Util.CriarArquivoCredencialJson(diretorio, usuarioTeste, novoUsuarioLista);
            }
            else
            {
                List<UsuarioTeste> usuariosCadastrados = new List<UsuarioTeste>();
                UsuarioTeste novoUsuarioTeste = JsonConvert.DeserializeObject<UsuarioTeste>(response.Content);
                usuariosCadastrados.Add(novoUsuarioTeste);

                string novoUsuarioLista = JsonConvert.SerializeObject(usuariosCadastrados, Formatting.Indented);

                Util.Util.CriarArquivoCredencialJson(diretorio, usuarioTeste, novoUsuarioLista);
            }
        }

        public static List<CategoriaML> buscarCategoriaPaisML(string pais)
        {
            var retornaCategoriaPais = new RestClient("https://api.mercadolibre.com/sites/" + pais + "/categories");
            retornaCategoriaPais.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaCategoriaPais.Execute(request);

            List<CategoriaML> categorias = JsonConvert.DeserializeObject<List<CategoriaML>>(response.Content);

            return categorias;
        }

        public static CategoriaML buscarCategoriaFilhoML(string idCategoria)
        {
            var retornaCategoriaPais = new RestClient("https://api.mercadolibre.com/categories/" + idCategoria);
            retornaCategoriaPais.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaCategoriaPais.Execute(request);

            CategoriaML categoriasFilhos = JsonConvert.DeserializeObject<CategoriaML>(response.Content);

            return categoriasFilhos;
        }

        public static string UploadImagemML(string accessToken, string caminhoImagem)
        {
            var uploadImagem = new RestClient("https://api.mercadolibre.com/pictures?access_token=" + accessToken);
            uploadImagem.Timeout = -1;

            RestRequest request = new RestRequest(Method.POST);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            request.AddFile("file", caminhoImagem);

            IRestResponse response = uploadImagem.Execute(request);
            var urlImagem = response.Content;

            return urlImagem;
        }

        /*public static string AtributosObrigatoriosCategoriaML(string idCategoria)
        {
            var atrObr = new RestClient("https://api.mercadolibre.com/categories/"+idCategoria+"/technical_specs/output");
            atrObr.Timeout = -1;

            RestRequest request = new RestRequest(Method.GET);
            IRestResponse response = atrObr.Execute(request);
            var retornoAtrObr = response.Content;

            return retornoAtrObr;
        }*/

        public static string EnviarProdutoML(string accessToken, string jsonItem)
        {

            var produto = new RestClient("https://api.mercadolibre.com/items?access_token=" + accessToken);
            produto.Timeout = -1;

            var request = new RestRequest(Method.POST);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            request.AddJsonBody(jsonItem);
            IRestResponse response = produto.Execute(request);
            var itemML = response.Content;

            return itemML;
        }

        public static List<AtributosCategoriaML> AtributosItemPorCategoria(string idCategoria)
        {
            var retornaAtrItem = new RestClient("https://api.mercadolibre.com/categories/" + idCategoria + "/attributes");
            retornaAtrItem.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaAtrItem.Execute(request);

            List<AtributosCategoriaML> atributos = JsonConvert.DeserializeObject<List<AtributosCategoriaML>>(response.Content);

            return atributos;
        }

        public static List<TipoGarantiaCategoriaML> GarantiaCategoriaML(string idCategoria)
        {
            var retornaGarantia = new RestClient("https://api.mercadolibre.com/categories/" + idCategoria + "/sale_terms");
            retornaGarantia.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaGarantia.Execute(request);

            List<TipoGarantiaCategoriaML> garantia = JsonConvert.DeserializeObject<List<TipoGarantiaCategoriaML>>(response.Content);

            return garantia;
        }

        public static List<PreditorCategoriaML> PreditorCategoriaML(string nomeProduto)
        {
            var retornaPreditorCategoria = new RestClient("https://api.mercadolibre.com/sites/MLB/domain_discovery/search?limit=1&q=" + nomeProduto);
            retornaPreditorCategoria.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaPreditorCategoria.Execute(request);

            List<PreditorCategoriaML> preditor = JsonConvert.DeserializeObject<List<PreditorCategoriaML>>(response.Content);

            return preditor;
        }

        public static ItemMLRetorno BuscarItemML(string idItemML)
        {
            var retornaItemML = new RestClient("https://api.mercadolibre.com/items/" + idItemML);
            retornaItemML.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaItemML.Execute(request);
            ItemMLRetorno itemML = JsonConvert.DeserializeObject<ItemMLRetorno>(response.Content);

            return itemML;
        }

        public static VariacaoRetornoAtributo BuscarVariacoesItemML(string idItemML)
        {
            var retornaVariacaoItemML = new RestClient("https://api.mercadolibre.com/items/" + idItemML + "?attributes=variations&include_attributes=all");
            retornaVariacaoItemML.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaVariacaoItemML.Execute(request);
            var retorno = JsonConvert.DeserializeObject<VariacaoRetornoAtributo>(response.Content);

            return retorno;
        }

        public static BuscarItens BuscarItemCadastradoML(string idUsuario)
        {
            var retornaListaDeProdutos = new RestClient("https://api.mercadolibre.com/users/" + idUsuario + "/items/search?search_type=scan");
            retornaListaDeProdutos.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaListaDeProdutos.Execute(request);
            var retorno = JsonConvert.DeserializeObject<BuscarItens>(response.Content);

            return retorno;
        }

        public static DescricaoItemML[] BuscaDescricaoItemML(string idItemML)
        {
            var retornaDescricao = new RestClient("https://api.mercadolibre.com/items/" + idItemML + "/descriptions");
            retornaDescricao.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaDescricao.Execute(request);
            var teste = response.Content.Replace("\\\"", string.Empty);
            var retorno = JsonConvert.DeserializeObject<DescricaoItemML[]>(teste);

            return retorno;
        }

        public static RespostaBuscaItemML[] BuscarVariosItemCadastradoML(string idsItens)
        {
            /*var retornaListaItensDescricao = new RestClient("https://api.mercadolibre.com/items?ids="+idsItens);*/
            var retornaListaItensDescricao = new RestClient("https://api.mercadolibre.com/items?ids="+ idsItens);
            retornaListaItensDescricao.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + SessaoConexao.AccessToken), ParameterType.HttpHeader);
            IRestResponse response = retornaListaItensDescricao.Execute(request);

            var retorno = JsonConvert.DeserializeObject<RespostaBuscaItemML[]>(response.Content);

            return retorno;
        }



    }
}
