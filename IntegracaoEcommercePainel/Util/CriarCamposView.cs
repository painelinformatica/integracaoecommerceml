﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IntegracaoEcommercePainel.Util
{
    public static class CriarCamposView
    {
        public static PackIcon CriarIcone(PackIconKind imagemIcon, HorizontalAlignment posicaoHorizontal, int altura, int largura, int marginLeft, int marginTop, int marginRight, int marginBottom)
        {
            PackIcon icon = new PackIcon();
            icon.Kind = imagemIcon;
            icon.HorizontalAlignment = posicaoHorizontal;
            icon.Height = altura;
            icon.Width = largura;
            icon.Margin = new Thickness(marginLeft, marginTop, marginRight, marginBottom);
            return icon;
        }

        public static TextBlock CriarTextBlock(string texto, int altura, HorizontalAlignment posicaoHorizontal, int marginLeft, int marginTop, int marginRight, int marginBottom, SolidColorBrush corFonte, SolidColorBrush corBackground)
        {
            TextBlock txtBlock = new TextBlock();
            txtBlock.Text = texto;
            txtBlock.Height = altura;
            txtBlock.HorizontalAlignment = posicaoHorizontal;
            txtBlock.Foreground = corFonte;
            txtBlock.Background = corBackground;
            txtBlock.Margin = new Thickness(marginLeft, marginTop, marginRight, marginBottom);
            return txtBlock;
        }
    }
}
