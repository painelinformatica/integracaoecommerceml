﻿using IntegracaoPainel.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows;
using IntegracaoPainel.ModelML.Credencial;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace IntegracaoEcommercePainel.Util
{
    public static class Util
    {
        
        public static void CriarArquivoCredencialJson(string caminhoDiretorio, string nomeArquivo , string resposta)
        {
            File.Create(Path.Combine(caminhoDiretorio, nomeArquivo)).Dispose();
            File.WriteAllText(Path.Combine(caminhoDiretorio, nomeArquivo), resposta);
        }

        public static String VerificarSeArquivoExiste(string caminhoDiretorio, string nomeArquivo)
        {
            string caminho = Path.Combine(caminhoDiretorio, nomeArquivo);

            if (File.Exists(caminho) == true)
            {
                return "ArquivoExiste";
            }
            else
            {
                return "ArquivoNaoExiste";
            }
        }

        public static void CriarDiretorioLocal(string caminho)
        {
            Directory.CreateDirectory(caminho);
        }

        public static List<CredencialML> CarregarDadosDataGrid(string ipServidor, string banco)
        {
            string status;
            bool validar;

            /*var lerCredencial = File.ReadAllText(Path.Combine(caminho, nomeArquivo));
            CredencialML resultado = JsonConvert.DeserializeObject<CredencialML>(lerCredencial);*/

            ConexaoBancoDados.GetConexao(ipServidor, banco);
            string sqlquery = "SELECT Iddadosdeacessoml, Nicknameusuario, Emailusuario, Dataautenticacao, Vlr_pegar_ecommerce, Idoper_pegar_ecommerce, Idpessoal_pegar_ecommerce FROM Tbdadosdeacessoml";
            SqlCommand cmd2 = new SqlCommand(sqlquery, ConexaoBancoDados.Conexao);
            cmd2.CommandType = CommandType.Text;
            SqlDataReader reader2;
            reader2 = cmd2.ExecuteReader();
            reader2.Read();
            
            TimeSpan ts = Convert.ToDateTime(reader2["Dataautenticacao"]) - DateTime.Now;
            int diferencaHora = ts.Hours;

            if ((diferencaHora > 0) && (diferencaHora <= 6))
            {
                status = "Válido";
                validar = false;
            }
            else
            {
                status = "Inválido";
                validar = true;
            }

            List<CredencialML> resultado1 = new List<CredencialML>();
            resultado1.Add(new CredencialML() {IdDadosDeAcesso = Convert.ToInt32(reader2["Iddadosdeacessoml"]) ,NickNameUsuario = reader2["Nicknameusuario"].ToString(), EmailUsuario = reader2["Emailusuario"].ToString(), ValorSGE = reader2["Vlr_pegar_ecommerce"].ToString(), Status = status, Validar = validar, IdOperacao = Convert.ToInt32(reader2["Idoper_pegar_ecommerce"].ToString()), IdPessoal = Convert.ToInt32(reader2["Idpessoal_pegar_ecommerce"].ToString())});                                                                                                                        
            reader2.Close();
            return resultado1;
        }

        public static string RemoverAcento(this string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }

        public static string RemoverAcentoEspaco(this string text)
        {
            string removerAcento = RemoverAcento(text);
            string removerEspaco = removerAcento.Replace(" ", "");

            return removerEspaco;
        }

        public static string ConverteValorSQL(string valor)
        {
            string v1 = valor.Replace(".", "");
            string v2 = v1.Replace(",", ".");

            return v2;
        }

        public static void CriarPastaArquivo(string caminhoDiretorio, string nomeArquivo, string arquivo)
        {
            if (!Directory.Exists(caminhoDiretorio))
            {
                Directory.CreateDirectory(caminhoDiretorio);
                File.Create(Path.Combine(caminhoDiretorio, nomeArquivo)).Dispose();
                File.WriteAllText(Path.Combine(caminhoDiretorio, nomeArquivo), arquivo);
            }
            else
            {
                File.Create(Path.Combine(caminhoDiretorio, nomeArquivo)).Dispose();
                File.WriteAllText(Path.Combine(caminhoDiretorio, nomeArquivo), arquivo);
            }
        }

        public static string NormalizaHora(DateTime dateTime)
        {
            string h1 = dateTime.ToString().Replace("/", "");
            string h2 = h1.Replace(":", "");
            string h3 = h2.Replace(" ", "");

            return h3;
        }

        public static string FormatarHora(DateTime horaFormatar, int indexFormatoHora)
        {
            DateTime hora = horaFormatar;
            String[] format =
            {
                "yy-MM-dd",
            };
            string date;
            date = hora.ToString(format[0], DateTimeFormatInfo.InvariantInfo);
            var retornoHora = String.Concat(format[0], " ", date);

            return retornoHora;
        }

        public static string FormatarCNPJ(string cnpj)
        {
            string response = cnpj.Trim();
            if (response.Length == 14)
            {
                response = response.Insert(12, "-");
                response = response.Insert(8, "/");
                response = response.Insert(5, ".");
                response = response.Insert(2, ".");
            }
            return response;

           /* return Convert.ToUInt64(cnpj).ToString(@"00\.000\.000\/0000\-00");*/
        }

        public static string FormatarCPF(string cpf)
        {
            string response = cpf.Trim();
            if (response.Length == 11)
            {
                response = response.Insert(9, "-");
                response = response.Insert(6, ".");
                response = response.Insert(3, ".");
            }
            return response;
        }
    }
}
